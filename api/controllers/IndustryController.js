/**
 * IndustryController
 *
 * @description :: Server-side logic for managing industries
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	
	create : function(req, res){
		console.log("reqbody", req.body)
		Industry.create(req.body).then(function(result){
			console.log("result======>", result)
			res.send(result);
		})
	},

	getIndustry : function(req, res){
		Industry.find().sort({value: 1}).exec(function(err, result){
			if(err){
				return err;
			}else{
				return res.send(result);
			}
		})
	}
};

