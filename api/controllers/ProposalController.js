/**
 * ProposalController
 *
 * @description :: Server-side logic for managing proposals
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var async = require('async');
var ObjectId = require('mongodb').ObjectID;
// var waterfall = require('async-waterfall');
var async = require('async');
var request = require('request');
var pdf = require('pdfcrowd');
var path = require('path');
var RazorpayUrl = sails.config.secrets.getRazorpayUrl();

module.exports = {
  find: function (req, res) {
    Proposal.native(function (error, collection) {
      var query = [{
        $match: {
          _id: ModelService.ObjectId(Proposal, req.params.id)
        }
      }, {
        $lookup: {
          localField: "postId",
          foreignField: "_id",
          from: "post",
          as: "post"
        }
      }, {
        $addFields: {
          post: { $arrayElemAt: ["$post", 0] }
        }
      }, {
        $lookup: {
          localField: "receiverId",
          foreignField: "_id",
          from: "user",
          as: "post_user"
        }
      }, {
        $addFields: {
          post_user_firstName: { $arrayElemAt: ["$post_user.firstName", 0] },
          post_user_lastName: { $arrayElemAt: ["$post_user.lastName", 0] },
          post_user_comapanyName: { $arrayElemAt: ["$post_user.comapanyName", 0] },
          post_user_profile_pic: { $arrayElemAt: ["$post_user.comapanyName", 0] },
          post_user_id: { $arrayElemAt: ["$post_user._id", 0] },

        }
      }, {

      }]
      collection.aggregate(query)
        .toArray(function (err, result) {
          err ? err.status(500).send() : res.send(result);
        })
    })
  },

  generateInvoice: function (req, res) {
    console.log(`Invoice content: `, req.body.htmlContent);
    var htmlContent = req.body.htmlContent;
    var client = new pdf.Pdfcrowd(sails.config.secrets.pdfcredential.username, sails.config.secrets.pdfcredential.api_key);
    var fileName = "invoice_" + new Date().getTime() + '.pdf';
    console.log(`PDF filename: `, path.join(process.env.PWD, 'temp', fileName));
    client.convertHtml(htmlContent, pdf.saveToFile(path.join(process.env.PWD, 'temp', fileName)));
    // CloudinaryService.upload_image_on_cloud(fileName);
    res.send({ filePath: '/temp/' + fileName });
  },

  getAllOngoingJobList: function (req, res) {
    console.log("------->req------------------>", req.params.pageNo)
    var perPageCount = 100;
    var pageNo = req.params.pageNo - 1
    var count = 0;
    var skipValue = pageNo * perPageCount;
    var totalPages = 0

    Proposal.native(function (err, collection) {
      var query = [
        {
          $match: { "status": "contractSign" }
        },
        {
          $lookup: {
            from: "post",
            localField: "postId",
            foreignField: "_id",
            as: "post_details"
          }
        },
        { $unwind: "$post_details" },
        {
          $lookup: {
            from: "user",
            localField: "receiverId",
            foreignField: "_id",
            as: "client_details"
          }
        },
        { $unwind: "$client_details" },
        {
          $lookup: {
            from: "user",
            localField: "senderId",
            foreignField: "_id",
            as: "sender"
          }
        },
        { $unwind: "$sender" },
        {
          $project: {
            post_id: "$post_details._id",
            post: "$post_details.title",
            start_date: "$post_details.start_date",
            end_date: "$post_details.end_date",
            cost: "$post_details.cost",
            createdAt: "$createdAt",
            proposalDetail1: "$proposalDetail",
            proposal_id: "$_id",
            _id: 1,
            subType: "$post_details.subType",
            sender: {
              _id: "$sender._id",
              user_name: "$sender.firstName",
              last_name: "$sender.lastName",
              email_id: "$sender.email_id",
              mobile: "$sender.mobile",
              userType: "$sender.userType"
            },
            receiver: {
              _id: "$client_details._id",
              user_name: "$client_details.firstName",
              last_name: "$client_details.lastName",
              email_id: "$client_details.email_id",
              mobile: "$client_details.mobile",
              userType: "$client_details.userType"
            }
          }
        },
        { $sort: { createdAt: -1 } }
      ]

      async.waterfall([
        function (cb) {
          collection.aggregate(query).toArray(function (err, result2) {
            console.log("----------------First result----------->", result2)
            if (result2.length == 0)
              res.send({ responseData: result2, totalPage: 1, totalCount: 0, responseMessage: "No data found", responseCode: 200 })
            if (result2.length) {
              count = result2.length
              if (count % perPageCount == 0) {
                totalPages = count / perPageCount
                cb(null, true)
              } else if (count / perPageCount < 1) {
                totalPages = 1
                cb(null, true)
              } else {
                totalPages = Math.ceil(count / perPageCount)
                cb(null, true)
              }
            } else {
              cb(null, true)
            }
          })
        }, function (result, cb) {
          collection.aggregate(query).skip(skipValue).limit(perPageCount).toArray(function (err, result1) {
            console.log("----------------Second result----------->", result1.length)
            if (err) {
              cb(err)
            } else {
              cb(null, result1)
            }
          })
        }
      ], function (err, result) {
        console.log("final result", result)
        err ? res.status(500).send(err) : res.send({ responseData: result, totalPage: totalPages, totalCount: count });
      })
    })
  },


  getAllCompletedJobList: function (req, res) {
    var perPageCount = 100;
    var pageNo = req.params.pageNo - 1
    var count = 0;
    var skipValue = pageNo * perPageCount;
    var totalPages = 0

    Proposal.native(function (err, collection) {
      var query = [
        {
          $match: { "status": "complete" }
        },
        {
          $lookup: {
            from: "post",
            localField: "postId",
            foreignField: "_id",
            as: "post_details"
          }
        },
        { $unwind: "$post_details" },
        {
          $lookup: {
            from: "user",
            localField: "receiverId",
            foreignField: "_id",
            as: "client_details"
          }
        },
        { $unwind: "$client_details" },
        {
          $lookup: {
            from: "user",
            localField: "senderId",
            foreignField: "_id",
            as: "sender"
          }
        },
        { $unwind: "$sender" },
        {
          $project: {
            post_id: "$post_details._id",
            post: "$post_details.title",
            start_date: "$post_details.start_date",
            end_date: "$post_details.end_date",
            cost: "$post_details.cost",
            createdAt: "$createdAt",
            proposalDetail1: "$proposalDetail",
            proposal_id: "$_id",
            _id: 1,
            subType: "$post_details.subType",
            sender: {
              _id: "$sender._id",
              user_name: "$sender.firstName",
              last_name: "$sender.lastName",
              email_id: "$sender.email_id",
              mobile: "$sender.mobile",
              userType: "$sender.userType"
            },
            receiver: {
              _id: "$client_details._id",
              user_name: "$client_details.firstName",
              last_name: "$client_details.lastName",
              email_id: "$client_details.email_id",
              mobile: "$client_details.mobile",
              userType: "$client_details.userType"
            }
          }
        },
        { $sort: { createdAt: -1 } }
      ]

      // var query=[
      //   {
      //      $match: {"status": "complete" }
      //    },
      //    {
      //      $lookup:{
      //           from: "post",
      //           localField: "postId",
      //           foreignField: "_id",
      //           as: "post_details"
      //      }
      //    }, {
      //      $unwind: "$post_details"
      //    },
      //    {
      //    $project:{
      //        post_id: "$post_details._id",
      //        post: "$post_details.title",
      //        duration:"$post_details.duration",
      //        cost:"$post_details.cost",
      //        createdAt:"$createdAt",
      //        proposalDetail1:"$proposalDetail",
      //        proposal_id:"$_id"
      //     }
      //   }
      //  ]

      // collection.aggregate(query).toArray(function(err, result1){

      //       if(err){
      //         console.log("errrr--------", err)
      //         // cb(err)
      //       }else{
      //         console.log("getAllPostedJobList getAllPostedJobList----------------->", result1)
      //         // cb(null, result1)
      //       }
      //     })

      async.waterfall([
        function (cb) {
          collection.aggregate(query).toArray(function (err, result2) {
            console.log('aggregate result--------------->', result2)
            if (result2.length == 0)
              res.send({ responseData: result2, totalPage: 1, totalCount: 0, responseMessage: "No data found", responseCode: 200 })
            if (result2.length) {
              count = result2.length
              if (count % perPageCount == 0) {
                totalPages = count / perPageCount
                cb(null, true)
              } else if (count / perPageCount < 1) {
                totalPages = 1
                cb(null, true)
              } else {
                totalPages = Math.ceil(count / perPageCount)
                cb(null, true)
              }
            } else {
              cb(null, true)
            }
          })
        }, function (result, cb) {
          collection.aggregate(query).skip(skipValue).limit(perPageCount).toArray(function (err, result1) {

            if (err) {
              console.log("errrr--------", err)
              cb(err)
            } else {
              console.log("getAllPostedJobList getAllPostedJobList----------------->", result1)
              cb(null, result1)
            }
          })
        }
      ], function (err, result) {
        console.log("final result", result)
        err ? res.status(500).send(err) : res.send({ responseData: result, totalPage: totalPages, totalCount: count });
      })

    })

  },


  // ,{
  //             $lookup:{
  //                 localField:"userId",
  //                 foreignField:"_id",
  //                 from:"user",
  //                 as:"userDetails"
  //             }
  //         },
  //         {
  //             $lookup:{
  //                 localField:"_id",
  //                 foreignField:"postId",
  //                 from:"proposal",
  //                 as:"proposalCount"
  //             }
  //         },
  //         {
  //                 $unwind:"$userDetails"
  //             },
  //              {
  //                 $unwind:"$proposalCount"
  //             },
  //         {
  //             $project:{
  //                 "title": "$title",
  //                 "cost": "$cost",
  //                 "duration": "$duration",
  //                 "createdAt": "$created_at",
  //                 "userDetailss": {
  //                     _id:"$userDetails._id",
  //                     user_name: "$userDetails.firstName",
  //                     last_name :  "$userDetails.lastName",
  //                     email_id :  "$userDetails.email_id",
  //                     userType : "$userDetails.userType"
  //                 },
  //                 proposalCount:{
  //                     proposal_id: "$proposalCount._id"
  //                 }
  //             }
  //         }

  getAllPostedJobList: function (req, res) {
    var perPageCount = 100;
    var pageNo = req.params.pageNo - 1
    var count = 0;
    var skipValue = pageNo * perPageCount;
    var totalPages = 0

    Post.native(function (err, collection) {
      var query = [
        {
          $match: { "type": "job" }
        },
        // { $group: { _id: null, count: { $sum: 1 } } },
        {
          $lookup: {
            from: "user",
            localField: "userId",
            foreignField: "_id",
            as: "userId"
          }
        },
        { $unwind: "$userId" },
        {
          $lookup: {
            from: "proposal",
            localField: "_id",
            foreignField: "postId",
            as: "proposal"
          }
        },
        // { $group: { _id: "$proposal.postId", total: { $sum: 1 } } },
        {
          $project: {
            title: "$title",
            start_date: "$start_date",
            end_date: "$end_date",
            status: "$status",
            userType: "$userType",
            cost: "$cost",
            proposal: "$proposal._id",
            userId: {
              _id: "$userId._id",
              firstName: "$userId.firstName",
              lastName: "$userId.lastName",
              email_id: "$userId.email_id",
              mobile: "$userId.mobile"
            },
            createdAt: "$createdAt"
          }
        },
        { $sort: { createdAt: -1 } }
      ]

      async.waterfall([
        function (cb) {
          collection.aggregate(query).toArray(function (err, result2) {
            if (result2.length == 0)
              res.send({ responseData: result2, totalPage: 1, totalCount: 0, responseMessage: "No data found", responseCode: 200 })
            if (result2.length) {
              count = result2.length
              if (count % perPageCount == 0) {
                totalPages = count / perPageCount
                cb(null, true)
              } else if (count / perPageCount < 1) {
                totalPages = 1
                cb(null, true)
              } else {
                totalPages = Math.ceil(count / perPageCount)
                cb(null, true)
              }
            } else {
              cb(null, true)
            }
          })
        }, function (result, cb) {
          collection.aggregate(query).skip(skipValue).limit(perPageCount).toArray(function (err, result1) {
            if (err) {
              cb(err)
            } else {
              console.log("------------", result1)
              cb(null, result1)
            }
          })
        }
      ], function (err, result) {
        console.log("final result", result)
        err ? res.status(500).send(err) : res.send({ responseData: result, totalPage: totalPages, totalCount: count });
      })

    })

  },

  getDashboardData: function (req, res) {
    async.parallel({
      user: function (callback) {
        User.find({ $or: [{ userType: "0" }, { userType: "1" }, { userType: "3" }, { userType: "6" }] }).limit(5).sort({ createdAt: -1 }).exec(function (err, result) {
          callback(null, result)
        })
      },
      post: function (callback) {
        Post.find({}).limit(5).populate("userId").sort({ createdAt: -1 }).exec(function (err, result) {
          callback(null, result)
        })
      }
    }, function (err, results) {

      console.log("result--", results)
      if (err) {
        return res.send({ responseCode: 500, responseMessage: "server error" })
      }
      else {
        return res.send({ responseCode: 200, responseMessage: "sucess", result: results })
      }
      // err?res.status(500).send(err):res.send({result:results})
      // results now equals to: [one: 'abc\n', two: 'xyz\n']
    });
  },


  getProposalSend: function (req, res) {
    // return res.send({length:1});
    Proposal.native(function (err, collection) {
      collection.aggregate([{
        $match: {
          senderId: ModelService.ObjectId(User, req.current_user.id)
        }
      }, {
        $group: {
          _id: "null",
          count: { $sum: 1 }
        }
      }])
        .toArray(function (err, result) {
          err ? res.status(500).send(err) : res.send(result)
        })
    });
  },

  getRunningProposalCount: function (req, res) {
    // return res.send({length:1});
    Proposal.native(function (err, collection) {
      collection.aggregate([{
        $match: {
          senderId: ModelService.ObjectId(User, req.current_user.id),
          status: "contractSign"
        }
      }, {
        $group: {
          _id: "null",
          count: { $sum: 1 }
        }
      }])
        .toArray(function (err, result) {
          err ? res.status(500).send(err) : res.send(result)
        })
    });
  },
  getRunningProposalClient: function (req, res) {
    // return res.send({length:1});
    Proposal.native(function (err, collection) {
      //var obj=(req.current_user.userType==0 || req.current_user.userType==1)?{senderId:ModelService.ObjectId(User,req.current_user.id)}:{receiverId:ModelService.ObjectId(User,req.current_user.id)}
      //console.log("obj----->",JSON.stringify(obj))
      var query = [];
      if (req.current_user.userType == 0 || req.current_user.userType == 1) {
        console.log("if----->", req.current_user.id)
        query = [{
          $match: {
            receiverId: ModelService.ObjectId(User, req.current_user.id),
            status: "contractSign"
          }
        }, {
          $lookup: {
            from: "post",
            localField: "postId",
            foreignField: "_id",
            as: "postDetail"
          }
        }, {
          $addFields: {
            postDetail: {
              $arrayElemAt: ["$postDetail", 0]
            },
            userType: { $arrayElemAt: ["$postDetail.userType", 0] }
          }
        }]
      }
      else {
        console.log("else----->", req.current_user.id)
        query = [{
          $match: {
            senderId: ModelService.ObjectId(User, req.current_user.id),
            status: "contractSign"
          }
        }, {
          $lookup: {
            from: "post",
            localField: "postId",
            foreignField: "_id",
            as: "postDetail"
          }
        }, {
          $addFields: {
            postDetail: {
              $arrayElemAt: ["$postDetail", 0]
            },
            userType: { $arrayElemAt: ["$postDetail.userType", 0] }
          }
        }]
      }
      collection.aggregate(query)
        .toArray(function (err, result) {
          err ? res.status(500).send(err) : res.send(result)
        })
    });
  },

  getCompleteProposal: function (req, res) {
    // return res.send({length:1});
    Proposal.native(function (err, collection) {
      //var obj=(req.current_user.userType==0 || req.current_user.userType==1)?{senderId:ModelService.ObjectId(User,req.current_user.id)}:{receiverId:ModelService.ObjectId(User,req.current_user.id)}
      //console.log("obj----->",JSON.stringify(obj))
      var query = [];
      if (req.current_user.userType == 0 || req.current_user.userType == 1) {
        console.log("if----->", req.current_user.id)
        query = [{
          $match: {
            receiverId: ModelService.ObjectId(User, req.current_user.id),
            status: "complete"
          }
        }, {
          $lookup: {
            from: "post",
            localField: "postId",
            foreignField: "_id",
            as: "postDetail"
          }
        }, {
          $addFields: {
            postDetail: {
              $arrayElemAt: ["$postDetail", 0]
            },
            userType: { $arrayElemAt: ["$postDetail.userType", 0] }
          }
        }]
      }
      else {
        console.log("else----->", req.current_user.id)
        query = [{
          $match: {
            senderId: ModelService.ObjectId(User, req.current_user.id),
            status: "complete"
          }
        }, {
          $lookup: {
            from: "post",
            localField: "postId",
            foreignField: "_id",
            as: "postDetail"
          }
        }, {
          $addFields: {
            postDetail: {
              $arrayElemAt: ["$postDetail", 0]
            },
            userType: { $arrayElemAt: ["$postDetail.userType", 0] }
          }
        }]
      }
      collection.aggregate(query)
        .toArray(function (err, result) {
          err ? res.status(500).send(err) : res.send(result)
        })
    });
  },


  getClientProposalCount: function (req, res) {
    // return res.send({length:1});
    Proposal.native(function (err, collection) {
      collection.aggregate([{
        $match: {
          receiverId: ModelService.ObjectId(User, req.current_user.id)
        }
      }, {
        $group: {
          _id: "null",
          count: { $sum: 1 }
        }
      }])
        .toArray(function (err, result) {
          err ? res.status(500).send(err) : res.send(result)
        })
    });
  },
  sendProposal: function (req, res) {
    console.log("req----------->", req.body)
    console.log("req.current_user.id", req.current_user.id)
    var senderDetail, recieverDetail, projectTitle;
    async.waterfall([function (cb) {
      // new entry in proposal
      Post.native(function (err, collection) {
        collection.findOne({ _id: ModelService.ObjectId(Post, req.body.postId) }, function (err, result) {
          console.log("ppppp", result)
          if (err) {
            cb(err)
          }
          else {
            console.log("proposalcreate", result);
            var title1 = result.title
            cb(null, title1, result)
          }
        })
      })
    }, function (title1, result, cb) {
      projectTitle = result.title;
      var estimated_hours = 0, estimated_cost = 0;
      console.log("proposalDetail1--", req.body);
      for (var i = 0; i < req.body.proposalDetail.length; i++) {
        console.log("proposalDetail", req.body.proposalDetail[i]);
        //  estimated_hours+=parseInt(req.body.proposalDetail[i].time);
        estimated_cost += parseInt(req.body.proposalDetail[i].cost);
        var oid = new ObjectId();
        req.body.proposalDetail[i]._id = JSON.parse(JSON.stringify(oid))
      }
      // if(req.body.projectDuration>=parseInt(result.time) || req.body.projectDuration!=estimated_hours){
      //     return res.json(500,{ success: 'ERROR' ,responseData:"Project Duration is not in Range!"})
      // }
      if (req.body.proposalCost != result.cost || req.body.proposalCost != estimated_cost) {
        return res.json(500, { success: 'ERROR', responseData: "Proposal Cost is not in Range!" })
      }
      else {
        var proposals = {
          postId: req.body.postId,
          senderId: req.current_user.id,
          receiverId: req.body.receiverId,
          status: req.body.status,
          coverLetter: req.body.coverLetter,
          type: req.body.type,
          description: req.body.description,
          proposalDetail: req.body.proposalDetail
        }
        Proposal.create(proposals).then(function (result) {
          cb(null, title1, result, result.id)
          sails.log.debug('Success------->', JSON.stringify(result))
        }).catch(function (err) {
          cb(err)
        });
      }

    }, function (title1, result, proposalId, cb) {
      User.native(function (err, collection) {
        var oid = new ObjectId();
        var query = {
          _id: ModelService.ObjectId(User, req.body.receiverId)
        };
        collection.update(query, {
          $push: {
            notification: {
              type: "notification",
              sub_type: "new fixedbid proposal",
              status: "unread",
              assignmentId: ModelService.ObjectId(Post, req.body.postId),
              proposalId: ModelService.ObjectId(Proposal, proposalId),
              user_id: ModelService.ObjectId(User, req.current_user.id),
              created_at: new Date(),
              _id: JSON.parse(JSON.stringify(oid))
            }
          }
        }, function (err, result) {
          err ? cb(err) : cb(null, title1, result, proposalId, JSON.parse(JSON.stringify(oid)))
        })
      })
    }, function (title1, result, proposalId, notifyId, cb) {
      var oid = new ObjectId();
      var _activityId = JSON.parse(JSON.stringify(oid))
      Proposal.native(function (err, collection) {
        collection.update({ _id: ModelService.ObjectId(Proposal, proposalId) }, { $push: { "recentActivity": { created_at: new Date(), type: "proposal", _id: _activityId, created_by: ModelService.ObjectId(User, req.current_user.id) } } }, function (err, result) {
          // console.log("activityresult-----------", result)
          err ? cb(err) : cb(null, title1, result, notifyId, proposalId)
        })
      })

    }, function (title1, result, notifyId, proposalId, cb) {
      console.log("title1title1title1title1title1--------------->", title1)
      //  var userId=req.body.receiverId
      var userId = req.current_user.id
      User.getNamePic(userId)
        .then(function (_user) {
          console.log("_user-------------------->", _user)
          var _obj = {
            sub_type: "new fixedbid proposal",
            assignmentId: req.body.postId,
            proposalId: proposalId,
            firstName: _user.firstName,
            lastName: _user.lastName,
            companyName: _user.companyName,
            projectName: title1,
            created_at: new Date(),
            notifyId: notifyId
          }
          NotificationService.newNotification(_obj, req.body.receiverId)
          cb(null)
        }, function (objE) {
          console.log("error--->", objE);
          cb(objE)
        })
    },
    function (cb) {
      var sender = req.current_user.id;
      User.native(function (err, collection) {
        collection.find({
          _id: ModelService.ObjectId(User, sender)
        })
          .toArray(function (err, result) {
            if (err) {
              cb(err);
            }
            senderDetail = {
              senderFirstName: result[0].firstName,
              senderLastName: result[0].lastName,
              senderCompanyName: result[0].companyName
            }
            cb(null)
          })
      })

    },
    function (cb) {
      var userId = req.body.receiverId;
      User.native(function (err, collection) {
        collection.find({
          _id: ModelService.ObjectId(User, userId)
        })
          .toArray(function (err, result) {
            console.log("result", result)
            if (err) {
              cb(err);
            }
            recieverDetail = {
              firstName: result[0].firstName,
              lastName: result[0].lastName,
              companyName: result[0].companyName,
              email_id: result[0].email_id
            }
            cb(null)
          })
      })

    },
    function (cb) {
      console.log("reqreqtitlereqtitle--->", req.body.title)
      var mailContentDynamic = EmailService.getMailHtml_consultantApplyJob(senderDetail.senderFirstName, senderDetail.senderLastName, recieverDetail.firstName, recieverDetail.lastName, projectTitle);

      var mailContent = MailTemp.style + MailTemp.content_start1 + mailContentDynamic + MailTemp.content_end2;
      EmailService.send('no-reply@feelance.co', recieverDetail.email_id, 'Feelance | New proposal received', mailContent).then(function (result) {
        console.log("result2333322" + JSON.stringify(result));
        if (!result.status) {
          var responseData = {
            user: "Mail not sent!",
            status: false
          }
          cb({ success: 'ERROR', responseData: responseData });
        }
        else
          var responseData = "Job Apply Successfully!"
        cb(null, { success: 'Success', responseData: responseData });
      })
    }

    ], function (err, result) {
      err ? res.json(500, { success: 'ERROR', responseData: err }) : res.json(200, { success: 'Success', responseData: result })

    })
  },


  sendOdcProposal: function (req, res) {
    var senderDetail, recieverDetail, projectTitle;
    async.waterfall([function (cb) {
      Post.native(function (err, collection) {
        collection.findOne({ _id: ModelService.ObjectId(Post, req.body.postId) }, function (err, result) {
          console.log("ppppp", result)
          if (err) {
            cb(err)
          }
          else {

            console.log("proposalcreate", result);
            var title1 = result.title
            cb(null, title1, result)
          }
        })
      })
    }, function (title1, result, cb) {
      projectTitle = result.title;
      var estimated_hours = 0, estimated_cost = 0;
      console.log("proposalDetail1--", req.body);
      for (var i = 0; i < req.body.proposalDetail.length; i++) {
        console.log("proposalDetail", req.body.proposalDetail[i]);
        // estimated_hours+=parseInt(req.body.proposalDetail[i].time);
        estimated_cost += parseInt(req.body.proposalDetail[i].cost);
        var oid = new ObjectId();
        req.body.proposalDetail[i]._id = JSON.parse(JSON.stringify(oid))
      }

      var proposals = {
        postId: req.body.postId,
        senderId: req.current_user.id,
        receiverId: req.body.receiverId,
        status: req.body.status,
        coverLetter: req.body.coverLetter,
        type: req.body.type,
        description: req.body.description,
        proposalDetail: req.body.proposalDetail
        // proposalDetail:[{
        //       milestone:"Milestone1",
        //       time:result.duration,
        //       cost:result.cost,
        //       _id:JSON.parse(JSON.stringify(oid))
        // }]

      }
      Proposal.create(proposals).then(function (result) {
        cb(null, title1, result, result.id)
        sails.log.debug('Success------->', JSON.stringify(result))
      }).catch(function (err) {
        cb(err)
      });


    }, function (title1, result, proposalId, cb) {
      User.native(function (err, collection) {
        var oid = new ObjectId();
        var query = {
          _id: ModelService.ObjectId(User, req.body.receiverId)
        };
        collection.update(query, {
          $push: {
            notification: {
              type: "notification",
              sub_type: "new odc proposal",
              status: "unread",
              assignmentId: ModelService.ObjectId(Post, req.body.postId),
              proposalId: ModelService.ObjectId(Proposal, proposalId),
              user_id: ModelService.ObjectId(User, req.current_user.id),
              created_at: new Date(),
              _id: JSON.parse(JSON.stringify(oid))
            }
          }
        }, function (err, result) {
          err ? cb(err) : cb(null, title1, result, proposalId, JSON.parse(JSON.stringify(oid)))
        })
      })
    }, function (title1, result, proposalId, notifyId, cb) {
      var oid = new ObjectId();
      var _activityId = JSON.parse(JSON.stringify(oid))
      Proposal.native(function (err, collection) {
        collection.update({ _id: ModelService.ObjectId(Proposal, proposalId) }, { $push: { "recentActivity": { created_at: new Date(), type: "proposal", _id: _activityId, created_by: ModelService.ObjectId(User, req.current_user.id) } } }, function (err, result2) {
          // console.log("activityresult-----------", result2)
          err ? cb(err) : cb(null, title1, result, notifyId, proposalId)

        })
      })
    }, function (title1, result, notifyId, proposalId, cb) {
      // var userId=req.body.receiverId;
      var userId = req.current_user.id;

      User.getNamePic(userId)
        .then(function (_user) {
          var _obj = {
            sub_type: "new odc proposal",
            assignmentId: req.body.postId,
            proposalId: proposalId,
            firstName: _user.firstName,
            lastName: _user.lastName,
            companyName: _user.companyName,
            projectName: title1,
            created_at: new Date(),
            notifyId: notifyId
          }
          NotificationService.newNotification(_obj, req.body.receiverId)
          cb(null)
        }, function (objE) {
          console.log("error--->", objE);
          cb(objE)
        })
    },
    function (cb) {
      var sender = req.current_user.id;
      User.native(function (err, collection) {
        collection.find({
          _id: ModelService.ObjectId(User, sender)
        })
          .toArray(function (err, result) {
            if (err) {
              cb(err);
            }
            senderDetail = {
              senderFirstName: result[0].firstName,
              senderLastName: result[0].lastName,
              senderCompanyName: result[0].companyName
            }
            cb(null)
          })
      })

    },
    function (cb) {
      var userId = req.body.receiverId;
      User.native(function (err, collection) {
        collection.find({
          _id: ModelService.ObjectId(User, userId)
        })
          .toArray(function (err, result) {
            if (err) {
              cb(err);
            }
            recieverDetail = {
              firstName: result[0].firstName,
              lastName: result[0].lastName,
              companyName: result[0].companyName,
              email_id: result[0].email_id
            }
            cb(null)
          })
      })

    },
    function (cb) {
      console.log("reqreqtitlereqtitle--->", req.body.title)
      var mailContentDynamic1 = EmailService.getMailHtml_consultantApplyJob(senderDetail.senderFirstName, senderDetail.senderLastName, recieverDetail.firstName, recieverDetail.lastName, projectTitle);
      var mailContent = MailTemp.style + MailTemp.content_start1 + mailContentDynamic1 + MailTemp.content_end2;
      EmailService.send('no-reply@feelance.co', recieverDetail.email_id, 'Feelance | New proposal received', mailContent).then(function (result) {
        console.log("result2333322" + JSON.stringify(result));
        if (!result.status) {
          var responseData = {
            user: "Mail not sent!",
            status: false
          }
          cb({ success: 'ERROR', responseData: responseData });
        }
        else
          var responseData = "Job Apply Successfully!"
        cb(null, { success: 'Success', responseData: responseData });
      })
    }
    ], function (err, result) {
      err ? res.json(500, { success: 'ERROR', responseData: err }) : res.json(200, { success: 'Success', responseData: result })

    })
  },

  applyJob: function (req, res) {
    var projectDetail = {};
    var senderDetail, recieverDetail, projectTitle;
    async.waterfall([function (cb) {
      // new entry in proposal
      Post.native(function (err, collection) {
        collection.findOne({ _id: ModelService.ObjectId(Post, req.body.postId) }, function (err, result) {
          console.log("ppppp", result)
          if (err) {
            cb(err)
          }
          else {
            projectDetail = result;
            projectTitle = result.title;
            console.log("proposalcreate", result);
            cb(null, result)
          }
        })
      })
    }, function (result, cb) {
      var proposals = {
        postId: req.body.postId,
        senderId: req.current_user.id,
        receiverId: req.body.receiverId,
        status: req.body.status
      }
      Proposal.create(proposals).then(function (result) {
        cb(null, result)
        sails.log.debug('Success------->', JSON.stringify(result))
      }).catch(function (err) {
        cb(err)
      });
    }
      , function (result, cb) {
        User.native(function (err, collection) {
          var oid = new ObjectId();
          var query = {
            _id: ModelService.ObjectId(User, req.body.receiverId)
          };
          collection.update(query, {
            $push: {
              notification: {
                type: "notification",
                sub_type: "recruiter applyJob",
                status: "unread",
                assignmentId: ModelService.ObjectId(Post, req.body.postId),
                user_id: ModelService.ObjectId(User, req.current_user.id),
                created_at: new Date(),
                _id: JSON.parse(JSON.stringify(oid))
              }
            }
          }, function (err, result) {
            err ? cb(err) : cb(null, result, JSON.parse(JSON.stringify(oid)))
          })
        })
      }, function (result, notifyId, cb) {
        var userId = req.body.receiverId;
        User.getNamePic(userId)
          .then(function (_user) {
            var _obj = {
              sub_type: "recruiter applyJob",
              assignmentId: req.body.postId,
              firstName: _user.firstName,
              lastName: _user.lastName,
              companyName: _user.companyName,
              projectName: projectDetail.title,
              created_at: new Date(),
              notifyId: notifyId
            }
            NotificationService.newNotification(_obj, userId)
            cb(null)
          }, function (objE) {
            console.log("error--->", objE);
            cb(objE)
          })
      },
    function (cb) {
      var sender = req.current_user.id;
      User.native(function (err, collection) {
        collection.find({
          _id: ModelService.ObjectId(User, sender)
        })
          .toArray(function (err, result) {
            if (err) {
              cb(err);
            }
            senderDetail = {
              senderFirstName: result[0].firstName,
              senderLastName: result[0].lastName,
              senderCompanyName: result[0].companyName
            }
            cb(null)
          })
      })

    },
    function (cb) {
      var userId = req.body.receiverId;
      User.native(function (err, collection) {
        collection.find({
          _id: ModelService.ObjectId(User, userId)
        })
          .toArray(function (err, result) {
            if (err) {
              cb(err);
            }
            recieverDetail = {
              firstName: result[0].firstName,
              lastName: result[0].lastName,
              companyName: result[0].companyName,
              email_id: result[0].email_id
            }
            cb(null)
          })
      })

    },
    function (cb) {
      console.log("reqreqtitlereqtitle--->", req.body.title)
      var mailContentDD = EmailService.getMailHtml_consultantApplyJob(senderDetail.senderFirstName, senderDetail.senderLastName, recieverDetail.firstName, recieverDetail.lastName, projectTitle);
      var mailContent = MailTemp.style + MailTemp.content_start1 + mailContentDD + MailTemp.content_end2;
      EmailService.send('no-reply@feelance.co', recieverDetail.email_id, 'Feelance | New proposal received', mailContent).then(function (result) {
        console.log("result2333322" + JSON.stringify(result));
        if (!result.status) {
          var responseData = {
            user: "Mail not sent!",
            status: false
          }
          cb({ success: 'ERROR', responseData: responseData });
        }
        else
          var responseData = "Job Apply Successfully!"
        cb(null, { success: 'Success', responseData: responseData });
      })
    }
    ], function (err, result) {
      err ? res.json(500, { success: 'ERROR', responseData: err }) : res.json(200, { success: 'Success', responseData: result })

    })
  },

  askForSignoff: function (req, res) {
    var projectDetail = {};
    var senderDetail, recieverDetail;
    var milestoneName = req.body.milestoneName;
    Proposal.native(function (err, collection) {
      //var query=[];
      async.waterfall([function (cb) {
        // new entry in proposal
        Post.native(function (err, collection) {
          collection.findOne({ _id: ModelService.ObjectId(Post, req.body.postId) }, function (err, result) {
            console.log("ppppp", result)
            if (err) {
              cb(err)
            }
            else {
              projectDetail = result;
              console.log("proposalcreate", result);
              cb(null, result)
            }
          })
        })
      }, function (result, cb) {
        collection.update({
          _id: ModelService.ObjectId(Proposal, req.body.proposalId),
          "proposalDetail._id": req.body.milestoneId
        }, {
            $set: {
              "proposalDetail.$.status": "askSignoff"
            }
          }, function (err, result) {
            err ? cb(err) : cb(null, result);
          })
      }, function (result, cb) {
        User.native(function (err, collection) {
          var oid = new ObjectId();
          var query = {
            _id: ModelService.ObjectId(User, req.body.receiverId)
          };
          collection.update(query, {
            $push: {
              notification: {
                type: "notification",
                sub_type: "askSignoff",
                status: "unread",
                msg: "user ask to sign off over the completed milestone",
                proposalId: ModelService.ObjectId(Post, req.body.proposalId),
                assignmentId: ModelService.ObjectId(Post, req.body.postId),
                user_id: ModelService.ObjectId(User, req.current_user.id),
                created_at: new Date(),
                _id: JSON.parse(JSON.stringify(oid))
              }
            }
          }, function (err, result) {
            err ? cb(err) : cb(null, result, JSON.parse(JSON.stringify(oid)))
          })
        })
      }, function (result, notifyId, cb) {
        var userId = req.body.receiverId;
        User.getNamePic(req.current_user.id)
          .then(function (_user) {
            var _obj = {
              sub_type: "askSignoff",
              msg: "user ask to sign off over the completed milestone",
              proposalId: req.body.proposalId,
              firstName: _user.firstName,
              lastName: _user.lastName,
              assignmentId: req.body.postId,
              companyName: _user.companyName,
              projectName: projectDetail.title,
              created_at: new Date(),
              notifyId: notifyId
            }
            NotificationService.newNotification(_obj, userId)
            cb(null)
          }, function (objE) {
            console.log("error--->", objE);
            cb(objE)
          })
      },
      function (cb) {
        var sender = req.current_user.id;
        User.native(function (err, collection) {
          collection.find({
            _id: ModelService.ObjectId(User, sender)
          })
            .toArray(function (err, result) {
              if (err) {
                cb(err);
              }
              senderDetail = {
                senderFirstName: result[0].firstName,
                senderLastName: result[0].lastName,
                senderCompanyName: result[0].companyName
              }
              cb(null)
            })
        })

      },
      function (cb) {
        var userId = req.body.receiverId;
        User.native(function (err, collection) {
          collection.find({
            _id: ModelService.ObjectId(User, userId)
          })
            .toArray(function (err, result) {
              if (err) {
                cb(err);
              }
              recieverDetail = {
                firstName: result[0].firstName,
                lastName: result[0].lastName,
                companyName: result[0].companyName,
                email_id: result[0].email_id
              }
              cb(null)
            })
        })

      },
      function (cb) {
        console.log("reqreqtitlereqtitle--->", req.body.title)
        var mailContentDynamic = EmailService.getMailHtml_consultantAskSignoff(senderDetail.senderFirstName, senderDetail.senderLastName, recieverDetail.firstName, recieverDetail.lastName, projectDetail.title, milestoneName);
        var mailContent = MailTemp.style + MailTemp.content_start1 + mailContentDynamic + MailTemp.content_end2;
        EmailService.send('no-reply@feelance.co', recieverDetail.email_id, 'Feelance | Request for sign off', mailContent).then(function (result) {
          console.log("result2333322" + JSON.stringify(result));
          if (!result.status) {
            var responseData = {
              user: "Mail not sent!",
              status: false
            }
            cb({ success: 'ERROR', responseData: responseData });
          }
          else
            var responseData = "Job Apply Successfully!"
          cb(null, { success: 'Success', responseData: responseData });
        })
      }
      ], function (err, result) {
        err ? res.status(500).send(err) : res.send(result);
      })


    })
  },
  approvedSignoff: function (req, res) {
    console.log("ProposalController > approvedSignoff: ", req.body)
    var projectDetail = {};
    var testamount = parseInt(req.body.amount);
    var senderDetail, recieverDetail, projectTitle;
    var milestoneName = req.body.milestoneName;
    console.log("testamount", testamount)
    async.waterfall([
      function (cb) {
        var obj1 = {
          "account": req.body.account_id,
          "amount": testamount,
          "currency": "INR"
        }
        request.post(RazorpayUrl + '/transfers', { form: obj1 }, function (error, response, body) {
          // console.log(response)
          console.log(body)
          test = JSON.parse(body);
          cb(null, test)
        })

      }, function (result, cb) {
        // new entry in proposal
        Post.native(function (err, collection) {
          collection.findOne({ _id: ModelService.ObjectId(Post, req.body.postId) }, function (err, result) {
            console.log("ppppp", result)
            if (err) {
              cb(err)
            }
            else {
              projectDetail = result;
              projectTitle = result.title;
              console.log("proposalcreate", result);
              cb(null, result)
            }
          })
        })
      }, function (result, cb) {
        Proposal.native(function (err, collection) {
          collection.update({
            _id: ModelService.ObjectId(Proposal, req.body.proposalId),
            "proposalDetail._id": req.body.milestoneId
          }, {
              $set: {
                "proposalDetail.$.status": "complete"
              }
            }, function (err, result) {
              err ? cb(err) : cb(null, result);
            })
        })
      }, function (result, cb) {
        Proposal.native(function (err, collection) {
          collection.find({ _id: ModelService.ObjectId(Proposal, req.body.proposalId) })
            .toArray(function (err, result) {
              if (err)
                cb(err)
              else {
                console.log("proposalDetail1", JSON.stringify(result));

                var uncomplete = result[0].proposalDetail.filter(x => x.status != "complete");
                console.log("uncomplete--------------->", uncomplete)
                cb(null, uncomplete.length == 0, result[0].postId)
              }
            })
        })
      }, function (isCompleted, postId, cb) {
        console.log("isCompletedisCompletedisCompleted", isCompleted)
        if (isCompleted) {
          Post.native(function (err, collection) {
            collection.update({
              _id: ModelService.ObjectId(Post, postId)
            }, {
                $set: {
                  status: "complete"
                }
              }, function (err, result) {
                projectDetail = result;
                Proposal.native(function (err, collection) {
                  collection.update({
                    _id: ModelService.ObjectId(Proposal, req.body.proposalId)
                  }, {
                      $set: {
                        "status": "complete"
                      }
                    }, function (err, result) {
                      err ? cb(err) : cb(null);
                    })
                })
                // err?cb(err):cb(null);
              })
          })
        }
        else {
          Post.native(function (err, collection) {
            collection.findOne({
              _id: ModelService.ObjectId(Post, postId)
            }
              , function (err, result) {
                projectDetail = result;
                err ? cb(err) : cb(null);
              })

          })

        }
      }, function (cb) {
        User.native(function (err, collection) {
          var oid = new ObjectId();
          var query = {
            _id: ModelService.ObjectId(User, req.body.senderId)
          };
          collection.update(query, {
            $push: {
              notification: {
                type: "notification",
                sub_type: "approvedSignoff",
                status: "unread",
                msg: "client give sign off over your request",
                assignmentId: ModelService.ObjectId(Post, req.body.postId),
                proposalId: ModelService.ObjectId(Post, req.body.proposalId),
                user_id: ModelService.ObjectId(User, req.current_user.id),
                created_at: new Date(),
                _id: JSON.parse(JSON.stringify(oid))
              }
            }
          }, function (err, result) {
            err ? cb(err) : cb(null, result, JSON.parse(JSON.stringify(oid)))
          })
        })
      }, function (result, notifyId, cb) {
        var userId = req.body.senderId;
        User.getNamePic(req.current_user.id)
          .then(function (_user) {
            var _obj = {
              sub_type: "approvedSignoff",
              msg: "client give sign off over your request",
              assignmentId: req.body.postId,
              proposalId: req.body.proposalId,
              firstName: _user.firstName,
              lastName: _user.lastName,
              companyName: _user.companyName,
              projectName: projectDetail.title,
              created_at: new Date(),
              notifyId: notifyId
            }
            NotificationService.newNotification(_obj, userId)
            cb(null)
          }, function (objE) {
            console.log("error--->", objE);
            cb(objE)
          })
      },
      function (cb) {
        var sender = req.body.senderId;
        User.native(function (err, collection) {
          collection.find({
            _id: ModelService.ObjectId(User, sender)
          })
            .toArray(function (err, result) {
              if (err) {
                cb(err);
              }
              senderDetail = {
                senderFirstName: result[0].firstName,
                senderLastName: result[0].lastName,
                senderCompanyName: result[0].companyName,
                email_id: result[0].email_id
              }
              cb(null)
            })
        })

      },
      function (cb) {
        var userId = req.current_user.id;;
        User.native(function (err, collection) {
          collection.find({
            _id: ModelService.ObjectId(User, userId)
          })
            .toArray(function (err, result) {
              if (err) {
                cb(err);
              }
              recieverDetail = {
                firstName: result[0].firstName,
                lastName: result[0].lastName,
                companyName: result[0].companyName
              }
              cb(null)
            })
        })

      },
      function (cb) {
        console.log("reqreqtitlereqtitle--->", req.body.title)
        var mailContentDynamic = EmailService.getMailHtml_clientSignoff(senderDetail.senderFirstName, senderDetail.senderLastName, recieverDetail.firstName, recieverDetail.lastName, projectTitle, milestoneName);
        var mailContent = MailTemp.style + MailTemp.content_start1 + mailContentDynamic + MailTemp.content_end2;
        EmailService.send('no-reply@feelance.co', senderDetail.email_id, 'Feelance | Payment success', mailContent).then(function (result) {
          console.log("result2333322" + JSON.stringify(result));
          if (!result.status) {
            var responseData = {
              user: "Mail not sent!",
              status: false
            }
            cb({ success: 'ERROR', responseData: responseData });
          }
          else
            var responseData = "Job Apply Successfully!"
          cb(null, { success: 'Success', responseData: responseData });
        })
      }
    ], function (err, result) {
      err ? res.status(500).send(err) : res.send(result);
    })
  },

  editProposal: function (req, res) {
    var projectDetail = {};
    async.waterfall([function (cb) {
      // new entry in proposal
      Post.native(function (err, collection) {
        collection.findOne({ _id: ModelService.ObjectId(Post, req.body.postId) }, function (err, result) {
          console.log("ppppp", result)
          if (err) {
            cb(err)
          }
          else {
            projectDetail = result;
            console.log("proposalcreate", result);
            cb(null, result)
          }
        })
      })
    },
    function (result, cb) {
      var estimated_hours = 0, estimated_cost = 0;
      console.log("proposalDetail1--", req.body);
      for (var i = 0; i < req.body.proposalDetail.length; i++) {
        console.log("proposalDetail", req.body.proposalDetail[i]);
        estimated_hours += parseInt(req.body.proposalDetail[i].time);
        estimated_cost += parseInt(req.body.proposalDetail[i].cost);
        var oid = new ObjectId();
        req.body.proposalDetail[i]._id = JSON.parse(JSON.stringify(oid))
      }
      console.log("req.body.projectDuration", req.body.projectDuration);
      console.log("estimated_cost", estimated_cost);
      console.log("estimated_hours", estimated_hours);
      // if(req.body.jobType=='FixedBid' && (req.body.projectDuration>=parseInt(result.time) || req.body.projectDuration!=estimated_hours)){
      //     return res.json(500,{ success: 'ERROR' ,responseData:"Project Duration is not in Range!"})
      // }
      if (req.body.jobType == 'FixedBid' && (req.body.proposalCost != result.cost || req.body.proposalCost != estimated_cost)) {
        return res.json(500, { success: 'ERROR', responseData: "Proposal Cost is not in Range!" })
      }
      Proposal.native(function (err, collection) {
        collection.update({
          _id: ModelService.ObjectId(Proposal, req.body.proposalId)
        }, {
            $set: {
              coverLetter: req.body.coverLetter,
              description: req.body.description,
              type: req.body.type,
              proposalDetail: req.body.proposalDetail
            }
          }, function (err, result) {
            if (err) {
              cb(err)
            }
            else {
              cb(null, result);
            }
          })
      })

    }
      ,
    function (result, cb) {
      User.native(function (err, collection) {
        var oid = new ObjectId();
        var query = {
          _id: ModelService.ObjectId(User, req.body.receiverId)
        };
        collection.update(query, {
          $push: {
            notification: {
              type: "notification",
              sub_type: "edit proposal",
              status: "unread",
              assignmentId: ModelService.ObjectId(Post, req.body.postId),
              proposalId: ModelService.ObjectId(Proposal, req.body.proposalId),
              user_id: ModelService.ObjectId(User, req.current_user.id),
              created_at: new Date(),
              _id: JSON.parse(JSON.stringify(oid))
            }
          }
        }, function (err, result) {
          err ? cb(err) : cb(null, result, oid)
        })
      })
    }, function (result, notifyId, cb) {
      var oid = new ObjectId();
      var _activityId = JSON.parse(JSON.stringify(oid))
      Proposal.native(function (err, collection) {
        collection.update({ _id: ModelService.ObjectId(Proposal, req.body.proposalId) }, { $push: { "recentActivity": { created_at: new Date(), type: "proposal", _id: _activityId, created_by: ModelService.ObjectId(User, req.current_user.id) } } }, function (err, result) {
          // console.log("activityresult-----------", result)
          err ? cb(err) : cb(null, result, notifyId)

        })
      })

    }, function (result, notifyId, cb) {
      var userId = req.body.receiverId;
      User.getNamePic(userId)
        .then(function (_user) {
          var _obj = {
            sub_type: "edit proposal",
            assignmentId: req.body.postId,
            proposalId: req.body.proposalId,
            firstName: _user.firstName,
            lastName: _user.lastName,
            companyName: _user.companyName,
            projectName: projectDetail.title,
            created_at: new Date(),
            notifyId: notifyId
          }
          NotificationService.newNotification(_obj, userId)
          cb(null, {})
        }, function (objE) {
          console.log("error--->", objE);
          cb(objE)
        })
    }], function (err, result) {
      err ? res.json(500, { success: 'ERROR', responseData: err }) : res.json(200, { success: 'Success', responseData: result })

    })

  },

  particular_post: function (req, res) {
    console.log("reeee", req.body)
    console.log("req.current_user.id", req.current_user.id)
    async.parallel({
      post: function (callback) {
        Post.native(function (err, collection) {
          collection.findOne({ _id: ModelService.ObjectId(Post, req.body.postId) }, function (err, result) {
            callback(null, result);
          })
        })
      },
      proposal: function (callback) {
        Proposal.native(function (err, collection) {
          collection.aggregate([
            {
              $match: { postId: ModelService.ObjectId(Post, req.body.postId) }
            }, {
              $lookup: {
                from: "user",
                localField: "senderId",
                foreignField: "_id",
                as: "senderDetail"
              }
            }
          ]).toArray(function (err, result) {
            callback(null, result);
          })
        })
      }
    }, function (err, results) {
      console.log("result--", results)
      if (err) {
        return res.send({ responseCode: 500, responseMessage: "server error" })
      }
      else {
        return res.send({ responseCode: 200, responseMessage: "sucess", result: results })
      }
      // err?res.status(500).send(err):res.send({result:results})
      // results now equals to: [one: 'abc\n', two: 'xyz\n']
    });

  },

  change_contract_status: function (req, res) {
    console.log("req=========================================>", req.body)
    var projectDetail = {};
    var senderDetail, recieverDetail;
    if (req.body.type == true) {
      async.waterfall([
        function (cb) {
          Post.native(function (err, collection) {
            collection.findOneAndUpdate({
              _id: ModelService.ObjectId(Post, req.body.postId)
            }, {
                $set: {
                  status: "askContract"
                }
              }, function (err, result) {
                console.log("change_contract_status-----", result.value)
                projectDetail = result.value;
                console.log("projectDetail", projectDetail);
                cb(null, result, projectDetail)
              })

          })

        },
        function (result, projectDetail, cb) {
          console.log("result--", result)
          Proposal.native(function (err, collection) {
            collection.findOneAndUpdate({
              _id: ModelService.ObjectId(Proposal, req.body.proposalId),
              postId: ModelService.ObjectId(Post, req.body.postId)
            }, {
                $set: {
                  status: "contractRequest"
                }
              }, function (err, result2) {
                cb(null, result2, projectDetail)

              })

          })

        },
        function (result, projectDetail, cb) {
          User.native(function (err, collection) {
            var oid = new ObjectId();
            var query = {
              _id: ModelService.ObjectId(User, req.body.senderId)
            };
            collection.update(query, {
              $push: {
                notification: {
                  type: "notification",
                  sub_type: "ask contract",
                  status: "unread",
                  assignmentId: ModelService.ObjectId(Post, req.body.postId),
                  proposalId: ModelService.ObjectId(Proposal, req.body.proposalId),
                  user_id: ModelService.ObjectId(User, req.current_user.id),
                  created_at: new Date(),
                  _id: JSON.parse(JSON.stringify(oid))
                }
              }
            }, function (err, result) {
              err ? cb(err) : cb(null, result, JSON.parse(JSON.stringify(oid)), projectDetail)
            })
          })
        },
        function (result, notifyId, projectDetail, cb) {
          console.log("qqqqqqq-----", projectDetail)
          var userId = req.body.senderId;
          User.getNamePic(req.current_user.id)
            .then(function (_user) {
              var _obj = {
                sub_type: "ask contract",
                assignmentId: req.body.postId,
                proposalId: req.body.proposalId,
                firstName: _user.firstName,
                lastName: _user.lastName,
                companyName: _user.companyName,
                projectName: projectDetail.title,
                created_at: new Date(),
                notifyId: notifyId
              }
              console.log("_obj_obj_obj_obj_obj_obj_obj", _obj)
              NotificationService.newNotification(_obj, userId)
              cb(null)
            }, function (objE) {
              console.log("error--->", objE);
              cb(objE)
            })
        },

        function (cb) {
          var sender = req.body.senderId;
          User.native(function (err, collection) {
            collection.find({
              _id: ModelService.ObjectId(User, sender)
            })
              .toArray(function (err, result) {
                if (err) {
                  cb(err);
                }
                senderDetail = {
                  senderFirstName: result[0].firstName,
                  senderLastName: result[0].lastName,
                  senderCompanyName: result[0].companyName,
                  email_id: result[0].email_id
                }
                cb(null)
              })
          })

        },
        function (cb) {
          var userId = req.current_user.id;;
          User.native(function (err, collection) {
            collection.find({
              _id: ModelService.ObjectId(User, userId)
            })
              .toArray(function (err, result) {
                if (err) {
                  cb(err);
                }
                recieverDetail = {
                  firstName: result[0].firstName,
                  lastName: result[0].lastName,
                  companyName: result[0].companyName
                }
                cb(null)
              })
          })

        },
        function (cb) {
          console.log("reqreqtitlereqtitle--->", req.body.title)
          // var mailContent=EmailService.getMailHtml_clientAskContract(senderDetail.senderFirstName,senderDetail.senderLastName,recieverDetail.firstName,recieverDetail.lastName,projectDetail.title);
          var mailContent = MailTemp.style + MailTemp.content_start1 + EmailService.getMailHtml_clientAskContract(senderDetail.senderFirstName, senderDetail.senderLastName, recieverDetail.firstName, recieverDetail.lastName, projectDetail.title) + MailTemp.content_end2;
          EmailService.send('no-reply@feelance.co', senderDetail.email_id, 'Feelance | Proposal accepted', mailContent).then(function (result) {
            console.log("result2333322" + JSON.stringify(result));
            if (!result.status) {
              var responseData = {
                user: "Mail not sent!",
                status: false
              }
              cb({ success: 'ERROR', responseData: responseData });
            }
            else
              var responseData = "Job Apply Successfully!"
            cb(null, { success: 'Success', responseData: responseData });
          })
        }


      ], function (err, success) {
        if (err) {
          return res.send({
            responseCode: 500,
            responseMessage: "server error"
          })
        } else {
          return res.send({
            responseCode: 200,
            responseMessage: "Done",
            result: success
          })
        }

      })



    }
    else {
      async.waterfall([
        function (cb) {
          Post.native(function (err, collection) {
            collection.findOneAndUpdate({
              _id: ModelService.ObjectId(Post, req.body.postId)
            }, {
                $set: {
                  status: "pending"
                }
              }, function (err, result) {
                projectDetail = result.value;
                cb(null, result, projectDetail)
              })

          })

        },
        function (result, projectDetail, cb) {
          console.log("result--", result)
          Proposal.native(function (err, collection) {
            collection.findOneAndUpdate({
              _id: ModelService.ObjectId(Post, req.body.proposalId),
              postId: ModelService.ObjectId(Post, req.body.postId)
            }, {
                $set: {
                  status: "pending"
                }
              }, function (err, result2) {
                cb(null, result2, projectDetail)

              })

          })

        },
        function (result, projectDetail, cb) {
          User.native(function (err, collection) {
            var oid = new ObjectId();
            var query = {
              _id: ModelService.ObjectId(User, req.body.senderId)
            };
            collection.update(query, {
              $push: {
                notification: {
                  type: "notification",
                  sub_type: "cancel contract",
                  status: "unread",
                  assignmentId: ModelService.ObjectId(Post, req.body.postId),
                  proposalId: ModelService.ObjectId(Proposal, req.body.proposalId),
                  user_id: ModelService.ObjectId(User, req.current_user.id),
                  created_at: new Date(),
                  _id: JSON.parse(JSON.stringify(oid))
                }
              }
            }, function (err, result) {
              err ? cb(err) : cb(null, result, JSON.parse(JSON.stringify(oid)), projectDetail)
            })
          })
        },
        function (result, notifyId, projectDetail, cb) {
          console.log("qqqqqqq-----", projectDetail)
          var userId = req.body.receiverId;
          User.getNamePic(req.current_user.id)
            .then(function (_user) {
              var _obj = {
                sub_type: "cancel contract",
                postId: req.body.postId,
                proposalId: req.body.proposalId,
                firstName: _user.firstName,
                lastName: _user.lastName,
                companyName: _user.companyName,
                projectName: projectDetail.title,
                created_at: new Date(),
                notifyId: notifyId
              }
              NotificationService.newNotification(_obj, userId)
              cb(null, {})
            }, function (objE) {
              console.log("error--->", objE);
              cb(objE)
            })
        }


      ], function (err, success) {
        if (err) {
          return res.send({
            responseCode: 500,
            responseMessage: "server error"
          })
        } else {
          return res.send({
            responseCode: 200,
            responseMessage: "Done",
            result: success
          })
        }

      })

    }


  },


  accept_contract: function (req, res) {
    console.log("req----", req.body)
    var projectDetail = {};
    var senderDetail, recieverDetail;
    async.waterfall([

      function (cb) {

        Post.native(function (err, collection) {
          collection.findOneAndUpdate({
            _id: ModelService.ObjectId(Post, req.body.postId)
          }, {
              $set: {
                status: "contractSign",
                cost: req.body.proposalCost,
                // duration:req.body.proposalDuration
              }
            }, function (err, result) {
              projectDetail = result.value;
              cb(null, result)
            })

        })
      }

      , function (result, cb) {
        // console.log("result2",result2)
        Proposal.native(function (err, collection) {

          collection.updateMany({ postId: ModelService.ObjectId(Post, req.body.postId) }, { $set: { status: "reject" } }, { multi: true }, function (err, updated) {

            if (err) {
              cb(err)
            }
            cb(null, updated)
          })
        })

      },

      function (result3, cb) {
        Proposal.native(function (err, collection) {
          collection.findOneAndUpdate({ postId: ModelService.ObjectId(Post, req.body.postId), _id: ModelService.ObjectId(Proposal, req.body.proposalId), senderId: ModelService.ObjectId(Post, req.body.senderId) }, { $set: { status: "contractSign" } }, function (err, result4) {
            cb(null, result4)
          })
        })
      },
      function (result, cb) {
        User.native(function (err, collection) {
          var oid = new ObjectId();
          var query = {
            _id: ModelService.ObjectId(User, req.body.senderId)
            // _id:ModelService.ObjectId(User,req.body.receiverId)
          };
          collection.update(query, {
            $push: {
              notification: {
                type: "notification",
                sub_type: "contract sign",
                status: "unread",
                assignmentId: ModelService.ObjectId(Post, req.body.postId),
                proposalId: ModelService.ObjectId(Proposal, req.body.proposalId),
                user_id: ModelService.ObjectId(User, req.current_user.id),
                created_at: new Date(),
                _id: JSON.parse(JSON.stringify(oid))
              }
            }
          }, function (err, result) {
            err ? cb(err) : cb(null, result, JSON.parse(JSON.stringify(oid)))
          })
        })
      },
      function (result, notifyId, cb) {
        // var userId=req.body.receiverId;
        var userId = req.body.senderId;
        User.getNamePic(req.current_user.id)
          .then(function (_user) {
            var _obj = {
              sub_type: "contract sign",
              proposalId: req.body.proposalId,
              assignmentId: req.body.postId,
              firstName: _user.firstName,
              lastName: _user.lastName,
              companyName: _user.companyName,
              projectName: projectDetail.title,
              created_at: new Date(),
              notifyId: notifyId
            }
            NotificationService.newNotification(_obj, userId)
            cb(null)
          }, function (objE) {
            console.log("error--->", objE);
            cb(objE)
          })
      },

      function (cb) {
        // var sender=req.current_user.id;
        var sender = req.body.senderId;
        User.native(function (err, collection) {
          collection.find({
            _id: ModelService.ObjectId(User, sender)
          })
            .toArray(function (err, result) {
              if (err) {
                cb(err);
              }
              senderDetail = {
                senderFirstName: result[0].firstName,
                senderLastName: result[0].lastName,
                senderCompanyName: result[0].companyName,
                email_id: result[0].email_id
              }
              cb(null)
            })
        })

      },
      function (cb) {
        // var userId=req.body.receiverId;
        var userId = req.current_user.id
        User.native(function (err, collection) {
          collection.find({
            _id: ModelService.ObjectId(User, userId)
          })
            .toArray(function (err, result) {
              if (err) {
                cb(err);
              }
              recieverDetail = {
                firstName: result[0].firstName,
                lastName: result[0].lastName,
                companyName: result[0].companyName,
                email_id: result[0].email_id
              }
              cb(null)
            })
        })
        //senderFirstName,senderLastName,firstName,lastName,title

      },
      function (cb) {
        console.log("reqreqtitlereqtitle--->", req.body.title)
        console.log("recieverDetail.firstName--->", recieverDetail.firstName)
        console.log("senderDetail.senderFirstName--->", senderDetail.senderFirstName)
        console.log("senderDetail.senderFirstName--->", senderDetail)
        console.log("senderDetail.senderFirstName--->", senderDetail.email_id)

        var mailContentDynamic = EmailService.getMailHtml_clientAskContract(senderDetail.senderFirstName, senderDetail.senderLastName, recieverDetail.firstName, recieverDetail.lastName, projectDetail.title);
        var mailContent = MailTemp.style + MailTemp.content_start1 + mailContentDynamic + MailTemp.content_end2;
        EmailService.send('no-reply@feelance.co', senderDetail.email_id, 'Feelance | Proposal accepted', mailContent).then(function (result) {
          console.log("result2333322" + JSON.stringify(result));
          if (!result.status) {
            var responseData = {
              user: "Mail not sent!",
              status: false
            }
            cb({ success: 'ERROR', responseData: responseData });
          }
          else
            var responseData = "Job Apply Successfully!"
          cb(null, { success: 'Success', responseData: responseData });
        })
      }


    ], function (err, success) {
      if (err) {
        return res.send({
          responseCode: 500,
          responseMessage: "server error"
        })
      } else {
        return res.send({
          responseCode: 200,
          responseMessage: "Done",
          result: success
        })
      }


    })

  },

  consulatantCount: function (req, res) {
    console.log("req====>", req.current_user.id)
    async.parallel({
      totalprofit: function (callback) {
        Proposal.native(function (err, collection) {
          collection.aggregate([
            {
              $match: {
                senderId: ModelService.ObjectId(User, req.current_user.id)
              }
            },
            {
              $lookup: {
                from: "post",
                localField: "postId",
                foreignField: "_id",
                as: "postDetail"
              }

            }, {
              $addFields: {
                postDetail: { $arrayElemAt: ["$postDetail", 0] }
              }
            },
            {
              $match: {
                $or: [{ status: "complete" }, { status: "contractSign" }]
              }
            }

          ]).toArray(function (err, result) {
            if (result.length == 0) {
              callback(null, 0);
            }
            else {
              let sum = a => a.reduce((x, y) => x + y);
              let totalAmount = sum(result.map(x => Number(x.postDetail.cost)));
              console.log(totalAmount.toFixed(2))
              callback(null, totalAmount.toFixed(2));
            }
          })


        })

      },
      proposalSum: function (callback) {
        var query = [
          {
            $match: {
              senderId: ModelService.ObjectId(User, req.current_user.id),
              $or: [{ status: "complete" }, { "status": "contractSign" }]
            }
          }, {
            "$unwind": "$proposalDetail"
          },
          {
            $match: { 'proposalDetail.status': "complete" }
          }

        ]

        Proposal.native(function (err, collection) {
          collection.aggregate(query).toArray(function (err, result2) {
            // callback(null,result2)
            if (result2.length == 0) {
              callback(null, 0)
            }
            else {
              let sum1 = a1 => a1.reduce((x, y) => x + y);
              let totalAmount1 = sum1(result2.map(x => Number(x.proposalDetail.cost)));
              console.log(totalAmount1.toFixed(2))
              callback(null, totalAmount1.toFixed(2));
            }
          })

        })


      }

    }, function (err, results) {
      if (err) {
        return res.send({ responseCode: 500, responseMessage: "server error" })
      }
      else {
        return res.send({ responseCode: 200, responseMessage: "sucess", result: results })
      }
      // err?res.status(500).send(err):res.send({result:results})
      // results now equals to: [one: 'abc\n', two: 'xyz\n']
    });




  },

  billingProjects: function (req, res) {
    console.log("reqqq")
    Proposal.native(function (err, collection) {
      collection.aggregate([
        {
          $match: {
            senderId: ModelService.ObjectId(User, req.current_user.id)
          }
        },
        {
          $lookup: {
            from: "post",
            localField: "postId",
            foreignField: "_id",
            as: "postDetail"
          }

        }, {
          $addFields: {
            postDetail: { $arrayElemAt: ["$postDetail", 0] }
          }
        },
        {
          $match: {
            $or: [{ status: "complete" }, { status: "contractSign" }]
          }
        }

      ]).toArray(function (err, result) {
        if (err) {
          res.json(500, { success: 'ERROR', responseData: err })
        }
        else {
          res.json(200, { success: 'Success', responseData: result })
        }

      })


    })


  },


  totalOngoingProjects: function (req, res) {

  },

  find: function (req, res) {
    Proposal.find({}, function (err, result) {
      if (err) {
        res.json(500, { success: 'ERROR', responseData: err })
      }
      else {
        res.json(200, { success: 'Success', responseData: result })
      }
    })
  }
};

