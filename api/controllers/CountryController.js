/**
 * CountryController
 *
 * @description :: Server-side logic for managing countries
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	getCity:function(req,res){
		Country.native(function(err,collection){
			var query=[];
			if(req.params.id==-1){
				query=[{
					$unwind:"$states"
				},{
					$unwind:"$states.cities"
				}]
			}
			else{
				query=[{
					$match:{
						_id:ModelService.ObjectId(Country,req.params.id)
					}
				},{
					$unwind:"$states"
				},{
					$unwind:"$states.cities"
				}]
			}
			collection.aggregate(query)
			.toArray(function(err,result){
				err?res.status(500).send(err):res.send(result);
			})
		})
	}
};

