/**
 * ConngrpController
 *
 * @description :: Server-side logic for managing conngrps
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var async=require("async")
var ObjectId = require('mongodb').ObjectID; 
var mongodb = require('mongodb');
/*$or:[{
					$match:{
						admin_id:ModelService.ObjectId(User,req.body.current_user._id)
					}
				},{
					
				}]*/
module.exports = {
	/*
	
	*/
	
	suggestedGrp:function(req,res){
		Conngrp.native(function(err,collection){
			console.log("req.current_user---->",req.current_user)
			var skills=[];
			var query=[];
			if(req.current_user.skill_sets){
				skills=req.current_user.skill_sets;
				query=[{
					$match:{
					$nor:[{
						admin_id:ModelService.ObjectId(User,req.current_user.id)
					},{
						members: { 
							$elemMatch: { 
								_id: ModelService.ObjectId(User,req.current_user.id),
								status:"accept"
							}
						}
					}]
				}
				},{
					$sort:{
						createdAt:-1
					}
				},{
					$project:{
						createdAt:1,
						groupName:1,
						description:1,
						profile_pic:1
					}
				}]
			}
			else{
				query=[{
					$match:{
					$nor:[{
						admin_id:ModelService.ObjectId(User,req.current_user.id)
					},{
						members: { 
							$elemMatch: { 
								_id: ModelService.ObjectId(User,req.current_user.id),
								status:"accept"
							}
						}
					}]
				}
				},{
					$sort:{
						createdAt:-1
					}
				},{
					$project:{
						createdAt:1,
						groupName:1,
						description:1,
						profile_pic:1
					}
				}]
			}
			//return res.send(req.current_user)
			console.log("\n\n\n\n\n\n\nqueryqueryqueryqueryqueryquery--->",JSON.stringify(query))
			collection.aggregate(query)
			.toArray(function(err,result){
				err?res.status(500).send(err):res.send(result)
			})
		})
	},

	updateGrp:function(req,res){
		console.log("req-----",req.body)
		var newMembers=req.body.members.filter(x=>x.isNew==true).map(x=>{
			x.status = "pending"
			delete x.isNew;
			return x
		})
		async.waterfall([function(cb){
			Conngrp.native(function(err,collection){
			    collection.update({
			    	_id:ModelService.ObjectId(Conngrp,req.body.id)
			    },{
			    	$set:{
			    		groupName:req.body.groupName,
			    		description:req.body.description,
			    		groupTags:req.body.groupTags,
			    		profile_pic:req.body.profile_pic,
			    		members:req.body.members.map(x=>{
			    			x.status=x.isNew?"pending":"active";
							delete x.isNew
							return x
						})
			    	}
		    	},function(err,result){
	                if(err){
	                    console.error("Error on ContactService.createContact");
	                    console.error(err);
	                    console.error(JSON.stringify(err));
	                    cb(err)
	                    //return res.json(500, { success: 'ERROR' ,responseData:err});
	                }
	                else{
	                	// collection.update({_id:ModelService.ObjectId(Conngrp,req.body.id)},{$addToSet:{members:newMembers}},function(err,result){

	                 console.log("addPostaddPostaddPost--->",result);
	                  var responseData = "Post add Successfully!"
	                  cb(null,responseData)
	                	// })
	               
	                  //return res.json(200, { success: 'Success' ,responseData:responseData})
	                }
	                
	              }) 
			})
		},function(result,cb){
			User.native(function(err,collection){
				var query=[];
				if(newMembers.length==0)
					return cb(null,"Post add Successfully!")
				async.map(newMembers,function(x,cbMap){
					var oid=new ObjectId()
					collection.update({
						_id:ModelService.ObjectId(User,x._id)
					},{
						$push:{
							notification:{
								group_id:ModelService.ObjectId(Conngrp,result.id),
								type:"group join request",
								admin_id:ModelService.ObjectId(User,req.current_user.id),
								is_admin:false,
								created_at:new Date(),
								_id:JSON.parse(JSON.stringify(oid))
							}
						}
					},function(err,result){
						err?res.status(500).send(err):res.send(result)
					})
				},function(err,result){
					err?res.status(500).send(err):res.send(result)
				})
				// for(var i=0;i<newMembers.length;i++){
				// 	query.push({_id:ModelService.ObjectId(User,newMembers[i]._id)})
				// }
				// collection.update({
				// 	$or:query
				// },{
				// 	$push:{
				// 		notification:{
				// 			group_id:ModelService.ObjectId(Conngrp,req.body.id),
				// 			type:"group join request",
				// 			admin_id:ModelService.ObjectId(User,req.current_user.id),
				// 			is_admin:false,
				// 			created_at:new Date()
				// 		}
				// 	}
				// },function(err,result){
				// 	err?cb(err):cb(null,result)
				// })
			})
		}],function(err,result){
			err?res.status(500).send(err):res.send(result);
		})
		
	},
	connResponse:function(req,res){
		var userId=""
		if(req.body.userId){
			user_id =req.body.userId
		}else{
			user_id = req.current_user.id
		}
		async.waterfall([function(cb){
			Conngrp.native(function(err,conn){
				console.log("req.body.groupIdreq.body.groupIdreq.body.groupIdreq.body.groupId---->",req.body.groupId);
				console.log("req.current_user.idreq.current_user.idreq.current_user.id---->",req.current_user.id);
				conn.update({
					_id:ModelService.ObjectId(Conngrp,req.body.groupId),
					"members._id":ModelService.ObjectId(User, user_id)
				},{
					$set:{
						"members.$.status":req.body.type.toLowerCase()
					}
				},function(err,result){
					err?cb(err):cb(null,result)
				})
			})
			
		},function(result,cb){
			User.native(function(err,conn){
				conn.update({
					_id:ModelService.ObjectId(User,user_id)
				},{
					$pull:{
						notification:{
							group_id:ModelService.ObjectId(Conngrp,req.body.groupId)
						}
					}
				},function(err,result){
					err?cb(err):cb(null,result)
				})
			})
		}],function(err,result){
			err?res.status(500).send(err):res.send(result)
		})
	},
	grpJoinResponse:function(req,res){
		console.log("grpJoinResponse res, ", req.body)
		async.waterfall([function(cb){
			Conngrp.native(function(err,conn){
				console.log("req.body.groupIdreq.body.groupIdreq.body.groupIdreq.body.groupId---->",req.body.groupId);
				console.log("req.current_user.idreq.current_user.idreq.current_user.id---->",req.current_user.id);
				console.log("req.current_user.idreq.current_user.idreq.current_user.id---->",req.body);
				conn.update({
					_id:ModelService.ObjectId(Conngrp,req.body.groupId),
					"members._id":ModelService.ObjectId(User,req.body.userId)
				},{
					$set:{
						"members.$.status": req.body.type
					}
				},function(err,result){
					err?cb(err):cb(null,result)
				})
			})
			
		},function(result,cb){
			//cb(null,result);
			User.native(function(err,conn){
				conn.update({
					_id:ModelService.ObjectId(User,req.current_user.id)
				},{
					$pull:{
						notification:{
							_id:req.body.notification_id
						}
					}
				},function(err,result){
					err?cb(err):cb(null,result)
				})
			})
		}],function(err,result){
			err?res.status(500).send(err):res.send(result)
		})
	},
	find:function(req,res){
		console.log(req.current_user.id)
		Conngrp.native(function(err,collection){
			var query=[{
				$match:{
					$or:[{
						admin_id:ModelService.ObjectId(User,req.current_user.id)
					},{
						members: { 
							$elemMatch: { 
								_id: ModelService.ObjectId(User,req.current_user.id),
								status:"Accept"
							}
						}
					}]
				}
			},
			{
				$lookup:{
					localField:"admin_id",
					foreignField:"_id",
					as:"admin",
					from:"user"
				}
			},
			{
				$addFields:{
					admin_fName:{$arrayElemAt:["$admin.firstName",0]},
					admin_lName:{$arrayElemAt:["$admin.lastName",0]},
					admin_email_id:{$arrayElemAt:["$admin.email_id",0]},
					admin_salutation:{$arrayElemAt:["$admin.salutation",0]}
				}
			},{
				$project:{
					admin:0
				}
			}
			];
			console.log("query==========", JSON.stringify(query));
			collection.aggregate(query)
			.toArray(function(err,result){
				err?res.status(500).send(err):res.send(result);
			})
		})
	},
	create:function(req,res){
		console.log("dsda--->",req.body)
		// req.body.admin_id=ModelService.ObjectId(User,req.current_user.id);
		req.body.admin_id=req.current_user.id;
		req.body.members=req.body.members.map(x=>{
			x.status="pending"
			x._id=ModelService.ObjectId(User,x._id)
			return x;
		})
		async.waterfall([function(cb){
			console.log("hkhjk--->",req.body)
			Conngrp.create(req.body,function(err,result){
				err?cb(err):cb(null,result)
			})
		},function(result,cb){
			User.native(function(err,collection){
				var query=[];
				if(req.body.members.length==0){
					cb(null,{})
					return;
				}
				async.map(req.body.members,function(x,cbMap){
					var oid=new ObjectId()
					collection.update({
						_id:ModelService.ObjectId(User,x._id)
					},{
						$push:{
							notification:{
								group_id:ModelService.ObjectId(Conngrp,result.id),
								type:"group join request",
								admin_id:ModelService.ObjectId(User,req.current_user.id),
								is_admin:false,
								created_at:new Date(),
								_id:JSON.parse(JSON.stringify(oid))
							}
						}
					},function(err,result){
						err?cbMap(err):cbMap(null,result)
					})
				},function(err,result){
					err?cb(err):cb(null,result)
				})
				// for(var i=0;i<req.body.members.length;i++){
				// 	query.push({_id:ModelService.ObjectId(User,req.body.members[i]._id)});
					
				// }
				// collection.update({
				// 	$or:query
				// },{
				// 	$push:{
				// 		notification:{
				// 			group_id:ModelService.ObjectId(Conngrp,result.id),
				// 			type:"group join request",
				// 			admin_id:ModelService.ObjectId(User,req.current_user.id),
				// 			is_admin:false,
				// 			created_at:new Date()
				// 		}
				// 	}
				// },function(err,result){
				// 	err?res.status(500).send(err):res.send(result)
				// })
			})

		}],function(err,result){
			err?res.status(500).send(err):res.send(result)
		})
		

	},

	// createPost:function(req,res){
	// 	console.log("createPost",req.body);
	// 	var oid = new mongodb.ObjectID();
	// 	req.body.post.created_at=new Date();
	// 	req.body.post._id=JSON.parse(JSON.stringify(oid));
	// 	req.body.post.user_id=ModelService.ObjectId(User,req.current_user.id)
	// 	Conngrp.native(function(err,collection){
	// 	    collection.update({_id:ModelService.ObjectId(Conngrp,req.body._id)},{$push:{post:req.body.post}},function(err,result){
 //                if(err){
 //                    console.error("Error on ContactService.createContact");
 //                    console.error(err);
 //                    console.error(JSON.stringify(err));
 //                    return res.json(500, { success: 'ERROR' ,responseData:err});
 //                }
 //                else{
 //                  console.log("addPostaddPostaddPost--->",result);
 //                  var responseData = "Post add Successfully!"
 //                  return res.json(200, { success: 'Success' ,responseData:responseData})
 //                }
                
 //              }) 
	// 	})
	// },

	// groupInfo:function(req,res){

	// },
	// addComment:function(req,res){
	// 	var groupId=ModelService.ObjectId(Conngrp,req.body.groupId);
	// 	var postId=ModelService.ObjectId(Conngrp,req.body.postId);
	// 	var userId=ModelService.ObjectId(Conngrp,req.current_user.id);
	// 	console.log("groupId----> %s \n postId----> %s \n postId----> %s",req.body.groupId,req.body.postId,req.current_user.id)
	// 	Conngrp.native(function(err,collection){

	// 		collection.update({
	// 			_id:groupId,
	// 			post:{
	// 				$elemMatch:{
	// 					_id:req.body.postId
	// 				}
	// 			}
	// 		},{
	// 			$addToSet:{
	// 				"post.$.comments":{
	// 					text:req.body.comment,
	// 					userId:userId,
	// 					at:new Date()
	// 				}
	// 			}
	// 		},function(err,result){
	// 			err?res.status(500).send(err):res.send(result)
	// 		})
	// 	})
	// },
	postList:function(req,res){
		console.log("postListpostListpostListpostListpostListpostListpostList---->",req.body)
		// Conngrp.find({id:req.body._id})
		// .populate('admin_id')
		// .populate('post.comments.userId')
		// .exec(function(err,result){
		// 	err?res.status(500).send(err):res.send(result);
		// })
		Conngrp.native(function(err,collection){
             collection.aggregate([
	            { 
	                 $match: {
	                         _id:ModelService.ObjectId(Conngrp,req.body._id)
	                 }
	            },
				
	            {
				  $unwind:"$post"
				},
	            {
				   $lookup:
				     {
				       from:"user",
				       localField:"post.user_id",
				       foreignField:"_id",
				       as:"userInfo"
				     }
				},
				{
				   $project:
				   {
	                  post:1,
	                  groupName:1,
	                  firstName:{$arrayElemAt:[ "$userInfo.firstName", 0 ]},
	                  lastName:{$arrayElemAt:[ "$userInfo.lastName", 0 ]},
	                  companyName:{$arrayElemAt:[ "$userInfo.companyName", 0 ]},
	                  profile_pic:{$arrayElemAt:[ "$userInfo.profile_pic", 0 ]},
				   }
				},{ 
				    $sort :
				     { 
				       "post.created_at" : -1
				     }
			    }
			]).toArray(function(err,result1){
				async.map(result1,function(x,cbMap){
					if(!x.post.comments){
						console.log("if------->",x);
						cbMap(null,x)
					}
					else{
						console.log("else------->",x.comments)
						async.map(x.post.comments,function(y,cbMapY){
							var obj=Object.assign({}, y);
							console.log("obj---->",obj);
							User.findOne({_id:ModelService.ObjectId(User,y.userId)})
							.exec(function(err,result){
								obj.firstName=result.firstName
								obj.lastName=result.lastName
								obj.companyName=result.companyName
								obj.profile_pic=result.profile_pic
								err?cbMapY(err):cbMapY(null,obj)
							})
						},function(err,result){
							x.post.comments=result;
							err?cbMap(err):cbMap(null,x)
						})
					}
				},function(err,result){
					//result1.post=result
					err?res.status(500).send(err):res.send(result1);
				})
	          // console.log("getSkillSuggestion---->",result);
	          
	        })
	   })
	},
	groupJoinRequest:function(req,res){
		async.waterfall([function(cb){
			User.native(function(err,collection){
				var query={
					_id:ModelService.ObjectId(User,req.body.admin_id)
				};
				var oid=new ObjectId();
				collection.update(query,{
					$push:{
						notification:{
							group_id:ModelService.ObjectId(Conngrp,req.body.group_id),
							type:"user want to join",
							admin_id:ModelService.ObjectId(User,req.body.admin_id),
							user_id:ModelService.ObjectId(User,req.current_user.id),
							is_admin:true,
							created_at:new Date(),
							_id:JSON.parse(JSON.stringify(oid))
						}
					}
				},function(err,result){
					err?cb(err):cb(null,result)
				})
			})
		},function(result,cb){
			/*
req.body.members=req.body.members.map(x=>{
			x.status="pending"
			x._id=ModelService.ObjectId(User,x._id)
			return x;
		}
			*/
			Conngrp.native(function(err,collection){
				var query={
					_id:ModelService.ObjectId(Conngrp,req.body.group_id)
				};

				collection.update(query,{
					$addToSet:{
						members:{
							_id:ModelService.ObjectId(User,req.current_user.id),
							status:"pending",
						}
					}
				},function(err,result){
					err?cb(err):cb(null,result)
				})
			})
		}],function(err,result){
			err?res.status(500).send(err):res.send(result);
		})
		
	}
};

