/**
 * SupportController
 *
 * @description :: Server-side logic for managing supports
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var ObjectId = require('mongodb').ObjectID; 
var mongodb = require('mongodb');

module.exports = {
  supportList:function(req,res){
      Support.native(function(err,collection){
        collection.aggregate(
          {
            $lookup:
                 {
                   from: "user",
                   localField:"userId",
                   foreignField:"_id",
                   as:"userInfo"
                 },
          },
          {
            $project:
                {
                  "subject" :1 ,
                  "message" :1 ,
                  "read" : 1,
                  "starred" :1 ,
                  "label" :1,
                  "query_status" :1,
                  "comments" : 1,
                  "createdAt":1,
                  "userInfo":1
                }
          } ,function(err,result){
                       if(err){
                          console.log("dfsd",err);
                          return res.json(500, { success: 'ERROR' ,error:err});
                       }
                       else{
                          console.log("ListOfProposal",result);
                          return res.json(200, { success: 'Success' ,result:result})
                       }
          })
      })  
  },
	addComment:function(req,res) {
    var oid = new mongodb.ObjectID();
    
    console.log("addComment",req.body)
		 Support.native(function(err,collection){
     req.body.comments.created_at=new Date();
     req.body.comments._id=JSON.parse(JSON.stringify(oid));
     req.body.comments.created_by_id=ModelService.ObjectId(User,req.current_user.id);
     req.body.comments.created_by_name=req.current_user.firstName+" "+req.current_user.lastName;
              collection.update({_id:ModelService.ObjectId(Support,req.body._id)},{$push:{comments:req.body.comments}},function(err,result){
                if(err){
                    console.error("Error on ContactService.createContact");
                    console.error(err);
                    console.error(JSON.stringify(err));
                    return res.json(500, { success: 'ERROR' ,responseData:err});
                }
                else{
                  console.log("addCommentaddCommentaddCommentaddComment--->",result);
                  var responseData = "Comments add Successfully!"
                  return res.json(200, { success: 'Success' ,responseData:responseData})
                }
                
              })
              
            })
	}


};

