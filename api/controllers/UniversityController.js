/**
 * UniversityController
 *
 * @description :: Server-side logic for managing universities
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var ObjectId = require('mongodb').ObjectID;

module.exports = {
	find:function(req,res){
		console.log("find in university", req.body)
		University.native(function (err,collection) {
			var query=[{
				$match:{
					universityId:ModelService.ObjectId(User,req.body.id)
				}
			},{
				$unwind:"$studentDetails"
			},{

				$lookup:{
					  localField:"universityId",
			          foreignField:"_id",
			          from:"user",
			          as:"universityId"
				}
			},{
				$group:{
					_id:"$studentDetails.branch",
					totalStudent:{
						$sum:1
					},
					uploadResume:{
						$push:"$studentDetails.cvPath"
					}
				}
			},{
				$addFields:{
					pendingResume:{
						$subtract:["$totalStudent",{
							$size:"$uploadResume"
						}]
					}
				}
			}];

			// [{
			// 	$match:{
			// 		universityId:ObjectId("5abf6232de19a4fb20d2a662")
			// 	}
			// },{
			// 	$unwind:"$studentDetails"
			// },{
			// 	$group:{
			// 		_id:"$studentDetails.Branch",
			// 		totalStudent:{
			// 			$sum:1
			// 		},
			// 		uploadResume:{
			// 			$push:"$studentDetails.cvPath"
			// 		}
			// 	}
			// },{
			// 	$addFields:{
			// 		pendingResume:{
			// 			$subtract:["$totalStudent",{
			// 				$size:"$uploadResume"
			// 			}]
			// 		}
			// 	}
			// }]

			collection.aggregate(query)
			.toArray(function(er,result){
				err?res.status(500).send(err):res.send(result);
			})
			// body...
		})
	},


    clientUniversityFind:function(req,res){
		 console.log("find in university", req.body)

		 var query = [

		   {
		$unwind:"$studentDetails"
		},

		{ 
		$lookup:{
							  localField:"universityId",
					          foreignField:"_id",
					          from:"user",
					          as:"universityId"
						}
		},

		   { "$group": {
		        "_id": { 
		             universityId:"$universityId",
		            "branch": "$studentDetails.branch"

		        },
		        "count": { "$sum": 1 }
		      
		            
		    } }
		   
		 ]


		 University.native(function(err,collection){

		  collection.aggregate(query).toArray(function(err,result){

		err?res.status(500).send(err):res.send(result);
		  	
		  })

		 })
	

	},

	createStudent:function(req,res){
		 var oid = new ObjectId();
		 University.native(function(err,collection){

		 collection.findOne({universityId : ModelService.ObjectId(User,req.current_user.id)}, function(err, result){
		 	console.log("result111",result)
		 	if(err){
		 		console.log("unierror", err)
		 	}else if(!result){
		 		console.log("req.body in university create", req.body.studentDetails[0]);
		 		req.body.universityId = JSON.parse(JSON.stringify(ModelService.ObjectId(User, req.current_user.id)))

		 		req.body.studentDetails[0]={
		 			branch : req.body.studentDetails[0].branch,
		 			certification : req.body.studentDetails[0].certification,
		 			contact_Number : req.body.studentDetails[0].contact_Number,
		 			email_id : req.body.studentDetails[0].email_id,
		 			joining_Year : req.body.studentDetails[0].joining_Year,
		 			passout_Year : req.body.studentDetails[0].passout_Year,
		 			firstName : req.body.studentDetails[0].firstName,
		 			lastName : req.body.studentDetails[0].lastName,
		 			marks : req.body.studentDetails[0].marks,
		 			cvPath : req.body.studentDetails[0].cvPath,
		 			_id : JSON.parse(JSON.stringify(oid))
		 		}
		 			
		 			University.create(req.body).then(function(result){
		 				return res.json(200, { success: 'Done' ,responseData:result});
		 			})
		}else{
			// console.log("ajkfhsjkfhjkshfjksjkfhsjkfhjkshfjkshjkfh", req.current_user.id)
			req.body.studentDetails[0]={
		 			branch : req.body.studentDetails[0].branch,
		 			certification : req.body.studentDetails[0].certification,
		 			contact_Number : req.body.studentDetails[0].contact_Number,
		 			email_id : req.body.studentDetails[0].email_id,
		 			joining_Year : req.body.studentDetails[0].joining_Year,
		 			passout_Year : req.body.studentDetails[0].passout_Year,
		 			firstName : req.body.studentDetails[0].firstName,
		 			lastName : req.body.studentDetails[0].lastName,
		 			marks : req.body.studentDetails[0].marks,
		 			cvPath : req.body.studentDetails[0].cvPath,
		 			_id : JSON.parse(JSON.stringify(oid))
		 		}

			 University.native(function(err,collection){
              	collection.findOneAndUpdate({universityId:ModelService.ObjectId(User,result.universityId)},{$push:{studentDetails:req.body.studentDetails[0]}},function(err,result3){
                  return res.json(200, { success: 'Done' ,responseData:result3});
              	})
              })
		}		
		 	
		 })
		})
	},

	updateInternData : function(req,res){
		console.log("req.body in updateInternData", req.body);
		console.log("req.current_user.id",req.current_user.id)
		University.native(function(err, collection){
			collection.findOneAndUpdate({universityId: ModelService.ObjectId(User,req.current_user.id),'studentDetails._id':req.body.studentId},{$set:{'studentDetails.$':req.body.studentDetails}},function(err,result2){
				console.log("reulrt2",result2)
                         	if(err)
		 			return res.send({responseMessage:"server err",responseCode:500})
		 		else{
		 			return res.send({responseCode:200,responseMessage:"uploaded Successfully"})
		 		}

			})
		})
	},

	// editIntern : function(req, res){
	// 	console.log("req.body in upload resume", req.body);
	// 	console.log("req.current_user.id",req.current_user.id)
	// 	University.native(function(err, collection){
	// 		collection.findOneAndUpdate({universityId: ModelService.ObjectId(User,req.current_user.id),'studentDetails._id':req.body.studentId},{$set:{'studentDetails.$':req.body.studentDetails}},function(err,result2){
	// 			console.log("reulrt2",result2)
 //                         	if(err)
	// 	 			return res.send({responseMessage:"server err",responseCode:500})
	// 	 		else{
	// 	 			return res.send({responseCode:200,responseMessage:"uploaded Successfully"})
	// 	 		}

	// 		})
	// 	})
	// },


	// edit_university : function(req,res){
	// 	 var oid = new ObjectId();
	// 	 console.log("req",req.current_user.id,"param",req.params.id)
	// 	 University.findOne({universityId : ModelService.ObjectId(University,req.current_user.id)}, function(err, result){
	// 	 	console.log("ressss",result)
	// 	 	if(err){
	// 	 		console.log("unierror", err)
	// 	 	}
	// 		else{
	// 			University.native(function(err,collection){
	// 				collection.update({
	// 			        universityId:ModelService.ObjectId(University,req.current_user.id),
	// 			        studentDetails: {$elemMatch: {_id:req.params.id}}
	// 			      },{
	// 			        $set:{
	// 			        	"studentDetails.$":req.body
	// 			        }
	// 			      },function(err,result){
	// 			        err?res.status(500).send(err):res.send(result);
	// 			    })
	// 		    })
	// 		}			
	// 	 })
	// },

	delete_university : function(req,res){
		 var oid = new ObjectId();
		 console.log("req",req.current_user.id,"param",req.body._id)
		 University.native(function(err,collection){
		 	collection.findOneAndUpdate({universityId:ModelService.ObjectId(University,req.current_user.id),"studentDetails._id":req.body._id},{$set:{"studentDetails.$.status":req.body.status}},function(err,result){
		 		if(err)
		 			return res.send({responseMessage:"server err",responseCode:500})
		 		else{
		 			return res.send({responseCode:200,responseMessage:"Deleted Successfully"})
		 		}
		 	})
		 })
	},

	list_university : function(req,res){
	 	User.find({userType:"2"},function(err,result){
	 		if(err)
	 			return res.send({responseMessage:"server err",responseCode:500})
	 		else{
	 			return res.send({responseCode:200,responseMessage:"Success",result:result})
	 		}
	 	})
	},

	sendInternNotify : function(req, res){
		console.log("req.body.studentDetails[0].sendInternNotify", req.body);
		User.native(function(err, collection){
			var oid= new ObjectId();
			collection.update({_id:ModelService.ObjectId(User,req.body.university_id)}, {
				$push:{
		          notification:{
		            type:"intern request",
		            user_id:ModelService.ObjectId(User,req.current_user.id),
		            status: "pending",
		            created_at:new Date(),
		            intern_data: req.body.studentDetails[0].intern_data,
		            _id:JSON.parse(JSON.stringify(oid))
		          }
		        }
			}, function(err, result){
				console.log("result in universty", result);
			})
		})
	},

	// updateInternData : function(req, res){
	// 	console.log("deleteIntern", req.body);
	// },

	getBatchInternData : function(req, res){
		console.log("getBatchInternData", req.body);

		University.native(function(err, collection){

			collection.aggregate([{
				$match:{
					universityId: ModelService.ObjectId(User,req.body.id)
				}
			},
			{
				"$unwind":"$studentDetails"
			},{
				$match:{
					'studentDetails.branch':req.body.branch
				}
			}
			]).toArray(function(err,result){
				                  	if(err)
		 			return res.send({responseMessage:"server err",responseCode:500})
		 		else{
		 			return res.send({responseCode:200, result: result})
		 		}

			})
		})
	},


	searchIntern : function(req, res){
		University.native(function(err, collection){
			console.log("searchIntern", req.body)
			var pageNo= req.body.pageNo-1
			var count=0;
			var skipValue= pageNo*8;
			var totalPages=0
			collection.aggregate([{
				$match:{
					universityId: ModelService.ObjectId(User,req.body.id)
				}
			},
			{
				"$unwind":"$studentDetails"
			},{
				$match:{
				'studentDetails.branch':req.body.branch
				}
			}
			]).toArray(function(err, result){
				count=result.length;
				if(count%8 ==0){
					totalPages= count/8;
				}else{
					totalPages= (count/8) + 1;
				}
			})

			collection.aggregate([{
				$match:{
					universityId: ModelService.ObjectId(User,req.body.id)
				}
			},
			{
				"$unwind":"$studentDetails"
			},{
				$match:{
				'studentDetails.branch':req.body.branch
				}
			}
			]).skip(skipValue).limit(8).toArray(function(err, result){
				if(err)
					return res.send({responseMessage:"server err",responseCode:500})
				else{
					return res.send({responseCode:200, result: result, totalPages: totalPages, totalCount: count})
				}
			})
		})
	},

	uploadResume : function(req, res){
		console.log("req.body in upload resume", req.body);
		console.log("req.current_user.id",req.current_user.id)
		University.native(function(err, collection){
			collection.findOneAndUpdate({universityId: ModelService.ObjectId(User,req.current_user.id),'studentDetails._id':req.body.studentId},{$set:{'studentDetails.$.cvPath':req.body.cvPath}},function(err,result2){
				console.log("reulrt2",result2)
                         	if(err)
		 			return res.send({responseMessage:"server err",responseCode:500})
		 		else{
		 			return res.send({responseCode:200,responseMessage:"uploaded Successfully"})
		 		}

			})
		})

	},

	viewStudent : function(req,res){
   		console.log("req.current_user.id",req.current_user.id)
   		University.native(function(err,collection){

   collection.findOne({universityId:ModelService.ObjectId(User,req.current_user.id)},function(err,result){
        
               if(err){
		 			return res.send({responseMessage:"server err",responseCode:500})
               }
               else if(!result){
               	return res.send({responseCode:204,responseMessage:"No data found"})
               }
		 		else{
		 			return res.send({responseCode:200,responseMessage:"Successfully",result:result})
		 		}
   })

   		})

	},


	internFilter : function(req, res){
		console.log("internFilter", req.body.filter["marks"],  req.body)
		var arr=[];
		
		University.native(function(err, collection){
			for(var i=0;i<req.body.filter["marks"].length;i++){
	           arr=req.body.filter["marks"][i].split('~');
	        }
	        
	        console.log("araaysafhkjshfjkhs",  arr, arr.length , req.body.id, req.body.batch);
	        console.log("check",parseInt(arr[0]))

			var query=[{
				$match:{
					universityId: ModelService.ObjectId(User, req.body.id)
				}
			},{ 
				$unwind:"$studentDetails"
			},{
				 $match: { $and: [  {"studentDetails.marks": { $lte: arr[1], $gte: arr[0]} } , {"studentDetails.branch": req.body.batch}] }
				 
				 }]

				
			collection.aggregate(query).toArray(function(err, result){
				if(err){
					return res.send({responseMessage:"server err",responseCode:500})
				}else{
					console.log("result Successfully", result)
					return res.send({responseMessage:"success", result:result, responseCode:200})
				}
			})
		})
	},

	saveInternId : function(req, res){
		console.log("saveInternId")
		async.waterfall([
		    function(cb){
		      User.native(function(err, collection){
		        collection.findOne({_id: ModelService.ObjectId(User,req.current_user.id)}, function(err, result){
		        	console.log("bucket", result.bucket.university.downloadCV.count)
		          if(result.bucket.university.downloadCV.count < 0){
		             // User.native(function(err,collection){
		             //    collection.findOneAndUpdate({_id:ModelService.ObjectId(User,req.current_user.id)},{$set:{'bucket.recruiter.downloadCV.count':0,isActive:false}},function(err,result2){
		             //      cb(null,result2)
		             //    })
		             //  })
		          }else{
		            console.log("downloadCV", result.bucket)
		            User.native(function(err, collection){
		            collection.findOneAndUpdate({_id:ModelService.ObjectId(User,req.current_user.id)},{$addToSet:{downloadedCv:req.body.id}}, function(err, result1){
		              if(err){
		                cb(err);
		              }else{
		                 cb(null,result,result1);
		              }
		            })
		          })
		          }
		        })
		      })
		      }, function(universityResult,result,cb){
		        var count = universityResult.bucket.university.downloadCV.count -1
		        console.log("count in university", count, universityResult.bucket.university.downloadCV)
		        User.native(function(err,collection){
		          collection.findOneAndUpdate({_id:ModelService.ObjectId(User,req.current_user.id)},{$set:{'bucket.university.downloadCV.count':count}},function(err,result2){
		            cb(null,result2)
		          })
		        })
		      }
		           
		    ],function(err,result){
		      err?res.json(500, { success: 'ERROR' ,responseData:err}):res.json(200, { success: 'Success' ,responseData:"Resume Downloaded Successfully"})

		   }
		)
	},

	clientInternData : function(req, res){
		console.log("clientInternData", req.body)
async.waterfall([
   function(cb){
			var query = [
			 {
			  $match:{
			    email_id:"namrata@senzecit.com"
			  }
			  },
			  {
			  $unwind:"$downloadedCv"
			  },
			  {
			  $project:{
			  downloadedCv:1
			  }
			  },{
			  $group:{
			    _id:"$downloadedCv"
			  }
			  }
			 ]
 User.native(function(err,collection){
  collection.aggregate(query).toArray(function(err,result){
  cb(null,result)

  })

 })

   },
    function (xcldata, cb) {
                async.map(xcldata,function (x,cbMap) {
                    var query2=[{
							 "$unwind":"$studentDetails"
							 },{
							 $match:{
							 'studentDetails._id' : x._id
							 }
							 },{
							 	$lookup:{
							 		              localField:"universityId",
										          foreignField:"_id",
										          from:"user",
										          as:"universityId"
							 	}
							 },{
							 	$project : { 
									firstName : "$studentDetails.firstName",
									lastName : "$studentDetails.lastName",
									email_id : "$studentDetails.email_id",
									branch : "$studentDetails.branch",
									contact_Number : "$studentDetails.contact_Number",
									joining_Year : "$studentDetails.joining_Year",
									passout_Year : "$studentDetails.passout_Year",
									marks : "$studentDetails.marks",
									certification : "$studentDetails.certification",
									_id: "$studentDetails._id",
									cvPath : "$studentDetails.cvPath",
									universityName: "$universityId.universityName",
									universityId: "$universityId._id"
								}
							 }
							 ]
         University.native(function(err,collection){
       collection.aggregate(query2).toArray(function(err,result2){
       	  err ? cbMap(err, null) : cbMap(null, result2);
       })

         })

                    },

                    function (err, result) {
                        err ? cb(err, null) : cb(null, result);

                    })
          }




	],function(err,success){
		console.log("success----",success)
   if(err){
					return res.send({responseMessage:"server err",responseCode:500})
				}else{
		
					return res.send({responseMessage:"success", result:success, responseCode:200})
				}


	})
		
	}

   // find:function(req, res){
   // 	University.native(function(err, collection){
   // 		collection.find({universityId: ModelService.ObjectId(User,req.current_user.id)}).toArray(function(err, result){
   // 			if(err){
   // 				console.log("errrrrr", err)
   // 			}else{
   // 				return res.json(200, { success: 'Done' ,responseData:result});
   // 			}
   // 		})
   // 	})
   // }


};

