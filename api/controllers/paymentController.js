/**
 * PaymentController
 *
 * @description :: Server-side logic for managing posts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var ObjectId = require('mongodb').ObjectID;
var async = require('async');
var request = require('request');
var pdf = require('pdfcrowd');
var path = require('path');
var fs = require('fs');
var RazorpayUrl = sails.config.secrets.getRazorpayUrl();

const NumberToWords = require('../services/NumberToWords');
var cloudinary = require('cloudinary');
cloudinary.config({
  cloud_name: sails.config.secrets.cloudinary.name,
  api_key: sails.config.secrets.cloudinary.key,
  api_secret: sails.config.secrets.cloudinary.secret
});

module.exports = {
  orderIdgenrate: function (req, res) { //send ammount and razor pay Ids
    var obj = {
      "amount": req.body.amount,
      "currency": "INR"
    }
    request.post(RazorpayUrl + '/orders', { form: obj }, function (error, response, body) {
      var test = JSON.parse(body);
      console.log(test.id)
      if (error) {
        return res.send({ responseCode: 500, responseMessage: "server err" })
      }
      else {
        return res.send({ responseCode: 200, responseMessage: "Order Id generated sucessfully", result: test.id })
      }
    })
  },

  getAllTransactions: function (req, res) {
    console.log("getAllTransactions-------------")
    var perPageCount = 100;
    var pageNo = req.params.pageNo - 1;
    var count = 0;
    var skipValue = pageNo * perPageCount;
    var totalPages = 0

    var query = [
      { $match: { "proposalDetail.is_payment": true } },
      { $unwind: "$proposalDetail" },
      {
        $lookup: {
          from: "post",
          localField: "postId",
          foreignField: "_id",
          as: "post_details"
        }
      },
      { $unwind: "$post_details" },
      {
        $lookup: {
          from: "user",
          localField: "senderId",
          foreignField: "_id",
          as: "sender"
        }
      },
      { $unwind: "$sender" },
      {
        $lookup: {
          from: "user",
          localField: "receiverId",
          foreignField: "_id",
          as: "reciever"
        }
      },
      { $unwind: "$reciever" },
      {
        $project: {
          payment_id: "$proposalDetail.payment_id",
          cost: "$proposalDetail.cost",
          release_date: "$proposalDetail.releaseDate",
          milestone: "$proposalDetail.milestone",
          post_id: "$post_details._id",
          post: "$post_details.title",
          createdAt: "$createdAt",
          proposal_id: "$_id",
          _id: 1,
          subType: "$post_details.subType",
          reciever: {
            _id: "$reciever._id",
            user_name: "$reciever.firstName",
            last_name: "$reciever.lastName",
            email_id: "$reciever.email_id",
            mobile: "$reciever.mobile",
            userType: "$reciever.userType"
          },
          sender: {
            _id: "$sender._id",
            user_name: "$sender.firstName",
            last_name: "$sender.lastName",
            email_id: "$sender.email_id",
            mobile: "$sender.mobile",
            userType: "$sender.userType"
          }
        }
      },
      { $sort: { createdAt: -1 } }
    ]

    async.waterfall([
      function (cb) {
        Proposal.native(function (err, collection) {
          collection.aggregate(query).toArray(function (err, result1) {
            if (result1.length == 0)
              res.send({ responseData: result1, totalPage: 1, totalCount: 0, responseMessage: "No data found", responseCode: 200 })
            else if (result1.length) {
              count = result1.length;
              cb(null, count)
            }
          })
        })
      },
      function (countt, cb) {
        Proposal.native(function (err, collection) {
          collection.aggregate(query).skip(skipValue).limit(perPageCount).toArray(function (err, result2) {
            cb(null, null, result2)
          })
        })
      }, function (err, result) {
        err ? res.status(500).send(err) : res.send({ responseData: result, totalPage: totalPages, totalCount: count });
      }
    ])
  },

  savePayment: function (req, res) {
    console.log("savePayment > req.body: ", req.body);
    var senderDetail, projectTitle, txId, amount, rzpPaymentRes, ProposalData;
    async.waterfall([
      function (cb) {
        console.log("get post title from db...")
        // new entry in proposal
        Post.native(function (err, collection) {
          collection.findOne({ _id: ModelService.ObjectId(Post, req.body.postId) }, { title: 1 }, function (err, result) {
            if (err) {
              cb(err)
            }
            else {
              console.log("Post details: ", result);
              projectTitle = result.title;
              cb(null, result)
            }
          })
        })
      },
      function (result, cb) {
        console.log(`paymentController > savePayment > Retrieve a Payment by ID: `, RazorpayUrl + '/payments/' + req.body.payment_id);
        request(RazorpayUrl + '/payments/' + req.body.payment_id, function (error, response, body) {
          rzpPaymentRes = JSON.parse(body);
          console.log(`Razorpay payment response: `, rzpPaymentRes);
          var query = [
            { $match: { _id: ModelService.ObjectId(Proposal, req.body.proposalId) } },
            { "$unwind": "$proposalDetail" },
            {
              $lookup: {
                from: "post",
                localField: "postId",
                foreignField: "_id",
                as: "post_details"
              }
            },
            {
              $lookup: {
                from: "user",
                localField: "senderId",
                foreignField: "_id",
                as: "sender"
              }
            },
            { "$match": { "proposalDetail._id": { "$in": req.body.proposalDetailId } } },
            /* {
              $project: {
                _id: 1,
                postId: "$postId",
                senderId: "$senderId",
                receiverId: "$receiverId",
                status: "$status",
                type: "$type",
                coverLetter: "$coverLetter",
                description: "$description",
                proposalDetail: "$proposalDetail",
                createdAt: "$createdAt",
  
                payment_id: "$proposalDetail.payment_id",
                cost: "$proposalDetail.cost",
                release_date: "$proposalDetail.releaseDate",
                milestone: "$proposalDetail.milestone",
  
                post_id: "$post_details._id",
                post: "$post_details.title",
                proposal_id: "$_id",
                subType: "$post_details.subType",
                sender: {
                  _id: "$sender._id",
                  user_name: "$sender.firstName",
                  last_name: "$sender.lastName",
                  email_id: "$sender.email_id",
                  userType: "$sender.userType",
                  companyName: "$sender.companyName",
                  invoiceAddress: "$sender.invoiceAddress",
                  billingAddress: "$sender.billingAddress"
                }
              }
            } */
          ];
          console.log('Proposal aggQuery: ', query);
          Proposal.native(function (err, collection) {
            collection.aggregate(query).toArray(function (err, result) {
              ProposalData = result;
              cb(null, rzpPaymentRes, ProposalData)
            })
          })
        })
      },
      function (rzpPaymentRes, ProposalData, cb) {
        console.log(`paymentController > savePayment > Capture a payment: `, RazorpayUrl + '/payments/' + req.body.payment_id + '/capture');
        request({
          method: 'POST',
          url: RazorpayUrl + '/payments/' + req.body.payment_id + '/capture',
          form: { amount: rzpPaymentRes.amount }
        }, function (error, response, body) {
          var rzpCaptureRes = JSON.parse(body);
          console.log(`Capture response: `, rzpCaptureRes);
          cb(null, rzpCaptureRes, rzpPaymentRes, ProposalData);
        });
        // cb(null, {}, rzpPaymentRes, ProposalData);
      },
      function (rzpCaptureRes, rzpPaymentRes, ProposalData, cb) {
        console.log('Updating Proposal document with razorpay details like payment_id, payment_response, cature_response...');
        console.log("Proposal collection data: ", ProposalData)
        for (var i = 0; i < ProposalData.length; i++) {
          if (ProposalData[i].proposalDetail.status == undefined) {
            console.log("If proposalDetail status is udefined")
            Proposal.native(function (err, collection) {
              // txId = rzpPaymentRes.id;
              // amount = rzpPaymentRes.amount;
              // console.log("rzpPaymentRes: ", rzpPaymentRes);
              collection.updateMany({ _id: ModelService.ObjectId(Proposal, req.body.proposalId), 'proposalDetail._id': ProposalData[i].proposalDetail._id }, { $set: { 'proposalDetail.$.payment_id': rzpPaymentRes.id, 'proposalDetail.$.is_payment': true, "proposalDetail.$.status": "start", 'proposalDetail.$.rzpPaymentResponse': rzpPaymentRes, 'proposalDetail.$.rzpCaptureResponse': rzpCaptureRes, } }, { multi: true }, function (err, result3) {

              })
            })
          }
          else if (ProposalData[i].proposalDetail.status == "askSignoff") {
            console.log("Elseif proposalDetail status is askSignoff")
            Proposal.native(function (err, collection) {
              collection.updateMany({ _id: ModelService.ObjectId(Proposal, req.body.proposalId), 'proposalDetail._id': ProposalData[i].proposalDetail._id }, { $set: { 'proposalDetail.$.payment_id': rzpPaymentRes.id, 'proposalDetail.$.is_payment': true, 'proposalDetail.$.rzpPaymentResponse': rzpPaymentRes, 'proposalDetail.$.rzpCaptureResponse': rzpCaptureRes } }, { multi: true }, function (err, result4) {
              })
            })
          }
        }
        cb(null, rzpPaymentRes)
      },
      function (result, cb) {
        console.log('storing user notificaiton in db...');
        User.native(function (err, collection) {
          var oid = new ObjectId();
          var query = { _id: ModelService.ObjectId(User, req.body.senderId) };
          collection.update(query, {
            $push: {
              notification: {
                type: "notification",
                sub_type: "payment",
                status: "unread",
                msg: "client save payment for consultant",
                assignmentId: ModelService.ObjectId(Post, req.body.postId),
                proposalId: ModelService.ObjectId(Post, req.body.proposalId),
                user_id: ModelService.ObjectId(User, req.current_user.id),
                created_at: new Date(),
                _id: JSON.parse(JSON.stringify(oid))
              }
            }
          }, function (err, result) {
            err ? cb(err) : cb(null, result, JSON.parse(JSON.stringify(oid)))
          })
        })
      }, function (result, notifyId, cb) {
        console.log('sending same notification through socket.io to user...');
        var userId = req.body.senderId;
        User.getNamePic(req.current_user.id).then(function (_user) {
          var _obj = {
            sub_type: "payment",
            msg: "client save payment for consultant",
            proposalId: req.body.proposalId,
            assignmentId: req.body.postId,
            firstName: _user.firstName,
            lastName: _user.lastName,
            companyName: _user.companyName,
            projectName: projectTitle,
            created_at: new Date(),
            notifyId: notifyId
          }
          NotificationService.newNotification(_obj, userId)
          cb(null)
        }, function (objE) {
          console.log("error--->", objE);
          cb(objE)
        })
      },
      function (cb) {
        console.log('getting user details from db...');
        var sender = req.current_user.id;
        User.native(function (err, collection) {
          collection.find({ _id: ModelService.ObjectId(User, sender) }).toArray(function (err, result) {
            if (err) {
              cb(err);
            }
            senderDetail = {
              firstName: result[0].firstName,
              lastName: result[0].lastName,
              companyName: result[0].companyName,
              email_id: result[0].email_id,
              address1: result[0].address1,
              address2: result[0].address2,
              city: result[0].city,
              pincode: result[0].pincode,
            }
            cb(null)
          })
        })
      },
      function (cb) {
        console.log('creating invoice...');
        // TODO: generate invoice here
        // cb(null)
        // console.log(`Near invoice rzpPaymentRes: `, rzpPaymentRes);
        // console.log(`Near invoice ProposalData: `, ProposalData);
        amount = rzpPaymentRes.amount / 100;
        txId = rzpPaymentRes.id;
        projectTitle = `${projectTitle} [Milestone: ${ProposalData[0].proposalDetail.milestone}]`;
        let amountInWords = NumberToWords.convertNumberToWords(amount);
        var htmlContent = `<div id='invoiceFormat'> <style type='text/css'> #invoiceFormat h4 { margin: 0px; } #invoiceFormat p { margin: 0px; padding: 5px 0px; } #invoiceFormat table tr td { padding: 5px 10px; /**/ min-height: 25px; } #invoiceFormat .border-tbl tr th { padding: 5px 0px; border: 2px solid #000; } #invoiceFormat .border-tbl tr td { border: 1px solid #000; } </style> <div style='width: 800px;  margin:0px auto; padding: 0px 10px;'> <h2 style='text-align: center;'>TAX INVOICE</h2> <table cellspacing='0' cellpadding='0' border='0' style='width: 100%;'> <tr> <td colspan='3' width='60%'> <div style='border: 2px solid #000; padding: 5px;'> <h4>Feelance Consultant International Pvt. Ltd.</h4> <p>1402 B Wing, ONE BKC,</p> <p>Bandra Kurla Complex</p> <p>Bandra East, Mumbai - 400051</p> </div> <div style='border: 2px solid #000; padding: 5px; margin-top: 10px;'> <h4>${senderDetail.firstName} ${senderDetail.lastName}</h4> <p>${senderDetail.companyName} ${senderDetail.address1} ${senderDetail.address2} ${senderDetail.city} ${senderDetail.pincode}</p> </div> </td> <td colspan='2' width='40%' valign='top'> <h4 style='text-align:center;'>Invoice No.</h4> </td> </tr> <tr> <td colspan='5'> <table cellpadding='0' cellspacing='0' width='100%' class='border-tbl'> <tr> <th width='15%'>S.No.</th> <th width='40%'>Description of Goods/Services</th> <th width='15%'>&nbsp;</th> <th width='15%'>HSN</th> <th width='15%'>Rate (in INR)</th> </tr> <tr> <td rowspan='3'>1</td> <td rowspan='3'>${projectTitle}</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>${amount}</td> </tr> <tr> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> </tr> <tr> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> </tr> <tr> <td>&nbsp;</td> <td align='right'> <h4 style='color: #257fc1;'>Total</h4> </td> <td>&nbsp;</td> <td>&nbsp;</td> <td>${amount}</td> </tr> <tr> <td>&nbsp;</td> <td align='right'> <h4 style='color: #257fc1;'>Grand Total</h4> </td> <td>&nbsp;</td> <td>&nbsp;</td> <td>${amount}</td> </tr> </table> </td> </tr> <tr> <td colspan='5'> <table cellpadding='0' cellspacing='0' width='100%'> <tr> <td width='50%' align='center'>Amount Chargeable (in words)</td> <td width='50%' align='center'> <h4 style='color: #257fc1;'>INR ${amountInWords} Only</h4> </td> </tr> </table> </td> </tr> <tr> <td colspan='5'> <table cellpadding='0' cellspacing='0' width='100%' class='border-tbl'> <tr> <th width='15%'>Sl. No.</th> <th width='40%' colspan='2'>Tax Description</th> <th width='45%' colspan='3'>Appicable Tax</th> </tr> <tr> <td></td> <td>HSN</td> <td>Taxable Amount</td> <td bgcolor='#257fc1' style='color: #fff;'>IGST</td> <td>CGST</td> <td>SGST</td> </tr> <tr> <td colspan='3'></td> <td>(@ 18%)</td> <td>(@ 9%)</td> <td>(@ 9%)</td> </tr> <tr> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> <td>&nbsp;</td> </tr> </table> </td> </tr> <tr> <td colspan='5'> <table cellpadding='0' cellspacing='0' width='100%'> <tr> <td width='50%' align='center'>Amount Chargeable (in words)</td> <td width='50%' align='center'> <h4 style='color: #257fc1;'>INR ${amountInWords} Only</h4> </td> </tr> </table> </td> </tr> </table> <p>PAN No.: <span><strong>AACCF9548H</strong></span></p> <p>GST No.: <span><strong></strong></span></p> <div style='width: 60%; float: left; min-height: 75px;'> <p>Declaration</p> <p>We declare that this invoice shows the actual price of the goods/services described & that all particulars are true and correct.</p> </div> <div style='width: 40%; float: left; min-height: 75px;'> Feelance Consultant <div style='min-height: 50px; width: 100%; border: 2px solid #000; display: inline-block;'> </div> </div> <p style='margin-top: 10px;display: inline-block;'><strong>Payment Details as below:</strong></p> <p>Check in favour of '<strong>Feelance Consultants International Pvt. Ltd.</strong>' OR</p> <p><strong>NEFT/RTGS details as below</strong></p> <p>Account Name : Feelance Consultants International Pvt Ltd.</p> <p>Account No : 50200023755754<br>IFSC Code : HDFC0000542<br>MICR Code : 400240002</p> <hr style='height: 2px; background-color: #000;'> <h4 style='text-align:center'><strong>Feelance Consultants International Pvt. Ltd</strong></h4> <h6 style='text-align:center; margin:5px 0 0 0px; font-weight: normal'>Registered Office: 412 Trade World building, Kamla Mills, Lower Parel, Mumbai - 400013<br>Corporate Office: 1402 B Wing, ONE BKC, Bandra Kurla Complex, Bandra East, Mumbai - 400051</h6> </div> </div>`;
        var client = new pdf.Pdfcrowd(sails.config.secrets.pdfcredential.username, sails.config.secrets.pdfcredential.api_key);

        let randomNumber = Math.random().toString().substr(2, 10);       // generate 10 digit random number
        let invoiceFileName = `invoice_${randomNumber}`;
        let invoiceFileNameWithPath = path.join(process.env.PWD, 'temp', `${invoiceFileName}.pdf`);

        console.log(`PDF filename: `, invoiceFileNameWithPath);
        client.convertHtml(htmlContent, pdf.saveToFile(invoiceFileNameWithPath, function (err, fname) {
          console.log(`convertHTML > IsFileExists > ${fname}: `, fs.existsSync(invoiceFileNameWithPath));
          if (err)
            cb(null);
          else
            cb(null, invoiceFileName, invoiceFileNameWithPath);
        }));
      },
      function (invoiceFileName, invoiceFileNameWithPath, cb) {
        console.log('uploading invoice file to cloudinary account...');
        console.log(`IsFileExists: `, fs.existsSync(invoiceFileNameWithPath));
        cloudinary.uploader.upload(invoiceFileNameWithPath, function (result) {
          console.log(`cloudinary response: `, result);
          senderDetail.invoicePath = result.secure_url;
          cb(null, result.secure_url);
          // cb(null);
        }, { public_id: invoiceFileName });
      },
      function (invoicePath, cb) {
        console.log('updating invoicePath into db...');
        Proposal.native(function (err, collection) {
          collection.update({ _id: ModelService.ObjectId(Proposal, req.body.proposalId), 'proposalDetail._id': ProposalData[0].proposalDetail._id }, { $set: { 'proposalDetail.$.invoicePath': invoicePath } }, { new: true }, function (err, result) {
            cb(null);
          });
        });
      },
      function (cb) {
        console.log('sending email notification to user...');
        console.log(`getMailHtml_clientAddMoney > params: `, senderDetail, amount, txId, projectTitle);
        var mailContentDynamic = EmailService.getMailHtml_clientAddMoney(senderDetail.firstName, senderDetail.lastName, amount, txId, projectTitle, senderDetail.invoicePath);
        var mailContent = MailTemp.style + MailTemp.content_start1 + mailContentDynamic + MailTemp.content_end2;
        EmailService.send('no-reply@feelance.co', senderDetail.email_id, 'Feelance | Amount successfully added', mailContent).then(function (result) {
          console.log("Email sent status: " + JSON.stringify(result));
          if (!result.status) {
            var responseData = {
              user: "Mail not sent!",
              status: false
            }
            cb({ success: 'ERROR', responseData: responseData });
          }
          else
            var responseData = "Job Apply Successfully!"
          cb(null, { success: 'Success', responseData: responseData });
        })
      }
    ], function (err, result) {
      console.log('save payment Done...');
      err ? res.json(500, { success: 'ERROR', responseData: err }) : res.json(200, { success: 'Success', responseData: result })
    })
  },

  add_money_wallet: function (req, res) {
    console.log("paymentController > add_money_wallet: ", req.body)
    async.waterfall([
      function (cb) {
        User.native(function (err, collection) {
          collection.findOne({ _id: ModelService.ObjectId(User, req.current_user.id) }, function (err, result) {
            // console.log("ppppp",result)
            if (err) {
              cb(err)
            }
            else {
              cb(null, result)
            }
          })
        })
      },
      function (result, cb) {
        request(RazorpayUrl + '/payments/' + req.body.payment_id, function (error, response, body) {
          var test = JSON.parse(body);
          cb(null, result, test)
        })
      },
      function (UserResult, test, cb) {
        console.log("paymentController > add_money_wallet > UserResult: ", UserResult)
        var prevAmmount = UserResult.wallet_amt
        var newAmmount = prevAmmount + req.body.wallet_amt
        User.native(function (err, collection) {
          collection.update({ _id: UserResult._id }, { $set: { wallet_amt: newAmmount } }, function (err, finalResult) {
            cb(null, finalResult)
          })
        })
      }
    ], function (err, success, finalResult) {
      if (err) {
      }
      else {
        res.send({
          responseCode: 200,
          responseMessage: "Add Successfully",
          result: finalResult
        })
      }
    })
  }
};
