/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var ObjectId = require('mongodb').ObjectID;
var asyn = require('async');
var fs = require('fs');
var path = require("path");
var UniversityController = require('./UniversityController');
var bcrypt = require("bcryptjs")

module.exports = {

  userKyc: function (req, res) {
    User.native(function (err, collection) {
      collection.findOneAndUpdate({ _id: ModelService.ObjectId(User, req.current_user.id) }, { $set: { "kyc": req.body, "kyc_status": "uploaded" } }, function (err, result) {
        if (err) {
          return res.json(500, { success: 'ERROR', responseData: err });
        } else {
          return res.json(200, { success: 'Success', responseData: "Successfully KYC updated...!!!" });
        }
      })
    })
  },


  getAllUsersAdmin: function (req, res) {
    var pageNo = req.params.pageNo - 1
    var count = 0;
    var skipValue = pageNo * 100;
    var totalPages = 0


    User.native(function (err, collection) {
      async.waterfall([
        function (cb) {
          collection.find({ $or: [{ userType: "0" }, { userType: "1" }, { userType: "3" }, { userType: "6" }] }, { firstName: 1, lastName: 1, userType: 1, kyc: 1, email_id: 1, mobile: 1, kyc_status: 1 }).sort({ firstName: 1 }).toArray(function (err, result) {
            if (result.length == 0)
              res.send({ responseData: result, totalPage: 1, totalCount: 0, responseMessage: "No data found", responseCode: 200 })
            if (result.length) {
              count = result.length
              if (count % 100 == 0) {
                totalPages = count / 100
                cb(null, true)
              } else if (count / 100 < 1) {
                totalPages = 1
                cb(null, true)
              } else {
                totalPages = Math.ceil(count / 100)
                cb(null, true)
              }
            }

          })
        }, function (result, cb) {
          collection.find({ $or: [{ userType: "0" }, { userType: "1" }, { userType: "3" }, { userType: "6" }] }, { firstName: 1, lastName: 1, userType: 1, kyc: 1, email_id: 1, mobile: 1, kyc_status: 1 }).sort({ firstName: 1 }).skip(skipValue).limit(100).toArray(function (err, result1) {
            if (err) {
              cb(err)
            } else {
              cb(null, result1)
            }
          })
        }
      ], function (err, result) {
        // console.log("final result", result)
        err ? res.status(500).send(err) : res.send({ responseData: result, totalPage: totalPages, totalCount: count });
      })
    })
  },



  getAllCertificate: function (req, res) {
    var pageNo = req.params.pageNo - 1
    var count = 0;
    var skipValue = pageNo * 10;
    var totalPages = 0

    Certificate.native(function (err, collection) {
      async.waterfall([
        function (cb) {
          collection.find({}).toArray(function (err, result) {
            console.log("getAllUsersAdmin=============================>", result.length)
            if (result.length == 0)
              res.send({ responseData: result, totalPage: 1, totalCount: 0, responseMessage: "No data found", responseCode: 200 })
            if (result.length) {
              count = result.length
              if (count % 10 == 0) {
                totalPages = count / 10
                cb(null, true)
              } else if (count / 10 < 1) {
                totalPages = 1
                cb(null, true)
              } else {
                totalPages = Math.ceil(count / 10)
                cb(null, true)
              }
            }

          })
        }, function (result, cb) {
          collection.find({}).skip(skipValue).limit(10).toArray(function (err, result1) {
            console.log("----------------Second result----------->", result1.length)
            if (err) {
              cb(err)
            } else {
              cb(null, result1)
            }
          })
        }
      ], function (err, result) {
        console.log("final result", result)
        err ? res.status(500).send(err) : res.send({ responseData: result, totalPage: totalPages, totalCount: count });
      })
    })
  },




  getAllOrganisation: function (req, res) {
    var pageNo = req.params.pageNo - 1
    var count = 0;
    var skipValue = pageNo * 10;
    var totalPages = 0

    Organisation.native(function (err, collection) {
      async.waterfall([
        function (cb) {
          collection.find({}).toArray(function (err, result) {
            console.log("getAllUsersAdmin=============================>", result.length);
            if (result.length == 0)
              res.send({ responseData: result, totalPage: 1, totalCount: 0, responseMessage: "No data found", responseCode: 200 })
            if (result.length) {
              count = result.length
              if (count % 10 == 0) {
                totalPages = count / 10
                cb(null, true)
              } else if (count / 10 < 1) {
                totalPages = 1
                cb(null, true)
              } else {
                totalPages = Math.ceil(count / 10)
                cb(null, true)
              }
            }

          })
        }, function (result, cb) {
          collection.find({}).skip(skipValue).limit(10).toArray(function (err, result1) {
            console.log("----------------Second result----------->", result1.length)
            if (err) {
              cb(err)
            } else {
              cb(null, result1)
            }
          })
        }
      ], function (err, result) {
        console.log("final result", result)
        err ? res.status(500).send(err) : res.send({ responseData: result, totalPage: totalPages, totalCount: count });
      })
    })
  },


  getEduList: function (req, res) {
    // Education.native(function(err, collection){
    Education.find({}, function (err, result) {
      err ? res.send({ responseCode: 500, responseMessage: 'Server error' }) : res.send({ responseCode: 200, responseMessage: "Success", result: result })
    })
    // })
  },

  getAllEducation: function (req, res) {
    var pageNo = req.params.pageNo - 1
    var count = 0;
    var skipValue = pageNo * 10;
    var totalPages = 0

    Education.native(function (err, collection) {
      async.waterfall([
        function (cb) {
          collection.find({}).toArray(function (err, result) {
            console.log("getAllUsersAdmin=============================>", result.length);
            if (result.length == 0)
              res.send({ responseData: result, totalPage: 1, totalCount: 0, responseMessage: "No data found", responseCode: 200 })
            if (result.length) {
              count = result.length
              if (count % 10 == 0) {
                totalPages = count / 10
                cb(null, true)
              } else if (count / 10 < 1) {
                totalPages = 1
                cb(null, true)
              } else {
                totalPages = Math.ceil(count / 10)
                cb(null, true)
              }
            }

          })
        }, function (result, cb) {
          collection.find({}).skip(skipValue).limit(10).toArray(function (err, result1) {
            console.log("----------------Second result----------->", result1.length)
            if (err) {
              cb(err)
            } else {
              cb(null, result1)
            }
          })
        }
      ], function (err, result) {
        console.log("final result", result)
        err ? res.status(500).send(err) : res.send({ responseData: result, totalPage: totalPages, totalCount: count });
      })
    })
  },


  getAllndustry: function (req, res) {
    var pageNo = req.params.pageNo - 1
    var count = 0;
    var skipValue = pageNo * 10;
    var totalPages = 0

    Industry.native(function (err, collection) {
      async.waterfall([
        function (cb) {
          collection.find({}).toArray(function (err, result) {
            console.log("getAllUsersAdmin=============================>", result.length);
            if (result.length == 0)
              res.send({ responseData: result, totalPage: 1, totalCount: 0, responseMessage: "No data found", responseCode: 200 })
            if (result.length) {
              count = result.length
              if (count % 10 == 0) {
                totalPages = count / 10
                cb(null, true)
              } else if (count / 10 < 1) {
                totalPages = 1
                cb(null, true)
              } else {
                totalPages = Math.ceil(count / 10)
                cb(null, true)
              }
            }

          })
        }, function (result, cb) {
          collection.find({}).skip(skipValue).limit(10).toArray(function (err, result1) {
            console.log("----------------Second result----------->", result1.length)
            if (err) {
              cb(err)
            } else {
              cb(null, result1)
            }
          })
        }
      ], function (err, result) {
        console.log("final result", result)
        err ? res.status(500).send(err) : res.send({ responseData: result, totalPage: totalPages, totalCount: count });
      })
    })
  },


  getSubscription: function (req, res) {
    User.native(function (err, collection) {
      collection.findOne({ _id: ModelService.ObjectId(User, req.current_user.id) }, { bucket: 1 }, function (err, result) {
        if (err) {
          return res.json(500, { success: 'ERROR', responseData: err });
        } else {
          return res.json(200, { success: 'Success', responseData: result });
        }
      })
    })
  },
  usersSubscription: function (req, res) {
    User.native(function (err, collection) {
      collection.find({ $or: [{ userType: "0" }, { userType: "1" }, { userType: "3" }, { userType: "6" }] }, { bucket: 0, manPowers: 0, notification: 0, connections: 0, visitors: 0, reviews: 0, ratings: 0, professionalLink: 0, rewards: 0, downloadedCv: 0, portfolio: 0, languages: 0 })
        .toArray(function (err, result) {
          if (err) {
            return res.json(500, { success: 'ERROR', responseData: err });
          } else {
            return res.json(200, { success: 'Success', responseData: result });
          }
        })
    })
  },
  activatePremium: function (req, res) {
    User.native(function (err, collection) {
      collection.update({ _id: ModelService.ObjectId(User, req.body.id) }, {
        $set: {
          status: req.body.status
        }
      }, function (err, result) {
        if (err) {
          return res.json(500, { success: 'ERROR', responseData: err });
        } else {
          return res.json(200, { success: 'Success', responseData: result });
        }
      })
    })
  },
  saveSubscriptionId: function (req, res) {

    async.waterfall([
      function (cb) {
        User.native(function (err, collection) {
          collection.findOne({ _id: ModelService.ObjectId(User, req.current_user.id) }, function (err, result) {
            if (result.bucket.recruiter.downloadCV.count < 0) {
              // User.native(function(err,collection){
              //    collection.findOneAndUpdate({_id:ModelService.ObjectId(User,req.current_user.id)},{$set:{'bucket.recruiter.downloadCV.count':0,isActive:false}},function(err,result2){
              //      cb(null,result2)
              //    })
              //  })
            } else {
              console.log("downloadCV", result.bucket)
              User.native(function (err, collection) {
                collection.findOneAndUpdate({ _id: ModelService.ObjectId(User, req.current_user.id) }, { $addToSet: { downloadedCv: req.body.id } }, function (err, result1) {
                  if (err) {
                    cb(err);
                  } else {
                    cb(null, result, result1);
                  }
                })
              })
            }
          })
        })
      }, function (recruiterResult, result, cb) {
        var count = recruiterResult.bucket.recruiter.downloadCV.count - 1
        User.native(function (err, collection) {
          collection.findOneAndUpdate({ _id: ModelService.ObjectId(User, req.current_user.id) }, { $set: { 'bucket.recruiter.downloadCV.count': count } }, function (err, result2) {
            cb(null, result2)
          })
        })
      }

    ], function (err, result) {
      err ? res.json(500, { success: 'ERROR', responseData: err }) : res.json(200, { success: 'Success', responseData: result })

    })

  },
  getCount: function (req, res) {
    async.parallel({
      clientCount: function (cb) {
        User.native(function (err, connection) {
          User.count({ $or: [{ userType: '0' }, { userType: '1' }] }, function (err, result1) {
            err ? cb(err) : cb(null, result1)
          })
        })
      },
      postCount: function (cb) {
        Post.native(function (err, connection) {
          Post.count({}, function (err, result2) {
            err ? cb(err) : cb(null, result2)
          })
        })
      },
      consultantCount: function (cb) {
        User.native(function (err, connection) {
          User.count({ $or: [{ userType: '3' }, { userType: '6' }] }, function (err, result3) {
            err ? cb(err) : cb(null, result3)
          })
        })
      },
      ongoingJobCount: function (cb) {
        Proposal.native(function (err, connection) {
          Proposal.count({ "status": "contractSign" }, function (err, result4) {
            err ? cb(err) : cb(null, result4)
          })
        })
      },
      completedJobCount: function (cb) {
        Proposal.native(function (err, connection) {
          Proposal.count({ "status": "complete" }, function (err, result4) {
            err ? cb(err) : cb(null, result4)
          })
        })
      },
      industryCount: function (cb) {
        Industry.native(function (err, connection) {
          Industry.count({}, function (err, result5) {
            err ? cb(err) : cb(null, result5)
          })
        })
      }
    }, function (err, result) {
      err ? res.send({ responseCode: 500, responseMessage: "Server Error" }) : res.send({ responseCode: 200, result: result })
    })
  },


  saveSubscription: function (req, res) {
    console.log("saveSubscriptionnn current_user------------>", req.current_user.bucket)
    async.waterfall([
      function (cb) {
        User.native(function (err, collection) {
          collection.update({ _id: ModelService.ObjectId(User, req.current_user.id), subscription: { $exists: false } }, { $set: { subscription: [] } }, function (err, result) {
            err ? cb(err) : cb(null, result)
          })
        })
      },
      function (result1, cb) {
        //.payment_id
        //Subscription.find()
        Subscription.native(function (err, collection) {
          collection.find({ _id: ModelService.ObjectId(Subscription, req.body.subscriptionId) })
            .toArray(function (err, result) {
              console.log("Subscription---->", result)
              err ? cb(err) : cb(null, result)
            })
        })
      },
      function (packageDetails, cb) {
        console.log("packageDetails---->", packageDetails)
        RazorpayService.savePayment(req.body.payment_id).then(function (objS) {
          //console.log("savePayment",objS,packageDetails);
          cb(null, objS, packageDetails)
        }, function (objE) {
          cb(objE)
        })
      },
      function (result, packageDetails, cb) {
        User.native(function (err, collection) {
          var oid = new ObjectId();
          var obj = {
            status: "active",
            orderId: req.body.orderId,
            paymentId: result.id,
            subscriptionId: req.body.subscriptionId,
            _id: JSON.parse(JSON.stringify(oid))
          }
          var objSet = {
            client: {},
            recruiter: {},
            fos: {},
            subscriptionName: "",
            subscriptionDescription: "",
            subscriptionPrice: 0,
            duration: 0,
            durationType: "",
            expireOn: ""
          };

          if (req.current_user.bucket && !req.current_user.bucket.isFree) {
            var remainDays = 0;
            if (new Date(req.current_user.bucket.expireOn) > new Date()) {
              remainDays = new Date(req.current_user.bucket.expireOn) - new Date();
            }
            if (remainDays > 0) {
              objSet = req.current_user.bucket;
              console.log("objSet----------------------->", objSet)
              console.log("objSet----------------------->", objSet)
              objSet.client.viewUser.count += packageDetails[0].client.viewUser.count;
              objSet.client.downloadCV.count += packageDetails[0].client.downloadCV.count;
              objSet.client.jobPosted.count += packageDetails[0].client.jobPosted.count;
              objSet.client.jobPosted.totalCount += packageDetails[0].client.jobPosted.totalCount;

              objSet.recruiter.viewUser.count += packageDetails[0].recruiter.viewUser.count;
              objSet.recruiter.downloadCV.count += packageDetails[0].recruiter.downloadCV.count;
              objSet.recruiter.jobPosted.count += packageDetails[0].recruiter.jobPosted.count;

              objSet.fos.viewUser.count += packageDetails[0].fos.viewUser.count;
              objSet.fos.downloadCV.count += packageDetails[0].fos.downloadCV.count;
              objSet.fos.jobPosted.count += packageDetails[0].fos.jobPosted.count;

              objSet.subscriptionName = packageDetails[0].subscriptionName;
              objSet.subscriptionDescription = packageDetails[0].subscriptionDescription;
              objSet.subscriptionPrice = packageDetails[0].subscriptionPrice;

              var dt = new Date(req.current_user.bucket.expireOn);
              switch (packageDetails[0].durationType) {
                case "Month":
                  dt.setMonth(dt.getMonth() + packageDetails[0].duration);
                  break;
                case "Year":
                  dt.setYear(dt.getFullYear() + packageDetails[0].duration);
                  break;
                case "Week":
                  dt.setDate(dt.getDate() + 7 * packageDetails[0].duration);
                  break;
                case "Days":
                  dt.setDate(dt.getDate() + packageDetails[0].duration);
                  break;
              }
              objSet.expireOn = dt;

            }
            else {
              var dt = new Date();
              switch (packageDetails[0].durationType) {
                case "Month":
                  dt.setMonth(dt.getMonth() + packageDetails[0].duration);
                  break;
                case "Year":
                  dt.setYear(dt.getFullYear() + packageDetails[0].duration);
                  break;
                case "Week":
                  dt.setDate(dt.getDate() + 7 * packageDetails[0].duration);
                  break;
                case "Days":
                  dt.setDate(dt.getDate() + packageDetails[0].duration);
                  break;
              }
              objSet.expireOn = dt;
            }
          }
          else {
            objSet = packageDetails[0];
            objSet.client.jobPosted.count += req.current_user.bucket.client.jobPosted.count;
            objSet.client.jobPosted.totalCount += req.current_user.bucket.client.jobPosted.totalCount;
            var dt = new Date();
            switch (packageDetails[0].durationType) {
              case "Month":
                dt.setMonth(dt.getMonth() + packageDetails[0].duration);
                break;
              case "Year":
                dt.setYear(dt.getFullYear() + packageDetails[0].duration);
                break;
              case "Week":
                dt.setDate(dt.getDate() + 7 * packageDetails[0].duration);
                break;
              case "Days":
                dt.setDate(dt.getDate() + packageDetails[0].duration);
                break;
            }
            objSet.expireOn = dt;
          }

          console.log("obj------------>", obj);
          console.log("Final----------------->", objSet)
          collection.update({ _id: ModelService.ObjectId(User, req.current_user.id) }, {
            $push: {
              'subscription': obj
            },
            $set: {
              'bucket': objSet
            }
          }, function (err, result) {
            err ? cb(err) : cb(null, result)
          })
        })
      },
      function (result, cb) {
        console.log("resultresult123--->", result.subscription)
        var sender = req.current_user.id;
        User.native(function (err, collection) {
          collection.find({
            _id: ModelService.ObjectId(User, sender)
          })
            .toArray(function (err, result) {
              if (err) {
                cb(err);
              }
              senderDetail = {
                firstName: result[0].firstName,
                lastName: result[0].lastName,
                email_id: result[0].email_id
              }
              cb(null, {
                firstName: result[0].firstName,
                lastName: result[0].lastName,
                email_id: result[0].email_id
              });
            })
        })
      },
      function (_senderDetail, cb) {
        var mailContentDynamic = EmailService.getMailHtml_saveSubscription(_senderDetail.firstName, _senderDetail.lastName, req.body.subscriptionName);
        var mailContent = MailTemp.style + MailTemp.content_start1 + mailContentDynamic + MailTemp.content_end2;
        EmailService.send('no-reply@feelance.co', _senderDetail.email_id, 'Feelance | Subscription', mailContent).then(function (result) {
          console.log("result2333322" + JSON.stringify(result));
          if (!result.status) {
            var responseData = {
              user: "Mail not sent!",
              status: false
            }
            cb({ success: 'ERROR', responseData: responseData });
          }
          else
            var responseData = "Job Apply Successfully!"
          cb(null, { success: 'Success', responseData: responseData });
        })
      }
    ], function (err, result) {
      err ? res.json(500, { success: 'ERROR', responseData: err }) : res.json(200, { success: 'Success', responseData: result })

    })
  },

  projectDetails: function (req, res) {
    console.log("projectDetails", req.body)
  },


  advanceSearchSuggestion: function (req, res) {
    User.native(function (error, collection) {
      var query = [];
      var keyword = '\"' + req.params.keyword + '\"';
      // var keyword=req.params.keyword+".*";
      query.push({
        $match: {
          $text: {
            $search: req.params.keyword
          }
        }
      })
      query.push({
        $sort: {
          score: {
            $meta: "textScore"
          }
        }
      })
      query.push({
        $project: {
          firstName: 1,
          lastName: 1,
          companyName: 1,
          skill_sets: 1,
          _id: 1,
          city: 1,
          country: 1,
          profile_pic: 1,
          userType: 1,
          score: {
            $meta: "textScore"
          }
        }
      })
      console.log("query--->", JSON.stringify(query));
      collection.aggregate(query)
        .toArray(function (err, result) {
          err ? res.status(500).send(err) : res.send(result)
        })
    })
  },


  searchService: function (req, res) {
    console.log("req----------------", req.body);
    User.native(function (err, collection) {
      var query = [], keywordCond = [], location = [], industry = [], education = [], skill = [], userType = [], total_experience = [];

      if (req.body["keyword"]) {
        for (var i = 0; i < req.body["keyword"].length; i++) {
          var obj = {
            $regex: req.body["keyword"][i] + ".*",
            $options: "i"
          }
          keywordCond.push({
            firstName: obj
          })
          keywordCond.push({
            companyName: obj
          })
          keywordCond.push({
            country: obj
          })
          keywordCond.push({
            city: obj
          })

          console.log("obj---->", JSON.stringify(obj));
        }
      }
      if (req.body["location"]) {
        for (var i = 0; i < req.body["location"].length; i++) {
          var arr = req.body["location"][i].split(',');
          var city = {
            $regex: arr[0],
            $options: "i"
          };
          var country = {
            $regex: arr[1],
            $options: "i"
          };
          location.push({
            $and: [{
              "city": city
            }, {
              "country": country
            }]
          })
        }
      }
      if (req.body["industry"]) {
        for (var i = 0; i < req.body["industry"].length; i++) {
          var _industry = {
            $regex: req.body["industry"][i],
            $options: "i"
          };
          industry.push({
            "industry": _industry
          })
        }
      }
      if (req.body["education"]) {
        for (var i = 0; i < req.body["education"].length; i++) {
          var _education = {
            $regex: req.body["education"][i],
            $options: "i"
          };
          education.push({
            "education": _education
          })
        }
      }
      if (req.body["skill"]) {
        for (var i = 0; i < req.body["skill"].length; i++) {
          var _skill = {
            $regex: req.body["skill"][i],
            $options: "i"
          };
          // skill.push({
          //   "$in":[_skill,"$skill_sets"]
          // })

          skill.push(req.body["skill"][i])
        }
      }
      if (req.body["userType"]) {
        /*
        {"userType":["University","Contractor"]}
        */
        for (var i = 0; i < req.body["userType"].length; i++) {
          var _userType;
          switch (req.body["userType"][i]) {
            case "university":
              _userType = "2";
              break;

            case "Individual Consultant":
              _userType = "3";
              break;

            case "consultant firm":
              _userType = "6";
              break;

            case "contractor":
              _userType = "9";
              break;

            case "Client Company":
              _userType = "1"
              break;

            case "Individual Client":
              _userType = "0"
              break;
          }
          userType.push({
            "userType": _userType
          })
        }
      }

      if (req.body["experience"]) {
        /*
        {"userType":["University","Contractor"]}
// ["Fresher","0-3 years", "3-5 years", "5-10 years", "10-20 years", "20+ years"]

        */
        for (var i = 0; i < req.body["experience"].length; i++) {
          var _userType = -1;
          switch (req.body["experience"][i].toLowerCase()) {
            case "Fresher":
              _experience = "Fresher";
              break;

            case "0-3 years":
              _experience = "0-3 years";
              break;

            case "3-5 years":
              _experience = "3-5 years";
              break;

            case "5-10 years":
              _experience = "5-10 years";
              break;

            case "10-20 years":
              _experience = "10-20 years"
              break;


            case "20+ years":
              _experience = "20+ years"
              break;
          }
          total_experience.push(
            req.body["experience"][i].toLowerCase()
          )
        }
      }


      if (keywordCond.length == 0 && location.length == 0 && industry.length == 0 && userType.length == 0 && skill.length == 0 && total_experience.length == 0 && education.length == 0) {
        query.push({
          $sort: {
            avg_rating: -1
          }
        })
      }
      else {
        if (keywordCond.length > 0) {
          query.push({
            $match: {
              $or: keywordCond
            }
          })
        }
        if (location.length > 0) {
          query.push({
            $match: {
              $or: location
            }
          })
        }
        if (industry.length > 0) {
          query.push({
            $match: {
              $or: industry.map(x => { return { selectedIndustry: x.industry } })
            }
          })
        }
        if (education.length > 0) {
          query.push({
            $match: {
              $or: education.map(x => { return { 'education_info.degree': x.education } })
            }

          })
        }
        if (userType.length > 0) {
          query.push({
            $match: {

              $or: userType

            }
          })
        }
        if (skill.length > 0) {
          query.push({
            $match: {
              "skill_sets": {
                $in: skill
              }
            }
          })
        }

        if (total_experience.length > 0) {
          query.push({
            $match: {
              "total_experience": {
                $in: total_experience
              }
            }
          })
        }

      }
      //// Query region End /////

      console.log("queryqueryqueryqueryqueryquery--------------->", JSON.stringify(query));
      collection.aggregate(query)
        .toArray(function (err, result) {
          for (var i = 0; i < result.length; i++) {
            if (result[i].userType == "admin") {
              result.splice(i, 1)
            }

          }
          err ? res.status(500).send(err) : res.send(result);
        })
    })

  },

  searchServiceProvider: function (req, res) {
    User.native(function (err, collection) {
      var query = [], keywordCond = [], location = [], industry = [], userType = [];

      if (req.body["keyword"]) {
        for (var i = 0; i < req.body["keyword"].length; i++) {
          var obj = {
            $regex: req.body["keyword"][i] + ".*",
            $options: "i"
          }
          keywordCond.push({
            title: obj
          })
          keywordCond.push({
            description: obj
          })

          console.log("obj---->", JSON.stringify(obj));
        }
      }
      if (req.body["location"]) {
        for (var i = 0; i < req.body["location"].length; i++) {
          var arr = req.body["location"][i].split(',');
          var city = {
            $regex: arr[0],
            $options: "i"
          };
          var country = {
            $regex: arr[1],
            $options: "i"
          };
          location.push({
            $and: [{
              "post_user.city": city
            }, {
              "post_user.country": country
            }]
          })
        }
      }
      if (req.body["industry"]) {
        for (var i = 0; i < req.body["industry"].length; i++) {
          var _industry = {
            $regex: req.body["industry"][i],
            $options: "i"
          };
          industry.push({
            "post_user.industry": _industry
          })
        }
      }
      if (req.body["userType"]) {
        /*
        {"userType":["University","Contractor"]}
        */
        for (var i = 0; i < req.body["userType"].length; i++) {
          var _userType = -1;
          switch (req.body["userType"][i].toLowerCase()) {
            case "university":
              _userType = 2;
              break;

            case "consultant":
              _userType = 3;
              break;

            case "consultant firm":
              _userType = 6;
              break;

            case "contractor":
              _userType = 9;
              break;

            case "bulk contractor":
              _userType = 10
              break;
          }
          userType.push({
            "userType": _userType
          })
        }
      }


      //// Query region start /////
      query.push({
        $match: {
          $or: [{
            userType: { $ne: 0 }
          }, {
            userType: { $ne: 1 }
          }]
        }
      })
      if (keywordCond.length == 0 && location.length == 0 && industry.length == 0 && userType.length == 0) {
      }
      else {
        if (keywordCond.length > 0) {
          query.push({
            $match: {
              $or: keywordCond
            }
          })
        }
        if (location.length > 0) {
          query.push({
            $match: {
              $or: location
            }
          })
        }
        if (industry.length > 0) {
          query.push({
            $match: {
              $or: industry
            }
          })
        }
        if (userType.length > 0) {
          query.push({
            $match: {
              $or: userType
            }
          })
        }


        query.push({
          $addFields: {
            matchFound: {
              $reduce: {
                input: req.current_user.skill_sets,
                initialValue: 0,
                in: {
                  $cond: {
                    if: {
                      $eq: [{
                        $indexOfArray: [{ $ifNull: ["$skill_sets", []] }, "$$this"]
                      }, -1]
                    },
                    then: "$$value",
                    else: {
                      $sum: ["$$value", 1]
                    }
                  }
                }
              }
            }
          }
        },
          {
            $sort: {
              matchFound: -1
            }
          })
      }
      //// Query region End /////

      console.log("query---->", query);
      collection.aggregate(query)
        .toArray(function (err, result) {
          err ? res.status(500).send(err) : res.send(result);
        })
    })
  },
  studentBulkUpload: function (req, res) {
    console.log("req.body, studentBulkUpload", req.body.filesPath)
    async.waterfall([function (cb) {
      XmlParserService.convertToJson(req.body.filesPath)
        .then(function (objS) {
          cb(null, objS["Sheet1"])
        }, function (objE) {
          cb(objE, null)
        })
    },
    function (xclData, cb) {
      async.map(xclData, function (x, cbMap) {
        console.log("xxxx", x)
        var oid = new ObjectId();
        var query = {
          universityId: ModelService.ObjectId(User, req.current_user.id)
        };

        var obj = {
          //studentDetails:[{
          firstName: x["FirstName"],
          lastName: x["LastName"],
          email_id: x["Email_id"],
          branch: x["Branch"],
          contact_Number: x["Contact_Number"],
          joining_Year: x["Joining_Year"],
          passout_Year: x["Passout_Year"],
          marks: parseInt(x["Marks"]),
          certification: x["certification"],
          backlogs: x["backlogs"],
          _id: JSON.parse(JSON.stringify(oid))
          //   }]
        };

        console.log("objjj-----", req.current_user, obj)
        University.native(function (err, collection) {
          collection.findOne({ universityId: ModelService.ObjectId(User, req.current_user.id) }, function (err, result) {
            console.log("result---------->>>>>", result)
            if (!result) {
              console.log("enter first")
              University.native(function (err, collection) {
                collection.findOneAndUpdate(query, { $push: { studentDetails: obj } }, { upsert: true, new: true }, function (err, result3) {
                  err ? cbMap(err, null) : cbMap(null, result3);
                })
              })
            }
            else {
              console.log("enter secound")
              University.native(function (err, collection) {
                collection.findOneAndUpdate({ universityId: ModelService.ObjectId(User, result.universityId) }, { $push: { studentDetails: obj } }, function (err, result4) {
                  console.log("jkhfjkhsjkfhkshfksjkfhkshfhs", result4)
                  err ? cbMap(err, null) : cbMap(null, result4);
                })
              })
            }
          })
        })
      }, function (err, result) {
        err ? cb(err) : cb(null, result)
      })
    }
    ], function (err, result) {
      console.log(err)
      err ? res.status(500).send(err) : res.send(result)
    })
  },

  // Consultant (Freelancer) bulk upload
  userBulkUpload: function (req, res) {
    var successCount = 0, failCount = 0;
    async.waterfall([function (cb) {
      XmlParserService.convertToJson(req.body.filesPath)
        .then(function (objS) {
          cb(null, objS["Sheet1"])
        }, function (objE) {
          cb(objE, null)
        })
    },
    function (xclData, cb) {
      async.map(xclData, function (x, cbMap) {
        User.findOne({ email_id: x['email'] }, function (err, result) {
          if (err) {
            cb(err);
          } else if (!result) {
            console.log(`userBulkUpload > Email: ${x['email']} new user.`);
            // extract firstname and lastname from full name
            let name = x['full_name'];
            let arr = name.split(" ");
            let firstname = '', lastname = '';
            if (arr.length == 1) {
              firstname = arr[0];
            } else if (arr.length == 2) {
              firstname = arr[0];
              lastname = arr[1];
            } else if (arr.length > 2) {
              lastname = arr[arr.length - 1];
              arr.pop();
              firstname = arr.join(" ");
            }

            var obj = {
              firstName: firstname,
              lastName: lastname,
              email_id: x['email'],
              mobile: x['phone_number'],
              password: 'feelance@2019',
              profile_description: x['skills'],
              invoiceAddress: {
                "address": "",
                "countryName": "India",
                "city": x['city'],
                "postalCode": ""
              },
              selectedIndustry: x['category'],
              userType: x['type'] == 'individual' ? '3' : '6',  //Individual or Company
              emailAv: true,
              is_email_verified: 'active',
              bulkUploadFileName: req.body.filesPath,
              bucket: { "isActive": true }
            };

            User.create(obj).then(function (result) {
              console.log('userBulkUpload > User > create: ', result);
            });

            successCount++;
          } else {
            failCount++;
            console.log(`userBulkUpload > Filename: ${req.body.filesPath} > Email: ${x['email']} already exists.`);
          }
        });

        cb(null, 'done');
      })
    }], function (err, result) {
      console.log(`userBulkUpload > err: `, err);
      err ? res.status(400).send(err) : res.send(result)
    })
  },

  // Clinet (SMEs) bulk upload
  clientBulkUpload: function (req, res) {
    // console.log("req.body, clientBulkUpload", req.body.filesPath);
    var successCount = 0, failCount = 0;
    async.waterfall([function (cb) {
      XmlParserService.convertToJson(req.body.filesPath)
        .then(function (objS) {
          cb(null, objS["Sheet1"])
        }, function (objE) {
          cb(objE, null)
        })
    },
    function (xclData, cb) {
      async.map(xclData, function (x, cbMap) {
        User.findOne({ email_id: x['email'] }, function (err, result) {
          if (err) {
            cb(err);
          } else if (!result) {
            console.log(`clientBulkUpload > Email: ${x['email']} new user.`);
            // extract firstname and lastname from full name
            let name = x['full_name'];
            let arr = name.split(" ");
            let firstname = '', lastname = '';
            if (arr.length == 1) {
              firstname = arr[0];
            } else if (arr.length == 2) {
              firstname = arr[0];
              lastname = arr[1];
            } else if (arr.length > 2) {
              lastname = arr[arr.length - 1];
              arr.pop();
              firstname = arr.join(" ");
            }

            var obj = {
              firstName: firstname,
              lastName: lastname,
              email_id: x['email'],
              mobile: x['phone_number'],
              password: 'feelance@2019',
              description: x['skill'],
              city: x['city'],
              selectedIndustry: x['category'],
              userType: x['type'] == 'individual' ? '0' : '1',  //Individual or Company
              emailAv: true,
              is_email_verified: 'active',
              bulkUploadFileName: req.body.filesPath
              // bucket: { "_id": ObjectId("5b61aa2f043e48f405e19f51"), "client": { "viewUser": { "unlimitedView": false, "count": 0, "totalCount": 0, "isActive": true }, "downloadCV": { "unlimitedView": false, "count": 0, "totalCount": 0, "isActive": true }, "jobPosted": { "unlimitedView": false, "count": 10, "totalCount": 10, "isActive": true } }, "recruiter": { "viewUser": { "unlimitedView": false, "count": 0, "totalCount": 0, "isActive": true }, "downloadCV": { "unlimitedView": false, "count": 0, "totalCount": 0, "isActive": true }, "jobPosted": { "unlimitedView": false, "count": 0, "totalCount": 0, "isActive": true } }, "fos": { "viewUser": { "unlimitedView": false, "count": 0, "totalCount": 0, "isActive": true }, "downloadCV": { "unlimitedView": false, "count": 0, "totalCount": 0, "isActive": true }, "jobPosted": { "unlimitedView": false, "count": 0, "totalCount": 0, "isActive": true } }, "subscriptionName": "Free Subscription", "subscriptionDescription": "Free", "isFree": true, "duration": 10, "durationType": "Days", "createdAt": "2018-08-01T12:40:15.345Z", "updatedAt": "2018-08-11T05:31:23.140Z", "isActive": true }
            };

            User.create(obj).then(function (result) {
              console.log('clientBulkUpload > Client > create: ', result);
            });
          } else {
            failCount++;
            console.log(`clientBulkUpload > Filename: ${req.body.filesPath} > Email: ${x['email']} already exists.`);
          }
        });

        cb(null, 'done');
      })
    }], function (err, result) {
      console.log(`clientBulkUpload > err:`, err);
      err ? res.status(400).send(err) : res.send(result)
    })
  },

  basicDetail: function (req, res) {
    User.findOne({ _id: ModelService.ObjectId(User, req.params.id) })
      .exec(function (err, result) {
        //console.log("result----------------->",result)
        err ? res.status(500).send() : res.send({ val: result[req.params.colName] })
      })
  },

  adminFilter: function (req, res) {
    User.find({ $or: [{ "createdAt": { $gte: new Date(req.body.start_date), $lte: new Date(req.body.end_date) } }] }).exec(function (err, result) {
      if (err) {
        console.log("error", err)
      } else {
        console.log("result========", result);
        return res.send(result)
      }
    })
  },

  userTypeFilter: function (req, res) {
    var query = {};
    if (req.body.query == "Consultant") {
      query = { userType: "4" }
    } if (req.body.query == "Individual") {
      query = { userType: "0" }
    } if (req.body.query == "industry") {
      query = { industry: "mobile app and development" }
    }
    User.find(query).exec(function (err, result) {
      if (err) {
        console.log("error", err)
      } else {
        console.log("result==============", result)
        return res.send(result);
      }
    })
  },



  readNotification: function (req, res) {
    User.native(function (err, collection) {
      console.log("req.params.notifyId------>", req.params.notifyId)
      console.log("req.current_user.id)------>", req.current_user.id)
      collection.update({
        _id: ModelService.ObjectId(User, req.current_user.id),
        "notification._id": req.params.notifyId
      }, {
          $set: {
            "notification.$.status": "read"
          }
        }, function (err, result) {
          err ? res.status(500).send(err) : res.send(result);
        })
    })
  },
  notification: function (req, res) {
    User.native(function (err, collection) {
      var query = [{
        $match: {
          _id: ModelService.ObjectId(User, req.current_user.id)
        }
      }, {
        $unwind: "$notification"
      }, {
        $match: {
          "notification.type": "notification",
          "notification.status": "unread"
        }
      }, {
        $lookup: {
          localField: "notification.assignmentId",
          foreignField: "_id",
          as: "assignment",
          from: "post"
        }
      }, {
        $lookup: {
          localField: "notification.user_id",
          foreignField: "_id",
          as: "sender",
          from: "user"
        }
      }, {
        $project: {
          sub_type: "$notification.sub_type",
          assignmentId: "$notification.assignmentId",
          proposalId: "$notification.proposalId",
          firstName: { $arrayElemAt: ["$sender.firstName", 0] },
          lastName: { $arrayElemAt: ["$sender.lastName", 0] },
          companyName: { $arrayElemAt: ["$sender.companyName", 0] },
          projectName: { $arrayElemAt: ["$assignment.title", 0] },
          created_at: "$notification.created_at",
          notifyId: "$notification._id"
        }
      }, {
        $sort: {
          created_at: -1
        }
      }];
      /*
      <span class="content" ng-switch-when="'new assignment'"><b>{{x.firstName}} {{x.lastName}} {{x.companyName}}</b> want to assigned you a project named "{{x.projectName}}" </span>
                            <span class="date" ng-switch-when="'new assignment'">{{x.created_at |dtRoundoff}}</span>
      */
      collection.aggregate(query)
        .toArray(function (err, result) {
          err ? res.status(500).send(err) : res.send(result);
        })
    })
  },




  viewProfile: function (req, res) {
    User.native(function (err, collection) {
      collection.find({
        _id: ModelService.ObjectId(User, req.params.id)
      })
        .toArray(function (err, result) {
          err ? res.status(500).send(err) : res.send(result)
        })
    })
  },

  change_password: function (req, res) {
    console.log("request----", req.current_user);
    // res.send(req.current_user)
    User.comparePassword(req.body.oldPassword, req.current_user.password).then(function (objS) {
      bcrypt.hash(req.body.newPassword, 10, function (err, hash) {
        if (err) return res.status(500).send(err);
        User.native(function (err, collection) {
          collection.update({ _id: ModelService.ObjectId(User, req.current_user.id) }, { $set: { password: hash } }, function (err, result) {
            err ? res.status(500).send(err) : res.send("Password changed Successfully!")

          })
        })
      });

    }, function (objE) {
      res.status(500).send("Old password is not matched!")
    })

  },

  addReview: function (req, res) {
    User.native(function (err, collection) {
      collection.update({
        _id: ModelService.ObjectId(User, req.body.user_id)
      }, {
          $addToSet: {
            "reviews": {
              user_id: ModelService.ObjectId(User, req.current_user.id),
              review: req.body.review,
              at: new Date()
            }

          }
        }, function (err, result) {
          err ? res.status(500).send(err) : res.send(result)
        })
    })
  },

  addRating: function (req, res) {
    async.waterfall([function (cb) {
      User.findOne({ _id: ObjectId(req.body.user_id) }).exec(function (err, result) {
        console.log("result==========>", result)
        if (err)
          cb(err)
        else {
          //var avg_rating=result.avg_rating?result.avg_rating:0;
          var rating = result.ratings ? result.ratings : [];
          cb(null, rating)
        }
      })
    }, function (rating, cb) {
      var totalRating = 0;
      var len = rating.length;
      var indx = rating.findIndex(x => x.user_id == req.current_user.id)
      if (indx > -1)
        return cb(null, { msg: "user already give the review" })
      for (var i = 0; i < len; i++) {
        totalRating += rating[i].rate_number
      }

      totalRating = totalRating + req.body.rating;
      len = +1;
      var avg_rating = totalRating / len;
      User.native(function (err, collection) {
        collection.update({
          _id: ModelService.ObjectId(User, req.body.user_id)
        }, {
            $set: {
              avg_rating: avg_rating
            },
            $addToSet: {

              "ratings": {
                user_id: ModelService.ObjectId(User, req.current_user.id),
                rate_number: req.body.rating,
                at: new Date(),
                review: req.body.review
              }

            }
          }, function (err, result) {
            err ? cb(err) : cb(null, result)
          })
      })
    }], function (err, result) {
      err ? res.status(500).send(err) : res.send(result)
    })
  },
  getReview: function (req, res) {
    User.native(function (err, collection) {
      var query = [{
        $match: {
          _id: ModelService.ObjectId(User, req.body.user_id)
        }
      },
      {
        $unwind: "$reviews"
      },
      {
        $lookup: {
          localField: "reviews.user_id",
          foreignField: "_id",
          as: "user",
          from: "user"
        }
      },
      {
        $project: {
          user_id: { $arrayElemAt: ["$user._id", 0] },
          user_firstName: { $arrayElemAt: ["$user.firstName", 0] },
          user_lastName: { $arrayElemAt: ["$user.lastName", 0] },
          user_companyName: { $arrayElemAt: ["$user.companyName", 0] },
          user_profile_pic: { $arrayElemAt: ["$user.profile_pic", 0] },
          userType: { $arrayElemAt: ["$user.userType", 0] },
          // user_rating:{$arrayElemAt:["$user.avg_rating",0]},
          rating: {
            $arrayElemAt: ["$ratings.rate_number", {
              $indexOfArray: ["$ratings.user_id", "$reviews.user_id"]
            }]
          },
          reviews: 1

        }
      }];
      collection.aggregate(query)
        .toArray(function (err, result) {
          console.log("in getReview", result)
          /* user_id: ModelService.ObjectId(User,req.current_user.id),
                rate_number : req.body.rating,*/
          err ? result.status(500).send(err) : res.send(result);
        });
    })
  },
  connectFrndResponse: function (req, res) {
    async.waterfall([function (cb) {
      User.native(function (err, conn) {
        //console.log("req.body.groupIdreq.body.groupIdreq.body.groupIdreq.body.groupId---->",req.body.sender_id);

        if (req.body.status === "Accept") {
          console.log("if----------> Accept")
          //console.log("req.current_user.idreq.current_user.idreq.current_user.id---->",req.body.status);
          conn.update({
            _id: ModelService.ObjectId(User, req.current_user.id),
          }, {
              $addToSet: {
                "connections": ModelService.ObjectId(User, req.body.user_id)
              }
            }, function (err, result) {
              err ? cb(err) : cb(null, result)
            })
          conn.update({
            _id: ModelService.ObjectId(User, req.body.user_id),
          }, {
              $addToSet: {
                "connections": ModelService.ObjectId(User, req.current_user.id)
              }
            }, function (err, result) {
              err ? cb(err) : cb(null, result)
            })
        } else {
          cb(null, {})
          //return  res.json(500, {success:"Error"})
        }

      })

    }, function (result, cb) {
      User.native(function (err, conn) {
        conn.update({
          _id: ModelService.ObjectId(User, req.current_user.id)
        }, {
            $pull: {
              notification: {
                user_id: ModelService.ObjectId(User, req.body.user_id),
                type: "connection request"
              }
            }
          }, function (err, result) {
            err ? cb(err) : cb(null, result)
          })
      })
    }], function (err, result) {
      err ? res.status(500).send(err) : res.send(result)
    })
  },

  consultantList: function (req, res) {
    console.log(req.body);
    User.native(function (err, collection) {
      var query = [], keywordCond = [], location = [], industry = [], skill = [], userType = [];

      if (req.body["keyword"]) {
        for (var i = 0; i < req.body["keyword"].length; i++) {
          var obj = {
            $regex: req.body["keyword"][i] + ".*",
            $options: "i"
          }
          keywordCond.push({
            firstName: obj
          })
          keywordCond.push({
            companyName: obj
          })
          keywordCond.push({
            country: obj
          })
          keywordCond.push({
            city: obj
          })

          console.log("obj---->", JSON.stringify(obj));
        }
      }
      if (req.body["location"]) {
        for (var i = 0; i < req.body["location"].length; i++) {
          var arr = req.body["location"][i].split(',');
          var city = {
            $regex: arr[0],
            $options: "i"
          };
          var country = {
            $regex: arr[1],
            $options: "i"
          };
          location.push({
            $and: [{
              "city": city
            }, {
              "country": country
            }]
          })
        }
      }
      if (req.body["industry"]) {
        for (var i = 0; i < req.body["industry"].length; i++) {
          var _industry = {
            $regex: req.body["industry"][i],
            $options: "i"
          };
          industry.push({
            "industry": _industry
          })
        }
      }
      if (req.body["skill"]) {
        for (var i = 0; i < req.body["skill"].length; i++) {
          var _skill = {
            $regex: req.body["skill"][i],
            $options: "i"
          };
          // skill.push({
          //   "$in":[_skill,"$skill_sets"]
          // })

          skill.push(req.body["skill"][i])
        }
      }
      if (req.body["userType"]) {
        /*
        {"userType":["University","Contractor"]}
        */
        for (var i = 0; i < req.body["userType"].length; i++) {
          var _userType = -1;
          switch (req.body["userType"][i].toLowerCase()) {
            case "university":
              _userType = "2";
              break;

            case "consultant":
              _userType = "3";
              break;

            case "consultant firm":
              _userType = "6";
              break;

            case "contractor":
              _userType = "9";
              break;

            case "bulk contractor":
              _userType = "10"
              break;

            case "University":
              _userType = "2"
              break;
          }
          userType.push({
            "userType": _userType
          })
        }
      }

      //// Query region start /////
      query.push({
        $match: {
          $and: [{
            userType: { $ne: 0 }
          }, {
            userType: { $ne: 1 }
          }]
        }
      })
      if (keywordCond.length == 0 && location.length == 0 && industry.length == 0 && userType.length == 0 && skill.length == 0) {
        query.push({
          $sort: {
            avg_rating: -1
          }
        })
      }
      else {
        if (keywordCond.length > 0) {
          query.push({
            $match: {
              $or: keywordCond
            }
          })
        }
        if (location.length > 0) {
          query.push({
            $match: {
              $or: location
            }
          })
        }
        if (industry.length > 0) {
          query.push({
            $match: {
              $or: industry
            }
          })
        }
        if (userType.length > 0) {
          query.push({
            $match: {
              $or: userType
            }
          })
        }
        if (skill.length > 0) {
          query.push({
            $match: {
              "skill_sets": {
                $in: skill
              }
            }
          })
        }

      }
      //// Query region End /////

      console.log("queryqueryqueryqueryqueryquery--------------->", JSON.stringify(query));
      collection.aggregate(query)
        .toArray(function (err, result) {
          err ? res.status(500).send(err) : res.send(result);
        })
    })

  },

  connectFrndRequest: function (req, res) {
    req.body.sender_id = req.current_user.id;

    User.native(function (err, collection) {
      var query = [];
      var oid = new ObjectId();
      query.push();
      collection.update({ _id: ModelService.ObjectId(User, req.body.receiver_id) }, {
        $push: {
          notification: {
            type: "connection request",
            user_id: ModelService.ObjectId(User, req.current_user.id),
            status: "pending",
            created_at: new Date(),
            _id: JSON.parse(JSON.stringify(oid))
          }
        }
      }, function (err, result) {
        var userId = req.body.receiver_id;
        User.getNamePic(userId)
          .then(function (_user) {
            var _obj = {
              user_id: userId,
              user_firstName: _user.firstName,
              user_lastName: _user.lastName,
              user_companyName: _user.companyName,
              user_profile_pic: _user.profile_pic,
              type: "connection request",
              created_at: new Date(),
              notification_id: oid
            }
            NotificationService.newfriendRequest(_obj, userId)
            //cb(null,{})
          }, function (objE) {
            console.log("error--->", objE);
            //cb(objE)
          })
        err ? res.status(500).send(err) : res.send(result)
      })
    })
  },

  connRequest: function (req, res) {
    //return res.send([])
    User.native(function (err, collec) {
      var query = [{
        $match: {
          _id: ModelService.ObjectId(User, req.current_user.id)
        }
      }, {
        $unwind: "$notification"
      }, {
        $match: {
          $or: [{
            "notification.type": "connection request"
          }, {
            "notification.type": "user want to join"
          }, {
            "notification.type": "group join request"
          }]

        }
      }, {
        $lookup: {
          localField: "notification.group_id",
          foreignField: "_id",
          as: "grpDetail",
          from: "conngrp"
        }
      }, {
        $lookup: {
          localField: "notification.user_id",
          foreignField: "_id",
          as: "user",
          from: "user"
        }
      }, {
        $project: {
          user_id: { $arrayElemAt: ["$user._id", 0] },
          user_firstName: { $arrayElemAt: ["$user.firstName", 0] },
          user_lastName: { $arrayElemAt: ["$user.lastName", 0] },
          user_companyName: { $arrayElemAt: ["$user.companyName", 0] },
          user_profile_pic: { $arrayElemAt: ["$user.profile_pic", 0] },
          groupName: { $arrayElemAt: ["$grpDetail.groupName", 0] },
          grp_profile_pic: { $arrayElemAt: ["$grpDetail.profile_pic", 0] },
          grp_description: { $arrayElemAt: ["$grpDetail.description", 0] },
          type: "$notification.type",
          grp_id: { $arrayElemAt: ["$grpDetail._id", 0] },
          created_at: "$notification.created_at",
          notification_id: "$notification._id"
        }
      }];
      collec.aggregate(query)
        .toArray(function (err, result) {
          err ? res.status(500).send(err) : res.send(result)
        })
    })
  },

  getSkillSuggestion: function (req, res) {
    // console.log("suggestion start---------->")
    async.waterfall([function (cb) {
      User.native(function (err, collection) {
        var query = [];
        if (req.params.text == -1) {
          query = [{
            $unwind: "$skill_sets"
          }, {
            $group: {
              _id: "$skill_sets",
              count: { $sum: 1 }
            }
          }, {
            $sort: {
              count: 1
            }
          }];
        }
        else {
          query = [{
            $unwind: "$skill_sets"
          }, {
            $match: {
              skill_sets: {
                $regex: req.params.text.toLowerCase() + ".*",
                $options: "i"
              }
            }
          }, {
            $group: {
              _id: "$skill_sets",
              count: { $sum: 1 }
            }
          }, {
            $sort: {
              count: 1
            }
          }];
        }
        //console.log("get skill query 1------------>", JSON.stringify(query));
        collection.aggregate(query)
          .toArray(function (err, result) {
            // console.log("getSkillSuggestion---->", result)
            err ? cb(err) : cb(null, result);
          })
      })
    }, function (skills, cb) {
      Post.native(function (err, collection) {
        var query = [{
          $match: {
            type: "job"
          }
        }, {
          $unwind: "$skill_sets"
        }];
        if (req.params.text != -1) {
          query = [{
            $match: {
              skill_sets: {
                $regex: req.params.text.toLowerCase() + ".*",
                $options: "i"
              }
            }
          }];
        }
        query.push({
          $group: {
            _id: "$skill_sets",
            count: { $sum: 1 }
          }
        }, {
            $sort: {
              count: -1
            }
          })

        //console.log("get skill query 2------------>", JSON.stringify(query));
        collection.aggregate(query)
          .toArray(function (err, result) {
            err ? cb(err) : cb(null, result, skills);
          })
      })
    }], function (err, result, skills) {
      // console.log("get skill query 1------------>", JSON.stringify(result));
      // console.log("get skill query 2------------>", JSON.stringify(skills));
      var tags = [];
      for (var i = 0; i < result.length; i++) {
        // console.log("result[i]---->", result[i])
        if (typeof (result[i]._id) == "string") {
          var indx = tags.findIndex(x => x._id.toLowerCase() == result[i]._id.toLowerCase());
          if (indx == -1) {
            tags.push(result[i])
          }
          else {
            tags[indx].count += result[i].count;
          }
        }

      }
      for (var i = 0; i < skills.length; i++) {
        if (typeof (skills[i]._id) == "string") {
          var indx = tags.findIndex(x => x._id.toLowerCase() == skills[i]._id.toLowerCase());
          if (indx == -1) {
            tags.push(skills[i])
          }
          else {
            tags[indx].count += skills[i].count;
          }
        }
      }
      err ? res.status(500).send(err) : res.send(tags);
    })

    //res.send(contacts.filter(x=>x.name.toLowerCase().indexOf(req.params.text.toLowerCase())>-1));
  },
  invities: function (req, res) {
    User.native(function (err, collection) {
      var query = [{
        $match: {
          $and: [{
            _id: {
              $ne: ModelService.ObjectId(User, req.current_user.id)
            }
          }, {
            $or: [{
              firstName: {
                $regex: req.params.text.toLowerCase() + ".*",
                $options: "i"
              }
            }, {
              email_id: {
                $regex: req.params.text.toLowerCase() + ".*",
                $options: "i"
              }
            }]
          }]
        }
      }, {
        $project: {
          _id: 1,
          name: { $concat: ["$firstName", " ", "$lastName"] },
          email: "$email_id",
          image: {
            $arrayElemAt: ["$profile_pic.eager.url", 0]
          },
          image1: { $concat: ["http://res.cloudinary.com/dtgvop9lw/image/upload/c_fit,h_150,w_100/v", { $arrayElemAt: ["$profile_pic.eager.version", 0] }, { $arrayElemAt: ["$profile_pic.eager.public_id", 0] }, { $arrayElemAt: ["$profile_pic.eager.format", 0] }] }
        }
      }];
      collection.aggregate(query)
        .toArray(function (err, result) {
          err ? res.status(500).send(err) : res.send(result);
        })
    })
  },
  friendsCount: function (req, res) {
    User.native(function (err, collection) {
      collection.aggregate([{
        $match: {
          _id: ModelService.ObjectId(User, req.current_user.id)
        }
      }, {
        $unwind: "$connections"
      }, {
        $group: {
          _id: "null",
          count: { $sum: 1 }
        }
      }])
        .toArray(function (err, result) {
          err ? res.status(500).send(err) : res.send(result)
        })
    })
  },
  assignmentCount: function (req, res) {
    Post.native(function (err, collection) {
      collection.aggregate([{
        $match: {
          $and: [{ viwerType: { $exists: false } }, {
            $or: [{ assigneeId: ModelService.ObjectId(User, req.current_user.id) }, { userId: ModelService.ObjectId(User, req.current_user.id) }]
          }, {
            type: "job"
          }]
        }
      }, {
        $group: {
          _id: "null",
          count: { $sum: 1 }
        }
      }])
        .toArray(function (err, result) {
          err ? res.status(500).send(err) : res.send(result)
        })
    })
  },
  getFriends: function (req, res) {
    // console.log("req.params------------------------>", req.params)
    User.native(function (err, collection) {
      var query = [];
      if (req.params.text == 0 || !req.params.text) {
        // console.log("if in else------------------------------>")
        query = [{
          $match: {
            _id: ModelService.ObjectId(User, req.current_user.id)
          }
        }, {
          $unwind: "$connections"
        }, {
          $lookup: {
            localField: "connections",
            foreignField: "_id",
            from: "user",
            as: "friend"
          }
        }, {
          $project: {
            _id: {
              $arrayElemAt: ["$friend._id", 0]
            },
            firstName: { $arrayElemAt: ["$friend.firstName", 0] },
            lastName: { $arrayElemAt: ["$friend.lastName", 0] },
            companyName: { $arrayElemAt: ["$friend.companyName", 0] },
            userType: { $arrayElemAt: ["$friend.userType", 0] },
            avg_rating: { $arrayElemAt: ["$friend.avg_rating", 0] },
            email: {
              $arrayElemAt: ["$friend.email_id", 0]
            },
            image: {
              $arrayElemAt: ["$friend.profile_pic", 0]
            },
            hasConnection: { $eq: [1, 1] }
          }
        }];
      }
      else {
        // console.log("enter in else------------------------------>")
        query = [{
          $match: {
            $or: [{
              firstName: {
                $regex: req.params.text.toLowerCase() + ".*",
                $options: "i"
              }
            }, {
              email_id: {
                $regex: req.params.text.toLowerCase() + ".*",
                $options: "i"
              }
            }]
          }
        }, {
          $project: {
            _id: 1,
            firstName: 1,
            lastName: 1,
            companyName: 1,
            universityName: 1,
            userType: 1,
            avg_rating: 1,
            email: "$email_id",
            image: "$profile_pic",
            hasConnection: {
              $gt: [{
                $indexOfArray: ["$connections", ModelService.ObjectId(User, req.current_user.id)]
              }, -1]
            },
            hasPending: {
              $cond: {
                if: {
                  $eq: [{ $indexOfArray: ["$notification.user_id", ModelService.ObjectId(User, req.current_user.id)] }, -1]
                },
                then: false,
                else: {
                  $and: [{
                    $eq: [{
                      $arrayElemAt: ["$notification.type", {
                        $indexOfArray: ["$notification.user_id", ModelService.ObjectId(User, req.current_user.id)]
                      }]
                    }, "connection request"]
                  }, {
                    $eq: [{
                      $arrayElemAt: ["$notification.status", {
                        $indexOfArray: ["$notification.user_id", ModelService.ObjectId(User, req.current_user.id)]
                      }]
                    }, "pending"]
                  }]
                }
              }
            }
          }
        }];
      }
      // console.log("query---->", JSON.stringify(query));
      collection.aggregate(query)
        .toArray(function (err, result) {
          // console.log("resultresultresultresultresult--->", result)
          for (var i = 0; i < result.length; i++) {
            if (result[i].userType == "admin") {
              result.splice(i, 1)
            }

          }
          err ? res.status(500).send(err) : res.send(result);
        })
    })
  },
  companySuggestions: function (req, res) {
    User.native(function (err, collection) {
      var query = [{
        $unwind: "$employment_history"
      }, {
        $match: {
          "employment_history.company": {
            $regex: req.params.text.toLowerCase() + ".*",
            $options: "i"
          }
        }
      }, {
        $group: {
          _id: "$employment_history.company",
          count: { $sum: 1 }
        }
      }, {
        $sort: {
          count: 1
        }
      }];
      // console.log("companySuggestionscompanySuggestions--->", JSON.stringify(query));
      collection.aggregate(query)
        .toArray(function (err, result) {
          // console.log("getSkillSuggestion---->", result)
          err ? res.status(500).send(err) : res.send(result);
        })
    })
  },
  checkUsername: function (req, res) {
    User.native(function (err, collection) {
      collection.find({ firstName: { $regex: req.body.firstName, $options: "i" } }).toArray(function (err, result) {

        if (err) {
          return res.json(500, { success: 'ERROR', responseData: err });
        }
        else {
          if (result.length == 0) {
            return res.json(200, { success: 'Success', msg: "Username is available" })
            // resolve({msg:"Username is available"})
          }
          else {
            return res.json(401, { success: 'ERROR', msg: "Username is not available" })
            // resolve({msg:"Username is not available"})
          }
        }

      })
    })
  },

  checkEmail: function (req, res) {
    User.native(function (err, collection) {
      collection.find({ email_id: { $regex: req.body.email_id, $options: "i" } }).toArray(function (err, result) {
        if (err) {
          return res.json(500, { success: 'ERROR', responseData: err });
        }
        else {
          // console.log("result--->",result)
          if (result.length == 0) {
            return res.json(200, { success: 'Success', msg: "" })
            // resolve({msg:"Email is available"})
          }
          else {
            return res.json(200, { success: 'ERROR', msg: "This email is already exist" })
            // resolve({msg:"Email is not available"})
          }
        }
      })
    })
  },


  create: function (req, res) {
    // console.log("req.body---->", req.body);
    if (req.body.password !== req.body.confirmPassword) {
      var responseData = {
        user: "Password doesn't match",
        status: false
      }
      return res.json(401, { success: 'ERROR', responseData: responseData })
    }
    var code = CryptoService.generateCode(req.body.email_id);
    delete req.body.confirmPassword;
    sails.log.debug("code", code);
    req.body.code = code;
    async.waterfall([

      function (cb) {
        User.native(function (err, collection) {
          collection.findOne({ "email_id": req.body.email_id }, function (err, result) {
            if (err) {
              return res.json(500, { success: 'ERROR', msg: "Server error" })
            } else if (!result) {
              cb(null, result)
            } else {
              return res.json(200, { success: 'ERROR', msg: "This email is already exist" })
            }
          })
        })
      }, function (count, cb) {
        Subscription.native(function (err, collection) {
          collection.findOne({ "isFree": true }, function (err, result) {
            if (err) {
              // console.log("inside user subscription err---------->")
              cb(null, err, false)
            } else {
              // console.log("inside user subscription result1---------->")
              // console.log("Create Subscription--------------->", result);
              cb(null, result, true)
            }
          })
        })
      }, function (firstResult, isResult, cb) {
        // console.log("isResult------------>", isResult)
        // console.log("firstResult-------------->", firstResult)
        if (req.body.userType == 0 || req.body.userType == 1)
          req.body.bucket = firstResult
        req.body.bucket.isActive = true
        User.create(req.body).then(function (result) {
          // console.log("Contact created: ", result);
          sails.log.debug('Success------->', JSON.stringify(result));
          sails.log.debug("sdsd", sails.config.globals.sparkPostApiKey);

          cb(null, null, true)
        }, function (objE) {
          cb(null, null, false)
        })

      }
    ], function (err, result) {
      // console.log("result1 result----------------->", result)

      var mailerMsg = "";
      switch (req.body.userType) {
        case 0: mailerMsg = EmailService.getVerificationMailHtml_clientIndividual(req, req.body.email_id, code, req.body.firstName);
          break;
        case 1: mailerMsg = EmailService.getVerificationMailHtml_clientCompany(req, req.body.email_id, code, req.body.firstName);
          break;
        case 2: mailerMsg = EmailService.getVerificationMailHtml_consultant(req, req.body.email_id, code, req.body.firstName);
          break;
        case 3: mailerMsg = EmailService.getVerificationMailHtml_consultant(req, req.body.email_id, code, req.body.firstName);
          break;
        case 6: mailerMsg = EmailService.getVerificationMailHtml_consultantFirm(req, req.body.email_id, code, req.body.firstName);
          break;
        case 7: mailerMsg = EmailService.getVerificationMailHtml_fosIndividual(req, req.body.email_id, code, req.body.firstName);
          break;
        case 8: mailerMsg = EmailService.getVerificationMailHtml_fosCompany(req, req.body.email_id, code, req.body.firstName);
          break;
        case 9: mailerMsg = EmailService.getVerificationMailHtml_recruitmentIndividual(req, req.body.email_id, code, req.body.firstName);
          break;
        case 10: mailerMsg = EmailService.getVerificationMailHtml_recruitmentCompany(req, req.body.email_id, code, req.body.firstName);
          break;
      }


      var mailContent = MailTemp.style + MailTemp.content_start1 + mailerMsg + MailTemp.content_end22;
      EmailService.send('no-reply@feelance.co', req.body.email_id, 'Feelance | Account verification', mailContent).then(function (result) {
        // console.log("result2333322"+JSON.stringify(result));
        if (!result.status) {
          var responseData = {
            user: "Mail not sent!",
            status: false
          }
          return res.json(500, { success: 'ERROR', responseData: responseData });
        }
        else
          var responseData = "Thank you for registering with us. A verification mail is sent to your registered email address. Please click the link in the email to activate your account."
        return res.json(200, { success: 'Success', responseData: responseData });
      })
    })
  },


  activeAccount: function (req, res, next) {
    sails.log.debug("gfghghg", req.params.email_id);
    User.findOne({ email_id: req.params.email_id }).then(function (result) {
      sails.log.debug("gfghghg", result);
      if (!result) {
        return res.json(500, { status: false, response: 'Username not found.' })
      }

      if (result.is_email_verified != "pending") {
        // return res.json(500, { status: false, response: 'Account is either activated,rejected or blocked!' })
        return res.status(400).redirect('/#/home?status=' + 0)
      }
      if (result.code != req.params.code) {
        // return res.json(500, { status: false, response: 'Activation code not matched!' })
        return res.status(400).redirect('/#/home?status=' + 0)
      }
      else {
        User.update({ email_id: result.email_id }, { is_email_verified: "active", code: "0" }, function (err, result) {
          sails.log.debug("useruser", result);
          sails.log.debug("useruser", err);
          if (err) {
            return res.json(500, { status: false, response: 'Not Found!' })
          }
          else {
            return res.status(200).redirect('/#/home?status=' + 0)
          }
        })

      }
    })

  },

  login: function (req, res) {
     console.log("req.body-->", req.body);

    User.findOne({ email_id: req.body.email_id, $or: [{ userType: "0" }, { userType: "1" }, { userType: "3" }, { userType: "6" }] }).exec(function (err, result) {
      if (err) {
        console.log("login error: ", err);
        return res.json(500, "Internal server error");
      }
      if (!result) {
        return res.json(403, { response: "User doesn't exist." })
      }
      if (result.is_email_verified == 'pending') {
        return res.json(500, { status: false, response: 'Your account is not activated, Please check your registered email.' })
      }
      User.comparePassword(req.body.password, result.password).then(function (objS) {
        var responseData = {
          token: JwtService.issue({ id: result.id })
        }
        console.log('responseData:::', responseData)
        return res.json(200, { status: true, responseData: responseData });
      }, function (objE) {
        return res.json(403, { status: false, response: "Password is incorrect." });
      })
    });
  },

  adminLogin: function (req, res) {
    console.log("adminLogin---------------------->", req.body)
    User.findOne({ email_id: req.body.email_id }).exec(function (err, result) {
      if (err) {
        return res.json(500, "Internal server error");
      } else {
        console.log("loginUserDetail result----------------->", result)
        if (result.userType != "admin") {
          return res.json(403, { response: "User doesn't exist." })
        } else {
          User.comparePassword(req.body.password, result.password).then(function (objS) {
            var responseData = {
              token: JwtService.issue({ id: result.id })
            }
            return res.json(200, { status: true, responseData: responseData });
          }, function (objE) {
            return res.json(403, { status: false, response: "Password is incorrect." });
          })
        }
      }
    })
  },

  loginUserDetail: function (req, res) {
    res.send({
      createdAt: req.current_user.createdAt,
      emailAv: req.current_user.emailAv,
      email_id: req.current_user.email_id,
      firstName: req.current_user.firstName,
      hourlyRate: req.current_user.hourlyRate,
      id: req.current_user.id,
      is_email_verified: req.current_user.is_email_verified,
      lastName: req.current_user.lastName,
      profile_description: req.current_user.profile_description,
      profile_pic: req.current_user.profile_pic,
      salutation: req.current_user.salutation,
      bucket: req.current_user.bucket,
      userType: req.current_user.userType,
      cvUpload: req.current_user.cvUpload,
      companyName: req.current_user.companyName,
      isclientFirstLogin: req.current_user.isclientFirstLogin
    })
  },
  loginUserDetailComplete: function (req, res) {
    res.send(req.current_user)
  },
  forgotPassword: function (req, res, next) {
    console.log(req.body)
    User.native(function (err, collection) {
      collection.findOne({ email_id: req.body.email_id }, function (err, result) {
        if (!result) {
          return res.status(500).send({ status: false, response: 'Email is not registered' });
        }
        if (err) {
          return res.status(500).send({ status: false, response: "Internal Server Error" });
        }
        else {
          var user = result;
          console.log("useruser--->", user);
          var code = CryptoService.generateCode(result.email_id)
          console.log("code--->", code)
          console.log("email--->", req.body.email_id)
          User.update({ email_id: req.body.email_id }, { code: code })
            .exec(function (err, result) {
              if (err)
                res.status(500).send(err)
              else {
                var mailContentDynamic = EmailService.resetPasswordMailHtml(req, user.firstName, user.email_id, code);
                var mailContent = MailTemp.style + MailTemp.content_start1 + mailContentDynamic + MailTemp.content_end2;
                EmailService.send('no-reply@feelance.co', user.email_id, 'Feelance | Recover Password', mailContent).then(function (result) {
                  console.log("result2333322" + JSON.stringify(result));
                  if (!result.status) { return res.json({ status: false, response: 'Mail not sent!' }) }
                  else
                    res.json({ status: true, response: 'Recover Password, please check your mail inbox.' })
                })
              }
            })
        }
      });
    })
  },

  recoverPassword: function (req, res, next) {
    console.log("code--->", req.body.code);
    console.log('Password---->', req.body.password)
    console.log('email---->', req.body.email_id)
    User.native(function (err, collection) {
      collection.findOne({ email_id: req.body.email_id }
        , function (err, result) {
          if (err) {
            res.status(500).send(err);
          }
          else {
            var dbcode = result.code;
            var user = result;
            console.log("user-->", user)
            console.log("dbcode-->", dbcode)
            console.log("req.params.code-->", req.body.code)
            if (dbcode == req.body.code) {
              CryptoService.bEncode(req.body.password).then(function (result) {
                console.log("result", result);
                if (!result.status) { return res.json({ status: false, response: 'Hash not generated!' }) }

                var password_hash = result.hash;
                console.log("password_hash-->", password_hash)
                User.native(function (err, collection) {
                  collection.update({ email_id: user.email_id }, { $set: { password: password_hash, is_email_verified: "active" } }, { new: true }, function (err, result) {
                    console.log("result123-->", result);
                    if (!result) {
                      return res.json({ status: false, response: 'Password not changed!' })
                    }
                    console.log("1233444")
                    return res.status(200).send({ status: true, response: 'Password changed Successfully!' })
                  })
                })
              })
            }
            else {
              res.status(500).send({ response: "Url link expired." });
            }
          }
        })
    })
  },

  updateUserInfo: function (req, res) {
    console.log("req--------->", req.body)
    console.log("secoundRequest-----", req.body.obj.connections)
    delete req.body.obj.connections
    delete req.body.obj.reviews
    User.native(function (err, collection) {
      console.log("ModelServiceModelService0", ModelService.ObjectId)
      collection.update({
        _id: ModelService.ObjectId(User, req.current_user.id)
        // "email_id" : "namrata@senzecit.com"
      }, {
          $set: req.body.obj
        }, { new: true }, function (err, result) {
          if (err) {
            console.error("Error on ContactService.createContact");
            console.error(err);
            console.error(JSON.stringify(err));
            return res.json(500, { success: 'ERROR', responseData: err });
          }
          else {
            var responseData = "Education Info Update Successfully!"
            return res.json(200, { success: 'Success', responseData: responseData })
          }

        })

    })

  },

  removeManpower: function (req, res) {

    console.log("removeManpower", req.body)
    User.native(function (err, collection) {
      collection.update(
        { _id: ModelService.ObjectId(User, req.current_user.id) },
        { '$set': { "manPowers": req.body.manPowers } }, function (err, result) {
          console.log("result", result)
          if (err) {
            console.error(JSON.stringify(err));
            return res.json(500, { success: 'ERROR', responseData: err });
          }
          else {
            var responseData = "Remove Manpower Successfully!"
            return res.json(200, { success: 'Success', responseData: responseData })
          }

        })
    })
  },
  updateUserInfoviaAdmin: function (req, res) {
    console.log("updateUserInfoviaAdmin------------------>", req.body)
    User.native(function (err, collection) {
      var _Id = req.body.obj._id;
      delete req.body.obj._id;
      console.log("_id_id------------------------------>", _Id)
      console.log("ModelServiceModelService0", ModelService.ObjectId)
      collection.update({
        _id: ModelService.ObjectId(User, _Id)
        // "email_id" : "namrata@senzecit.com"
      }, {
          $set: req.body.obj
        }, { new: true }, function (err, result) {
          if (err) {
            console.error("Error on ContactService.createContact");
            console.error(err);
            console.error(JSON.stringify(err));
            return res.json(500, { success: 'ERROR', responseData: err });
          }
          else {
            var responseData = "Education Info Update Successfully!"
            return res.json(200, { success: 'Success', responseData: responseData })
          }

        })

    })

  },

  user_detail: function (req, res) {
    User.native(function (err, collection) {
      collection.findOne({ _id: ModelService.ObjectId(User, req.body.userId) }, function (err, result) {

        err ? res.json(500, err) : res.json(200, result)

      })


    })

  },

  update_kyc: function (req, res) {
    console.log("update_kyc=========================>", req.body)
    async.waterfall([
      function (cb) {
        User.native(function (err, collection) {
          collection.findOneAndUpdate({ _id: ModelService.ObjectId(User, req.body.userId) }, { $set: { kyc_status: "active" } }, function (err, result) {
            err ? cb(null, false) : cb(null, true)
            // cb(null, true)
          })
        })
      },
      function (isupdate, cb) {
        if (isupdate) {

          User.native(function (err, collection) {
            collection.findOne({ _id: ModelService.ObjectId(User, req.body.userId) }, function (err1, result1) {
              console.log("user result------------------------>", typeof result1)
              var mailContentDynamic = EmailService.getMailHtml_approveKycNotify(result1.firstName, result1.lastName);
              var mailContent = MailTemp.style + MailTemp.content_start1 + mailContentDynamic + MailTemp.content_end2;
              EmailService.send('no-reply@feelance.co', result1.email_id, 'Feelance | Bank details verified', mailContent).then(function (result) {
                if (!result.status) {
                  console.log("result status==============>", result.status)
                  var responseData = {
                    user: "Mail not sent!",
                    status: false
                  }
                  cb({ success: 'ERROR', responseData: responseData });
                }
                else {
                  console.log("result status==============>", result.status)
                  var responseData = "kyc Update Successfully"
                  cb(null, { success: 'Success', responseData: responseData });
                }

              })
            })
          })


        }
      }
    ], function (err, result) {
      console.log("kyc result----------------->", result)
      err ? res.json(500, err) : res.json(200, result)
    })

  },


  skill_detail: function (req, res) {
    console.log("req")
    User.native(function (err, collection) {
      collection.aggregate([
        {
          $unwind: "$skill_sets"
        }, {
          $group: { _id: "$skill_sets" }
        }

      ]).toArray(function (err, result) {
        err ? res.json(500, err) : res.json(200, result)
      })
    })

  },



  upload: function (req, res) {
    //XmlParserService.convert2JSON()
    async.waterfall([
      function (cb) {
        req.file(req.params.mediaType).upload({
          maxBytes: 10000000 // maximum 10 MB
        }, function (err, result) {
          console.log("uploadMediaResult", result)
          var _isArray = Array.isArray(result);
          err ? cb(err) : cb(null, _isArray ? result[0].fd : result.fd)
        });
      }, function (filepath, cb) {
        var _data = XmlParserService.convertToJson(filepath);
        var _incorrctArray = [];
        var correctData = [];
        var len = _data.length;
        for (var i = 0; i < len; i++) {
          var cnt = i + 1;
          if (!_data[i]["First Name"] || _data[i]["First Name"].trim() == "") {
            _data[i].msg = "First Name is missing at row " + cnt
            _incorrctArray.push(_data[i])
          }
          else if (!_data[i]["Email ID"] || _data[i]["Email ID"].trim() == "") {
            _data[i].msg = "Email ID is missing at row " + cnt
            _incorrctArray.push(_data[i])
          }
          else {
            correctData.push(_data[i])
          }
        }

        async.map(correctData, function (x, cbMap) {

        }, function (err, result) {
          err ? cb(err) : cb(null, result)
        })


      }], function (err, result) {
        err ? res.json(500, err) : res.json(200, result)
      })
  },

  newHomeSearch: function (req, res) {
    var queryUser = [],
      queryJobs = [{ $match: { "status": "pending" } }],
      keywordCond = [], userSearch = "", userSkill = [], location = [], industry = [], skill = [], userType = [], total_experience = [];;
    if (req.body["keyword"]) {
      userSearch = req.body["keyword"].join(" ")
      userSkill = req.body["keyword"].map(function (e) { return new RegExp(e, "i"); });
      // console.log("userSkilluserSkill---->", userSkill)
      for (var i = 0; i < req.body["keyword"].length; i++) {
        var obj = {
          $regex: req.body["keyword"][i] + ".*",
          $options: "i"
        }
        keywordCond.push({
          firstName: obj
        })
        keywordCond.push({
          companyName: obj
        })
        keywordCond.push({
          country: obj
        })
        keywordCond.push({
          city: obj
        })
        keywordCond.push({
          skill_sets: obj
        })
        //userSkill.push(obj)

        // console.log("obj---->", JSON.stringify(obj));
      }
    }
    if (req.body["industry"]) {
      for (var i = 0; i < req.body["industry"].length; i++) {
        var _industry = {
          $regex: req.body["industry"][i],
          $options: "i"
        };
        industry.push({
          "industry": _industry
        })
      }
    }
    if (req.body["skill"]) {
      for (var i = 0; i < req.body["skill"].length; i++) {
        var _skill = {
          $regex: req.body["skill"][i],
          $options: "i"
        };
        // skill.push({
        //   "$in":[_skill,"$skill_sets"]
        // })

        skill.push(req.body["skill"][i])
      }
    }
    if (req.body["location"]) {
      for (var i = 0; i < req.body["location"].length; i++) {
        var arr = req.body["location"][i].split(',');
        var city = {
          $regex: arr[0],
          $options: "i"
        };
        var country = {
          $regex: arr[1],
          $options: "i"
        };
        location.push({
          $and: [{
            "city": city
          }, {
            "country": country
          }]
        })
      }
    }
    if (req.body["userType"]) {
      /*
      {"userType":["University","Contractor"]}
      */
      for (var i = 0; i < req.body["userType"].length; i++) {
        var _userType = -1;
        switch (req.body["userType"][i].toLowerCase()) {
          case "university":
            _userType = "2";
            break;

          case "consultant":
            _userType = "3";
            break;

          case "consultant firm":
            _userType = "6";
            break;

          case "contractor":
            _userType = "9";
            break;

          case "bulk contractor":
            _userType = "10"
            break;

          case "University":
            _userType = "2"
            break;
        }
        userType.push({
          "userType": _userType
        })
      }
    }

    if (req.body["experience"]) {
      /*
      {"userType":["University","Contractor"]}
      */
      for (var i = 0; i < req.body["experience"].length; i++) {
        var _userType = -1;
        switch (req.body["experience"][i].toLowerCase()) {
          case "Fresher":
            _experience = "Fresher";
            break;

          case "0-3 years":
            _experience = "0-3 years";
            break;

          case "3-5 years":
            _experience = "3-5 years";
            break;

          case "5-10 years":
            _experience = "5-10 years";
            break;

          case "10-20 years":
            _experience = "10-20 years"
            break;


          case "20+ years":
            _experience = "20+ years"
            break;
        }
        total_experience.push({
          "total_experience": _experience
        })
      }
    }

    if (keywordCond.length == 0 && location.length == 0 && industry.length == 0 && userType.length == 0 && skill.length == 0 && total_experience.length == 0) {
      queryJobs.push({
        $sort: {
          avg_rating: -1
        }
      })
      queryUser.push({
        $sort: {
          avg_rating: -1
        }
      })
    }
    else {
      if (keywordCond.length > 0) {
        // queryJobs.push({
        //   $match:{
        //     $or:keywordCond
        //   }
        // })
        queryJobs.push({
          $match: {
            skill_sets: {
              $in: userSkill
            }
          }
        })
        // queryUser.push({
        //   $match: { 
        //     $text: { 
        //       $search:userSearch 
        //     } 
        //   }
        // })

        queryUser.push({
          $match: {
            $or: keywordCond
          }
        })

        // queryUser.push({
        //   $match:{
        //     skill_sets:{
        //       $in:userSkill
        //     }
        //   }
        // })

      }

      if (location.length > 0) {
        queryJobs.push({
          $match: {
            $or: location
          }
        })
        queryUser.push({
          $match: {
            $or: location
          }
        })
      }
      if (industry.length > 0) {
        queryJobs.push({
          $match: {
            $or: industry
          }
        })
        queryUser.push({
          $match: {
            $or: industry.map(x => { return { selectedIndustry: x.industry } })
          }
        })
      }
      if (userType.length > 0) {
        queryJobs.push({
          $match: {
            $or: userType
          }
        })
      }
      if (skill.length > 0) {
        queryJobs.push({
          $match: {
            "skill_sets": {
              $in: skill
            }
          }
        })
        queryUser.push({
          $match: {
            "skill_sets": {
              $in: skill
            }
          }
        })
      }
      if (total_experience.length > 0) {
        queryUser.push({
          $match: {
            $or: total_experience
          }
        })
      }
    }
    console.log("queryUser: ", JSON.stringify(queryUser));
    console.log("queryJobs: ", JSON.stringify(queryJobs));
    async.parallel({
      peopleSearch: function (cb) {
        User.native(function (err, connection) {
          connection.aggregate(queryUser)
            .toArray(function (err, result) {
              for (var i = 0; i < result.length; i++) {
                if (result[i].userType == "admin") {
                  result.splice(i, 1)
                }

              }
              err ? cb(err) : cb(null, result)
            })
        })
      }, skillSetSearch: function (cb) {
        Post.native(function (err, connection) {
          connection.aggregate(queryJobs)
            .toArray(function (err, result) {

              err ? cb(err) : cb(null, result)
            })
        })
      }
    }, function (err, result) {
      err ? res.status(500).send(err) : res.send(result)
    })
  },

  newHomeSearch1: function (req, res) {


  },

  uploadTest: function (req, res) {
    var data = XmlParserService.convertToJson('./Consultant.xls')
    res.send({ data: data });
  }
};




