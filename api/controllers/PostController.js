/**
 * PostController
 *
 * @description :: Server-side logic for managing posts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var ObjectId = require('mongodb').ObjectID;
var async = require('async');

module.exports = {
  searchJobs: function (req, res) {
    Post.native(function (err, collection) {
      //res.send(req.body.filter)
      var keywordCond = [];
      var location = [];
      var industry = [];
      var skills = [];

      if (req.body["keyword"]) {
        for (var i = 0; i < req.body["keyword"].length; i++) {
          var obj = {
            $regex: req.body["keyword"][i] + ".*",
            $options: "i"
          }
          keywordCond.push({
            title: obj
          })
          keywordCond.push({
            description: obj
          })

          console.log("obj---->", JSON.stringify(obj));
        }
      }
      if (req.body["location"]) {
        for (var i = 0; i < req.body["location"].length; i++) {
          var arr = req.body["location"][i].split(',');
          var city = {
            $regex: arr[0],
            $options: "i"
          };
          var country = {
            $regex: arr[1],
            $options: "i"
          };
          location.push({
            $and: [{
              "post_user.city": city
            }, {
              "post_user.country": country
            }]
          })
        }
      }
      if (req.body["industry"]) {
        for (var i = 0; i < req.body["industry"].length; i++) {
          var _industry = {
            $regex: req.body["industry"][i],
            $options: "i"
          };
          industry.push({
            "post_user.industry": _industry
          })
        }
      }

      if (req.body["skill"]) {
        for (var i = 0; i < req.body["skill"].length; i++) {
          var _skill = {
            $regex: req.body["skill"][i],
            $options: "i"
          };
          skills.push({
            "skill_sets": _skill
          })
        }
      }

      //industry 

      var query = [{
        $match: {
          type: "job",
          viwerType: "public",
          status: "pending"
        }
      }, {
        $lookup: {
          from: "user",
          localField: "userId",
          foreignField: "_id",
          as: "post_user"
        }
      }, {
        $addFields: {
          post_user: { $arrayElemAt: ["$post_user", 0] }
        }
      }];

      if (keywordCond.length == 0 && location.length == 0 && industry.length == 0 && skills.length == 0) {
        if (req.current_user.skill_sets && req.current_user.skill_sets.length > 0) {
          query.push({
            $addFields: {
              matchFound: {
                $reduce: {
                  input: req.current_user.skill_sets,
                  initialValue: 0,
                  in: {
                    $cond: {
                      if: {
                        $eq: [{
                          $indexOfArray: [{ $ifNull: ["$skill_sets", []] }, "$$this"]
                        }, -1]
                      },
                      then: "$$value",
                      else: {
                        $sum: ["$$value", 1]
                      }
                    }
                  }
                }
              }
            }
          },
            {
              $match: {
                matchFound: { $gt: 0 }
              }
            })
        }
        else {
          query.push({
            $match: {
              "post_user.industry": req.current_user.industry
            }
          })
        }
      }
      else {
        if (keywordCond.length > 0) {
          query.push({
            $match: {
              $or: keywordCond
            }
          })
        }
        if (location.length > 0) {
          query.push({
            $match: {
              $or: location
            }
          })
        }
        if (industry.length > 0) {
          query.push({
            $match: {
              $or: industry
            }
          })
        }
        if (skills.length > 0) {
          query.push({
            $match: {
              $or: skills
            }
          })
        }


        query.push({
          $addFields: {
            matchFound: {
              $reduce: {
                input: req.current_user.skill_sets,
                initialValue: 0,
                in: {
                  $cond: {
                    if: {
                      $eq: [{
                        $indexOfArray: [{ $ifNull: ["$skill_sets", []] }, "$$this"]
                      }, -1]
                    },
                    then: "$$value",
                    else: {
                      $sum: ["$$value", 1]
                    }
                  }
                }
              }
            }
          }
        },
          {
            $sort: {
              matchFound: -1
            }
          })
      }

      query = query.concat([{
        $lookup: {
          from: "proposal",
          as: "proposal",
          foreignField: "postId",
          localField: "_id"
        }
      }, {
        $addFields: {
          notSend: {
            $eq: [{
              $indexOfArray: ["$proposal.senderId", ModelService.ObjectId(User, req.current_user.id)]
            }, -1]
          }
        }
      }, {
        $match: {
          notSend: true
        }
      }
        , {
        $addFields: {
          proposal: {
            $cond: {
              if: {
                $eq: [{
                  $size: "$proposal"
                }, 0]
              },
              then: [{ _id: 0 }],
              else: "$proposal"
            }
          }
        }
      }
        , {
        $unwind: "$proposal"
      }, {
        $lookup: {
          from: "user",
          as: "prop_user",
          localField: "proposal.senderId",
          foreignField: "_id"
        }
      }, {
        $addFields: {
          prop_id: { $arrayElemAt: ["$prop_user._id", 0] },
          prop_user_firstName: { $arrayElemAt: ["$prop_user.firstName", 0] },
          prop_user_lastName: { $arrayElemAt: ["$prop_user.lastName", 0] },
          prop_user_profile_pic: { $arrayElemAt: ["$prop_user.profile_pic", 0] },
          prop_user_id: { $arrayElemAt: ["$prop_user._id", 0] }
        }
      }, {
        $match: {
          $or: [{
            prop_id: { $ne: ModelService.ObjectId(User, req.current_user.id) }
          }, {
            prop_id: { $ne: 0 }
          }]
        }
      }, {
        $group: {
          _id: "$_id",
          title: { $first: "$title" },
          type: { $first: "$type" },
          description: { $first: "$description" },
          cost: { $first: "$cost" },
          attachment: { $first: "$attachment" },
          status: { $first: "$status" },
          skill_sets: { $first: "$skill_sets" },
          start_date: { $first: "$start_date" },
          end_date: { $first: "$end_date" },
          post_user_firstName: { $first: "$post_user.firstName" },
          post_user_lastName: { $first: "$post_user.lastName" },
          post_user_profile_pic: { $first: "$post_user.profile_pic" },
          currencyType: { $first: "$currencyType" },
          country: { $first: "$post_user.country" },
          city: { $first: "$post_user.city" },
          createdAt: { $first: "$createdAt" },

          hoursPerWeek: { $first: "$hoursPerWeek" },
          intermediateLevel: { $first: "$intermediateLevel" },
          viwerType: { $first: "$viwerType" },
          proposal: {
            $push: {
              firstName: "$prop_user_firstName",
              lastName: "$prop_user_lastName",
              profile_pic: "$prop_user_profile_pic",
              _id: "$prop_user_id"
            }
          },
        }
      }
      ])
      console.log("query--->", JSON.stringify(query));

      collection.aggregate(query)
        .toArray(function (err, result) {
          err ? res.status(500).send(err) : res.send(result);
        })
    })
  },




  getPostDetails: function (req, res) {
    console.log("getPostDetails==========>", req.params.id)
    Post.native(function (err, collection) {
      collection.findOne({ _id: ModelService.ObjectId(Post, req.params.id) }, function (err, result) {
        console.log("result------------------->", result)
        err ? res.status(500).send(err) : res.send(result);

      })
    })
  },

  editProject: function (req, res) {
    console.log("editProject----------------", req.body)
    var test1 = ModelService.ObjectId(Post, req.body.obj._id)
    delete req.body.obj._id
    delete req.body.obj.userId
    var test2 = req.body.obj
    Post.native(function (err, collection) {
      collection.update({
        _id: test1
        // "email_id" : "namrata@senzecit.com"
      }, {
          $set: test2
        }, { new: true }, function (err, result) {
          if (err) {
            console.error("Error on ContactService.createContact");
            console.error(err);
            console.error(JSON.stringify(err));
            return res.json(500, { success: 'ERROR', responseData: err });
          }
          else {
            var responseData = "Education Info Update Successfully!"
            return res.json(200, { success: 'Success', responseData: responseData })
          }

        })

    })
  },




  recruiterJobList: function (req, res) {
    var freshness = [], loation = [], education = [], salary = [], _subType = "";
    if (req.current_user.userType == 7 || req.current_user.userType == 8) {
      _subType = "tempHiring";
    }
    else if (req.current_user.userType == 9 || req.current_user.userType == 10) {
      _subType = "recruitment";
    }
    console.log("------------ ID---->", req.current_user.id)
    Post.native(function (err, collection) {
      collection.aggregate([
        {
          $lookup: {
            localField: "_id",
            foreignField: "postId",
            as: "proposalList",
            from: "proposal"
          }
        },
        {
          $match: {
            "proposalList.senderId": ModelService.ObjectId(User, req.current_user.id)
          }
        },
        {
          $lookup: {
            from: "user",
            as: "post_user",
            localField: "userId",
            foreignField: "_id"
          }
        }, {
          $addFields: {
            post_user: { $arrayElemAt: ["$post_user", 0] }
          }
        },
        {
          $lookup: {
            from: "user",
            as: "prop_user",
            localField: "proposalList.senderId",
            foreignField: "_id"
          }
        }, {
          $addFields: {
            prop_user: { $arrayElemAt: ["$prop_user", 0] }
          }
        }, {
          $group: {
            _id: "$_id",
            userId: { $first: "$userId" },
            title: { $first: "$title" },
            description: { $first: "$description" },
            minSalary: { $first: "$minSalary" },
            maxSalary: { $first: "$maxSalary" },
            education: { $first: "$education" },
            industry: { $first: "$industry" },
            expMnth: { $first: "$expMnth" },
            expYear: { $first: "$expYear" },
            status: { $first: "$status" },
            skill_sets: { $first: "$skill_sets" },
            country: { $first: "$post_user.country" },
            city: { $first: "$post_user.city" },
            type: { $first: "$type" },
            cost: { $first: "$cost" },
            viwerType: { $first: "$viwerType" },
            createdAt: { $first: "$createdAt" },
            updatedAt: { $first: "$updatedAt" },
            proposalId: { $first: "$proposalList._id" },
            currencyType: { $first: "$currencyType" }
          }
        }
      ]).toArray(function (err, result) {
        err ? res.status(500).send(err) : res.send(result);
      })
    })
  },
  recruiterFeeds: function (req, res) {
    Post.native(function (error, collection) {
      var query = [], _subType = "";
      if (req.current_user.userType == 7 || req.current_user.userType == 8) {
        _subType = "tempHiring";
      }
      else if (req.current_user.userType == 9 || req.current_user.userType == 10) {
        _subType = "recruitment";
      }
      var freshness = [], location = [], education = [], salary = [];
      if (req.body["freshness"]) {
        for (var i = 0; i < req.body["freshness"].length; i++) {
          var dt = new Date();
          dt.setDate(dt.getDate() - req.body["freshness"][i])
          var _freshness = {
            $gte: dt
          };

          freshness.push({
            "createdAt": _freshness
          })
        }
      }

      if (req.body["location"]) {
        for (var i = 0; i < req.body["location"].length; i++) {
          var city = {
            $regex: req.body["location"][i],
            $options: "i"
          };
          var country = {
            $regex: req.body["location"][i],
            $options: "i"
          };
          location.push({
            $or: [{
              "city": city
            }, {
              "country": country
            }]
          })
        }
      }

      if (req.body["education"]) {
        for (var i = 0; i < req.body["education"].length; i++) {
          var _education = {
            $regex: req.body["education"][i],
            $options: "i"
          };
          education.push({
            education: _education
          })
        }
      }

      if (req.body["salary"]) {
        for (var i = 0; i < req.body["salary"].length; i++) {
          var arr = req.body["salary"][i].split('~');
          console.log("arrarrarrarrarrarrarrarrarr>", arr)
          if (arr[0] == "infinity") {
            salary.push({
              minSalary: { $lte: parseInt(arr[1]) }
            })
          }
          else if (arr[1] == "infinity") {
            salary.push({
              minSalary: { $gte: parseInt(arr[0]) }
            })
          }
          else {
            salary.push({
              $and: [
                { minSalary: { $lte: parseInt(arr[1]) } },
                { minSalary: { $gte: parseInt(arr[0]) } }
              ]
            })
          }

        }
      }

      query.push({
        $match: {
          type: "job",
          subType: _subType,
          viwerType: "public"
        }
      });
      query.push({
        $lookup: {
          from: "proposal",
          as: "proposal",
          foreignField: "postId",
          localField: "_id"
        }
      }, {
          $addFields: {
            notSend: {
              $eq: [{
                $indexOfArray: ["$proposal.senderId", ModelService.ObjectId(User, req.current_user.id)]
              }, -1]
            }
          }
        }, {
          $match: {
            notSend: true
          }
        });
      if (freshness.length > 0) {
        query.push({
          $match: {
            $or: freshness
          }
        })
      }
      if (location.length > 0) {
        query.push({
          $match: {
            $or: location
          }
        })
      }
      if (education.length > 0) {
        query.push({
          $match: {
            $or: education
          }
        })
      }
      if (salary.length > 0) {
        query.push({
          $match: {
            $or: salary
          }
        })
      }

      if (req.current_user.skill_sets && req.current_user.skill_sets.length > 0) {
        query.push({
          $addFields: {
            matchFound: {
              $reduce: {
                input: req.current_user.skill_sets,
                initialValue: 0,
                in: {
                  $cond: {
                    if: {
                      $eq: [{
                        $indexOfArray: [{ $ifNull: ["$skill_sets", []] }, "$$this"]
                      }, -1]
                    },
                    then: "$$value",
                    else: {
                      $sum: ["$$value", 1]
                    }
                  }
                }
              }
            }
          }
        }, {
            $sort: {
              matchFound: -1,
              createdAt: -1
            }
          })
      }

      query.push({
        $lookup: {
          from: "user",
          localField: "userId",
          foreignField: "_id",
          as: "post_user"
        }
      }, {
          $addFields: {
            post_user: { $arrayElemAt: ["$post_user", 0] }
          }
        })
      query.push({
        $project: {
          _id: 1,
          title: 1,
          type: 1,
          currencyType: 1,
          description: 1,
          expMnth: 1,
          expYear: 1,
          status: 1,
          skill_sets: 1,
          minSalary: 1,
          maxSalary: 1,
          type: 1,
          country: 1,
          city: 1,
          cost: 1,
          post_user_firstName: "$post_user.firstName",
          post_user_lastName: "$post_user.lastName",
          post_user_profilePic: "$post_user.profile_pic.url",
          post_user_companyName: "$post_user.companyName",
          subType: 1,
          viwerType: 1,
          createdAt: 1
        }
      });
      console.log('\n\n\n\n\nquery--------nquery ---->', JSON.stringify(query));
      collection.aggregate(query)
        .toArray(function (err, result) {
          err ? res.status(500).send(err) : res.send(result);
        })
    })
  },
  consultantFeeds: function (req, res) {
    Post.native(function (err, collection) {
      var query = [{
        $match: {
          type: "job",
          viwerType: "public",
          status: "pending"
        }
      }, {
        $lookup: {
          from: "user",
          localField: "userId",
          foreignField: "_id",
          as: "post_user"
        }
      }, {
        $addFields: {
          post_user: { $arrayElemAt: ["$post_user", 0] }
        }
      }];

      if (req.current_user.skill_sets && req.current_user.skill_sets.length > 0) {
        console.log("query----> if");
        query.push({
          $addFields: {
            matchFound: {
              $reduce: {
                input: req.current_user.skill_sets,
                initialValue: 0,
                in: {
                  $cond: {
                    if: {
                      $eq: [{
                        $indexOfArray: [{ $ifNull: ["$skill_sets", []] }, "$$this"]
                      }, -1]
                    },
                    then: "$$value",
                    else: {
                      $sum: ["$$value", 1]
                    }
                  }
                }
              }
            }
          }
        }, {
            // $match:{
            //   matchFound:{$gt:0}
            // }
            $sort: { matchFound: -1 }
            // $sort: { "Post.createdAt": 1 }
          })
      }
      else {
        console.log("query----> else");
        query.push({
          $match: {
            "post_user.industry": req.current_user.industry
          }
        })
      }
      query.push({
        $match: {
          $or: [{
            "userType": 3
          }, {
            "userType": 6
          }]
          // "post_user.userType":req.current_user.industry
        }
      })
      query.push({
        $lookup: {
          from: "proposal",
          as: "proposal",
          foreignField: "postId",
          localField: "_id"
        }
      }, {
          $addFields: {
            notSend: {
              $eq: [{
                $indexOfArray: ["$proposal.senderId", ModelService.ObjectId(User, req.current_user.id)]
              }, -1]
            }
          }
        }, {
          $match: {
            notSend: true
          }
        }
        , {
          $addFields: {
            proposal: {
              $cond: {
                if: {
                  $eq: [{
                    $size: "$proposal"
                  }, 0]
                },
                then: [{ _id: 0 }],
                else: "$proposal"
              }
            }
          }
        }
        , {
          $unwind: "$proposal"
        }, {
          $lookup: {
            from: "user",
            as: "prop_user",
            localField: "proposal.senderId",
            foreignField: "_id"
          }
        }, {
          $addFields: {
            prop_id: { $arrayElemAt: ["$prop_user._id", 0] },
            prop_user_firstName: { $arrayElemAt: ["$prop_user.firstName", 0] },
            prop_user_lastName: { $arrayElemAt: ["$prop_user.lastName", 0] },
            prop_user_profile_pic: { $arrayElemAt: ["$prop_user.profile_pic", 0] }
          }
        }, {
          $match: {
            $or: [{
              prop_id: { $ne: ModelService.ObjectId(User, req.current_user.id) }
            }, {
              prop_id: { $ne: 0 }
            }]
          }
        }, {
          $group: {
            _id: "$_id",
            title: { $first: "$title" },
            type: { $first: "$type" },
            description: { $first: "$description" },
            cost: { $first: "$cost" },
            attachment: { $first: "$attachment" },
            status: { $first: "$status" },
            skill_sets: { $first: "$skill_sets" },
            start_date: { $first: "$start_date" },
            end_date: { $first: "$end_date" },
            hoursPerWeek: { $first: "$hoursPerWeek" },
            post_user: { $first: "$post_user" },
            currencyType: { $first: "$currencyType" },
            intermediateLevel: { $first: "$intermediateLevel" },
            createdAt: { $first: "$createdAt" },
            viwerType: { $first: "$viwerType" },
            proposal: {
              $push: {
                firstName: "$prop_user_firstName",
                lastName: "$prop_user_lastName",
                profile_pic: "$prop_user_profile_pic",
                _id: "$prop_user_id"
              }
            },
          }
        })
      console.log("consultantFeeds > query: ", JSON.stringify(query));

      collection.aggregate(query)
        .toArray(function (err, result) {
          err ? res.status(500).send(err) : res.send(result);
        })
    })
  },
  changeJobStatus: function (req, res) {
    Post.native(function (err, collection) {
      collection.update({
        _id: ModelService.ObjectId(Post, req.body.id)
      }, {
          $set: {
            status: req.body.status
          }
        }, function (err, result) {
          err ? res.status(500).send(result) : res.send(result);
        })
    })
  },
  consultantProposalList: function (req, res) {
    Post.native(function (err, collection) {
      collection.aggregate([
        {
          $lookup: {
            localField: "_id",
            foreignField: "postId",
            as: "proposalList",
            from: "proposal"
          }
        },
        {
          $match: {

            "proposalList.senderId": ModelService.ObjectId(User, req.current_user.id)
          }
        },
        {
          $unwind: "$proposalList"
        },
        {
          $match: {
            $and: [{
              "proposalList.senderId": ModelService.ObjectId(User, req.current_user.id)
            }/* , {
              $or: [{
                "proposalList.status": "pending"
              }, {
                "proposalList.status": "contractRequest"
              }]
            } */]
          }
        },
        {
          $lookup: {
            from: "user",
            as: "post_user",
            localField: "userId",
            foreignField: "_id"
          }
        }, {
          $addFields: {
            post_user: { $arrayElemAt: ["$post_user", 0] }
          }
        },
        {
          $lookup: {
            from: "user",
            as: "prop_user",
            localField: "proposalList.senderId",
            foreignField: "_id"
          }
        }, {
          $addFields: {
            prop_user: { $arrayElemAt: ["$prop_user", 0] }
          }
        }, {
          $group: {
            _id: "$_id",
            userId: { $first: "$userId" },
            title: { $first: "$title" },
            description: { $first: "$description" },
            cost: { $first: "$cost" },
            attachment: { $first: "$attachment" },
            status: { $first: "$status" },
            skill_sets: { $first: "$skill_sets" },
            start_date: { $first: "$start_date" },
            end_date: { $first: "$end_date" },
            country: { $first: "$post_user.country" },
            city: { $first: "$post_user.city" },
            hoursPerWeek: { $first: "$hoursPerWeek" },
            type: { $first: "$type" },
            viwerType: { $first: "$viwerType" },
            createdAt: { $first: "$createdAt" },
            updatedAt: { $first: "$updatedAt" },
            proposalId: { $first: "$proposalList._id" },
            currencyType: { $first: "$currencyType" },
            proposalDetail: {
              $addToSet: {
                prop_ud: "$prop_user._id",
                firstName: "$prop_user.firstName",
                lastName: "$prop_user.lastName",
                companyName: "$prop_user.companyName",
                profile_pic: "$prop_user.profile_pic"
              }
            }
          }
        }
      ]).toArray(function (err, result) {
        err ? res.status(500).send(err) : res.send(result);
      })
    })
  },

  consultantFeedsSearch: function (req, res) {
    Post.native(function (err, collection) {
      var query = [{
        $match: {
          type: "job"
        }
      }, {
        $lookup: {
          as: "createUser",
          from: "user",
          localField: "userId",
          foreignField: "_id"
        }
      }, {
        $addFields: {
          createUser: { $arrayElemAt: ["$createUser", 0] }
        }
      }
        , {
        $sort: {
          "createUser.avg_rating": -1
        }
      },
      {
        $match: {
          $or: [
            {
              title: { '$regex': req.body.searchKey, '$options': 'i' }
            },
            {
              description: { '$regex': req.body.searchKey, '$options': 'i' }
            }
          ]
        }
      }];
      collection.aggregate(query)
        .toArray(function (err, result) {
          err ? res.status(500).send(err) : res.send(result);
        })
    })
  },

  feeds: function (req, res) {
    Post.native(function (err, collection) {
      var query = [{
        $match: {
          $or: [{
            userId: ModelService.ObjectId(User, req.current_user.id)
          }, {
            assigneeId: ModelService.ObjectId(User, req.current_user.id)
          }]
        }
      }]
      collection.aggregate(query)
        .toArray(function (err, result) {
          err ? res.status(500).send(err) : res.send(result);
        })
    })
  },

  attachment: function (req, res) {
    Post.native(function (err, collection) {
      var query = [{
        $match: { _id: ModelService.ObjectId(Proposal, req.params.id) }
      }, {
        $project: {
          comments: {
            $concatArrays: [{
              $ifNull: ["$comments", []]
            }, [{
              created_at: "$createdAt",
              created_by_id: "$userId",
              attachment: "$attachment"
            }]]
          }
        }

      }, {
        $unwind: "$comments"
      }, {
        $unwind: "$comments.attachment"
      }, {
        $lookup: {
          from: "user",
          localField: "comments.created_by_id",
          foreignField: "_id",
          as: "userDet"
        }
      }, {
        $addFields: {
          firstName: { $arrayElemAt: ["$userDet.firstName", 0] },
          lastName: { $arrayElemAt: ["$userDet.lastName", 0] },
          companyName: { $arrayElemAt: ["$userDet.companyName", 0] }
        }
      }];
      var query1 = [{
        $match: {
          _id: ModelService.ObjectId(Proposal, req.params.id)
        }
      }, {
        $project: {
          files: {
            $concatArrays: [{
              $map: {
                input: "$attachment",
                as: "x",
                in: {
                  file: "$$x",
                  uploadedBy: "$userId",
                  createdAt: "$createdAt"
                }
              }
            }, {
              $map: {
                input: "$comments",
                as: "x",
                in: {
                  $map: {
                    input: "$$x.attachment",
                    as: "y",
                    in: {
                      fileArray: "$$y",
                      uploadedBy: "$$x.created_by_id",
                      createdAt: "$$x.created_at"
                    }
                  }
                }
              }
            }]
          }
        }
      }, {
        $unwind: "$files"
      }, {
        $lookup: {
          from: "user",
          localField: "files.uploadedBy",
          foreignField: "_id",
          as: "userDet"
        }
      }, {
        $addFields: {
          firstName: { $arrayElemAt: ["$userDet.firstName", 0] },
          lastName: { $arrayElemAt: ["$userDet.lastName", 0] },
          companyName: { $arrayElemAt: ["$userDet.companyName", 0] }
        }
      }];
      collection.aggregate(query)
        .toArray(function (err, result) {
          err ? res.status(500).send(err) : res.send(result);
        })
    })
  },

  createAssign: function (req, res) {
    async.waterfall([
      function (cb) {
        var obj = {
          userId: req.current_user.id,
          assigneeId: req.body.assigneeId,
          title: req.body.title,
          description: req.body.description,
          cost: req.body.cost,
          attachment: req.body.attachment,
          status: req.body.status,
          skill_sets: req.body.skill_sets,
          start_date: req.body.start_date,
          end_date: req.body.end_date,
          userType: req.body.userType,
          currencyType: req.body.currencyType,
          hoursPerWeek: req.body.hoursPerWeek,
          intermediateLevel: req.body.intermediateLevel,
          preferredQualification: req.body.preferredQualification,
          negotiate: req.body.negotiate,
          education: req.body.education,
          experience: req.body.experience,
          industry: req.body.industry,
          type: "job",
          subType: "fixedBid",
          jobType: req.body.jobType,
          proposal: []

        }
        Post.create(obj).then(function (result) {
          cb(null, result)
          // var responseData = "Assignment Add Successfully!"
          // return res.json(200, { success: 'Success' ,responseData:responseData});           
        }).catch(function (err) {
          cb(err)
          // return res.json(500, { success: 'ERROR' ,responseData:err});
        });

      },
      function (result, cb) {
        var assignmentId = result.id;
        User.native(function (err, collection) {
          var oid = new ObjectId();
          var query = {
            _id: ModelService.ObjectId(User, req.body.assigneeId)
          };
          collection.update(query, {
            $push: {
              notification: {
                type: "notification",
                sub_type: "new assignment",
                status: "unread",
                assignmentId: ModelService.ObjectId(Post, result.id),
                user_id: ModelService.ObjectId(User, req.current_user.id),
                created_at: new Date(),
                _id: JSON.parse(JSON.stringify(oid))
              }
            }
          }, function (err, result) {
            err ? cb(err) : cb(null, result, assignmentId, oid)
          })
        })

      },
      function (result, assignmentId, notifyId, cb) {
        var userId = req.body.assigneeId
        console.log("userId--->", userId)
        User.getNamePic(userId)
          .then(function (_user) {
            var _obj = {
              sub_type: "new assignment",
              assignmentId: assignmentId,
              firstName: _user.firstName,
              lastName: _user.lastName,
              companyName: _user.companyName,
              projectName: req.body.title,
              created_at: new Date(),
              notifyId: notifyId
            }
            NotificationService.newNotification(_obj, userId)
            cb(null, {})
          }, function (objE) {
            console.log("error--->", objE);
            cb(objE)
          })
      }
    ], function (err, result) {
      err ? res.json(500, { success: 'ERROR', responseData: err }) : res.json(200, { success: 'Success', responseData: result })

    })
  },
  createPublicAssign: function (req, res) {
    console.log("createPublicAssign---------------->", req.body)
    async.waterfall([
      function (cb) {
        var obj = {
          userId: req.current_user.id,
          // assigneeId:req.body.assigneeId,
          title: req.body.title,
          userType: req.body.userType,
          description: req.body.description,
          cost: req.body.cost,
          attachment: req.body.attachment,
          status: req.body.status,
          currencyType: req.body.currencyType,
          skill_sets: req.body.skill_sets,
          start_date: req.body.start_date,
          end_date: req.body.end_date,
          hoursPerWeek: req.body.hoursPerWeek,
          intermediateLevel: req.body.intermediateLevel,
          preferredQualification: req.body.preferredQualification,
          type: "job",
          negotiate: req.body.negotiate,
          education: req.body.education,
          experience: req.body.experience,
          industry: req.body.industry,
          subType: "fixedBid",
          viwerType: "public",
          jobType: req.body.jobType,
          proposal: []

        }
        Post.create(obj).then(function (result) {
          cb(null, result)
          // var responseData = "Assignment Add Successfully!"
          // return res.json(200, { success: 'Success' ,responseData:responseData});           
        }).catch(function (err) {
          cb(err)
          // return res.json(500, { success: 'ERROR' ,responseData:err});
        });

      }
      // ,
      // function(result,cb){
      //      User.native(function(err,collection){
      //       var oid = new ObjectId();
      //       var query={
      //           _id:ModelService.ObjectId(User,req.body.assigneeId)
      //         };
      //         collection.update(query,{
      //           $push:{
      //             notification:{
      //               type:"notification",
      //               sub_type:"new assignment",
      //               status:"unread",
      //               assignmentId:ModelService.ObjectId(Post,result.id),
      //               user_id:ModelService.ObjectId(User,req.current_user.id),
      //               created_at:new Date(),
      //               _id:JSON.parse(JSON.stringify(oid))
      //             }
      //           }
      //         },function(err,result){
      //           err?cb(err):cb(null,result)
      //         })
      //     })

      // }
    ], function (err, result) {
      err ? res.json(500, { success: 'ERROR', responseData: err }) : res.json(200, { success: 'Success', responseData: result })

    })
  },
  createPublicJob: function (req, res) {
    console.log("createPublicJob--------->", req.body)
    async.waterfall([
      function (cb) {
        var obj = {
          userId: req.current_user.id,
          title: req.body.title,
          userType: req.body.userType,
          description: req.body.description,
          cost: req.body.cost,
          attachment: req.body.attachment,
          status: req.body.status,
          currencyType: req.body.currencyType,
          skill_sets: req.body.skill_sets,
          start_date: req.body.start_date,
          end_date: req.body.end_date,
          type: "job",
          negotiate: req.body.negotiate,
          education: req.body.education,
          experience: req.body.experience,
          industry: req.body.industry,
          jobType: req.body.jobType,
          subType: "odc",
          viwerType: "public",
          proposal: []
        }
        Post.create(obj).then(function (result) {
          // send email notification to matching users with same skills
          // EmailToSkillUsers(obj.title, obj.description, cost, obj.skill_sets, `${obj.start_date} - ${obj.end_date} days`);
          cb(null, result)
        }).catch(function (err) {
          cb(err)
        });
      }
    ], function (err, result) {
      err ? res.json(500, { success: 'ERROR', responseData: err }) : res.json(200, { success: 'Success', responseData: result })

    })
  },
  EmailToSkillUsers: function (title, descr, cost, skills, duration) {
    sails.log.debug('EmailToSkillUsers: ', arguments);
    /* const users = User.find({ "email_id"});
    users.forEach(element => {
      EmailService.newJobFeedToUsers(req, req.body.email_id, code, req.body.firstName);
    }); */
  },
  createJobRecruitment: function (req, res) {
    async.waterfall([

      function (cb) {
        User.native(function (err, collection) {
          collection.findOne({ _id: ModelService.ObjectId(User, req.current_user.id) }, function (err, result) {
            console.log("createJobRecruitment result2", result.bucket)
            if (!result.bucket) {
              err ? res.json(500, { success: 'ERROR', responseData: err }) : res.json(400, { status: 400, success: 'ERROR', responseData: "You Do not Have Any Subscrition..!!" })
            } else {
              if (result.bucket.recruiter.jobPosted.count < 0) {

              } else {
                console.log("createJobRecruitment", result.bucket)
                var obj = {
                  userId: req.current_user.id,
                  title: req.body.title,
                  userType: req.body.userType,
                  description: req.body.description,
                  expMnth: req.body.expMnth,
                  expYear: req.body.expYear,
                  status: req.body.status,
                  skill_sets: req.body.skill_sets,
                  minSalary: req.body.minSalary,
                  currencyType: req.body.currencyType,
                  maxSalary: req.body.maxSalary,
                  type: "job",

                  education: req.body.education,
                  industry: req.body.industry,
                  country: req.body.country,
                  city: req.body.city,
                  subType: "recruitment",
                  viwerType: "public",
                  proposal: []
                }

                Post.create(obj).then(function (result1) {
                  cb(null, result1, result)
                })
              }
            }
          })
        })
      }, function (recruiterResult, result, cb) {
        var count = result.bucket.recruiter.jobPosted.count - 1
        User.native(function (err, collection) {
          collection.findOneAndUpdate({ _id: ModelService.ObjectId(User, req.current_user.id) }, { $set: { 'bucket.recruiter.jobPosted.count': count } }, function (err, result2) {
            cb(null, result2)
          })
        })
      }


      // function(cb){

      //   // User.findOne({_id: ModelService.ObjectId(User,req.body.current_user)}, {bucket:1}, function(err, result){
      //   //   if(err){

      //   //   }
      //   // })
      //     var obj = {
      //           userId:req.current_user.id,
      //           title:req.body.title,
      //           userType:req.body.userType,
      //           description:req.body.description,
      //           expMnth:req.body.expMnth,
      //           expYear:req.body.expYear,
      //           status:req.body.status,
      //           skill_sets:req.body.skill_sets,
      //           minSalary:req.body.minSalary,
      //           currencyType:req.body.currencyType,
      //           maxSalary:req.body.maxSalary,
      //           type:"job",
      //           education:req.body.education,
      //           industry:req.body.industry,
      //           country:req.body.country,
      //           city:req.body.city,
      //           subType:"recruitment",
      //           viwerType:"public",
      //           proposal:[] 
      //       }
      //       Post.create(obj).then(function (result) {
      //           cb(null,result)           
      //       }).catch(function (err) {
      //           cb(err)
      //       });

      // }
    ], function (err, result) {
      err ? res.json(500, { success: 'ERROR', responseData: err }) : res.json(200, { success: 'Success', responseData: result })

    })
  },
  createJobTempHiring: function (req, res) {
    async.waterfall([

      function (cb) {
        User.native(function (err, collection) {
          collection.findOne({ _id: ModelService.ObjectId(User, req.current_user.id) }, function (err, result) {
            if (!result.bucket) {
              err ? res.json(500, { success: 'ERROR', responseData: err }) : res.json(400, { status: 400, success: 'ERROR', responseData: "You Do not Have Any Subscrition..!!" })
            } else {
              if (result.bucket.fos.jobPosted.count < 0) {
              }
              else {
                var obj = {
                  userId: req.current_user.id,
                  title: req.body.title,
                  userType: req.body.userType,
                  description: req.body.description,
                  duration: req.body.duration,
                  cost: req.body.cost,
                  country: req.body.country,
                  city: req.body.city,
                  status: req.body.status,
                  currencyType: req.body.currencyType,
                  skill_sets: req.body.skill_sets,
                  education: req.body.education,
                  industry: req.body.industry,
                  type: "job",
                  subType: "tempHiring",
                  viwerType: "public",
                  proposal: []
                }
                Post.create(obj).then(function (result1) {
                  cb(null, result1, result)
                })
              }
            }
          })
        })
      },


      function (fosResult, result, cb) {
        var a = result.bucket.fos.jobPosted.count - 1
        User.native(function (err, collection) {
          collection.findOneAndUpdate({ _id: ModelService.ObjectId(User, req.current_user.id) }, { $set: { 'bucket.fos.jobPosted.count': a } }, function (err, result2) {
            cb(null, result2)
          })

        })
        // var obj = {
        //       userId:req.current_user.id,
        //       title:req.body.title,
        //       userType:req.body.userType,
        //       description:req.body.description,
        //       duration:req.body.duration,
        //       cost:req.body.cost,
        //       status:req.body.status,
        //       currencyType:req.body.currencyType,
        //       skill_sets:req.body.skill_sets,
        //       education:req.body.education,
        //       industry:req.body.industry,
        //       type:"job",
        //       subType:"tempHiring",
        //       viwerType:"public",
        //       proposal:[] 
        //   }
        //   Post.create(obj).then(function (result) {
        //       cb(null,result)           
        //   }).catch(function (err) {
        //       cb(err)
        //   });

      }
    ], function (err, result) {
      err ? res.json(500, { success: 'ERROR', responseData: err }) : res.json(200, { success: 'Success', responseData: result })

    })
  },
  assignmentDetail: function (req, res) {
    console.log("assignmentDetail--------------->", req.params)
    Post.native(function (err, collection) {
      var query = [{
        $match: { _id: ModelService.ObjectId(Post, req.params.id) }
      }, {
        $lookup: {
          from: "user",
          localField: "userId",
          foreignField: "_id",
          as: "userDet"
        }
      }];
      collection.aggregate(query)
        .toArray(function (err, result) {
          err ? res.status(500).send(err) : res.send(result);
        })
    })
  },
  addLike: function (req, res) {
    var groupId = ModelService.ObjectId(Conngrp, req.body.groupId);
    var postId = ModelService.ObjectId(Post, req.body.postId);
    var userId = ModelService.ObjectId(User, req.current_user.id);
    console.log("groupId----> %s \n postId----> %s \n postId----> %s", req.body.groupId, req.body.postId, req.current_user.id)
    Post.native(function (err, collection) {

      collection.update({
        _id: postId
      }, {
          $addToSet: {
            "like": {
              userId: userId,
              type: "like"
            }
          }
        }, function (err, result) {
          err ? res.status(500).send(err) : res.send(result)
        })
    })
  },
  addComment: function (req, res) {
    var groupId = ModelService.ObjectId(Conngrp, req.body.groupId);
    var postId = ModelService.ObjectId(Post, req.body.postId);
    var userId = ModelService.ObjectId(User, req.current_user.id);
    var oid = new ObjectId();
    var _commentId = JSON.parse(JSON.stringify(oid))
    var _activityId = JSON.parse(JSON.stringify(oid))
    console.log("groupId----> %s \n postId----> %s \n postId----> %s", req.body.groupId, req.body.postId, req.current_user.id)
    Post.native(function (err, collection) {

      collection.update({
        _id: postId
      }, {
          $addToSet: {
            "comments": {
              text: req.body.comment,
              userId: userId,
              at: new Date(),
              _id: _commentId
            }
          }
        }, function (err, result) {
          err ? res.status(500).send(err) : res.send(result)
        })
    })
  },
  assignList: function (req, res) {
    console.log(req.body);
    var condition = {}
    if (req.body.type == "all") {
      condition = {
        $and: [{ $or: [{ assigneeId: ModelService.ObjectId(User, req.current_user.id) }, { userId: ModelService.ObjectId(User, req.current_user.id) }] }, { type: "job" }]
      }
    }
    else if (req.body.type == "assignee") {
      condition = {
        $and: [{ assigneeId: ModelService.ObjectId(User, req.current_user.id) }, { type: "job" }]
      }
    }
    else {
      $and: [{ userId: ModelService.ObjectId(User, req.current_user.id) }, { type: "job" }]
    }
    Post.native(function (err, collection) {
      collection.aggregate([
        {
          $match: condition
        },
        {
          $match: { viwerType: { $exists: false } }
        },
        {
          $lookup:
          {
            from: "user",
            localField: "userId",
            foreignField: "_id",
            as: "user"
          }
        },
        {
          $project: {
            userId: 1,
            comments: 1,
            attachment: 1,
            user_firstName: { $arrayElemAt: ["$user.firstName", 0] },
            user_lastName: { $arrayElemAt: ["$user.lastName", 0] },
            user_companyName: { $arrayElemAt: ["$user.companyName", 0] },
            user_profile_pic: { $arrayElemAt: ["$user.profile_pic", 0] },
            user_avg_rating: { $arrayElemAt: ["$user.avg_rating", 0] },
            title: 1,
            cost: 1,
            status: 1,
            description: 1,
            start_date: 1,
            end_date: 1,
            createdAt: 1,
            skill_sets: 1,
            assigneeId: 1
          }
        }
      ]).toArray(function (err, result) {
        err ? res.status(500).send(err) : res.send(result);
      })
    })
  },


  // newPublicAssignList:function(req,res){
  //   console.log(req.body);

  //   var pageNo= req.body.pageNo-1
  //   var count=0;
  //   var skipValue= pageNo*10;
  //   var totalPages=0

  //   var condition={
  //       $and:[{$or:[{assigneeId:ModelService.ObjectId(User,req.current_user.id)},{ }]}]
  //   }
  //   Post.native(function(err,collection){
  //      collection.aggregate([
  //               {
  //                  $match:{
  //                   userId:ModelService.ObjectId(User,req.current_user.id)
  //                 }
  //               },
  //               {
  //                 $lookup:
  //                      {
  //                        from: "user",
  //                        localField:"userId",
  //                        foreignField:"_id",
  //                        as:"user"
  //                      }
  //               },
  //               {
  //                 $lookup:{
  //                   localField:"_id",
  //                   foreignField:"postId",
  //                   as:"proposalList",
  //                   from:"proposal"
  //                 }
  //               },
  //               {
  //                 $addFields:{
  //                   user:{$arrayElemAt:["$user",0]},
  //                   proposalList:{
  //                     $cond:{
  //                       if:{
  //                         $eq:[{
  //                           $size:"$proposalList"
  //                         },0]
  //                       },
  //                       then:[{_id:0}],
  //                       else:"$proposalList"
  //                     }
  //                   }
  //                 }
  //               },{
  //                 $unwind:"$proposalList"
  //               },{
  //                 $lookup:{
  //                   from:"user",
  //                   as:"prop_user",
  //                   localField:"proposalList.senderId",
  //                   foreignField:"_id"
  //                 }
  //               },{
  //                 $addFields:{
  //                   prop_user:{$arrayElemAt:["$prop_user",0]}
  //                 }
  //               },
  //               {
  //                 $group:{
  //                   _id:"$_id",
  //                   userId:{$first:"$userId"},
  //                   title:{$first:"$title"},
  //                   userType:{$first:"$userType"},
  //                   description:{$first:"$description"},
  //                   cost:{$first:"$cost"},
  //                   minSalary:{$first:"$minSalary"},
  //                   maxSalary:{$first:"$maxSalary"},
  //                   education:{$first:"$education"},
  //                   industry:{$first:"$industry"},
  //                   expMnth:{$first:"$expMnth"},
  //                   expYear:{$first:"$expYear"},
  //                   attachment:{$first:"$attachment"},
  //                   status:{$first:"$status"},
  //                   skill_sets:{$first:"$skill_sets"},
  //                   duration:{$first:"$duration"},
  //                   hoursPerWeek:{$first:"$hoursPerWeek"},
  //                   city:{$first:"$user.city"},
  //                   country:{$first:"$user.country"},
  //                   type:{$first:"$type"},
  //                   viwerType:{$first:"$viwerType"},
  //                   createdAt:{$first:"$createdAt"},
  //                   updatedAt:{$first:"$updatedAt"},
  //                   currencyType:{$first:"$currencyType"},

  //                   proposalDetail:{
  //                     $addToSet:{
  //                       prop_ud:"$prop_user._id",
  //                       firstName:"$prop_user.firstName",
  //                       lastName:"$prop_user.lastName",
  //                       companyName:"$prop_user.companyName",
  //                       profile_pic:"$prop_user.profile_pic",
  //                       _id:"$proposalList._id"
  //                     }
  //                   }
  //                 }
  //               }
  //           ]).skip(skipValue).limit(10).toArray(function(err,result){
  //             console.log("finalresult-----",result.length)
  //             if(result){
  //               count= result.length;
  //               if(count%10 == 0){
  //                 totalPages= count/10
  //               } else if(count/10 < 1) {
  //                 totalPages=1
  //               }else{
  //                 totalPages= Math.ceil(count/10) 
  //               }
  //             }
  //         err?res.status(500).send(err):res.send({status: "SUCCESS",result:result, totalPage: totalPages, totalCount: count});
  //       })
  //   })
  // },

  publicAssignList: function (req, res) {
    console.log(req.body);
    var condition = {
      $and: [{ $or: [{ assigneeId: ModelService.ObjectId(User, req.current_user.id) }, {}] }]
    }
    Post.native(function (err, collection) {
      collection.aggregate([
        {
          $match: {
            userId: ModelService.ObjectId(User, req.current_user.id)
          }
        },
        {
          $match: {
            "type": "job"
          }
        },
        {
          $lookup:
          {
            from: "user",
            localField: "userId",
            foreignField: "_id",
            as: "user"
          }
        },
        {
          $lookup: {
            localField: "_id",
            foreignField: "postId",
            as: "proposalList",
            from: "proposal"
          }
        },
        // {
        //  $match:{
        //      $or:[{'proposalList.status':"complete"},{'proposalList.status':"contractSign"}]
        //  }
        // },
        {
          $addFields: {
            user: { $arrayElemAt: ["$user", 0] },
            proposalList: {
              $cond: {
                if: {
                  $eq: [{
                    $size: "$proposalList"
                  }, 0]
                },
                then: [{ _id: 0 }],
                else: "$proposalList"
              }
            }
          }
        }, {
          $unwind: "$proposalList"
        }, {
          $lookup: {
            from: "user",
            as: "prop_user",
            localField: "proposalList.senderId",
            foreignField: "_id"
          }
        }, {
          $addFields: {
            prop_user: { $arrayElemAt: ["$prop_user", 0] }
          }
        },
        {
          $group: {
            _id: "$_id",
            userId: { $first: "$userId" },
            title: { $first: "$title" },
            userType: { $first: "$userType" },
            description: { $first: "$description" },
            cost: { $first: "$cost" },
            minSalary: { $first: "$minSalary" },
            maxSalary: { $first: "$maxSalary" },
            education: { $first: "$education" },
            industry: { $first: "$industry" },
            expMnth: { $first: "$expMnth" },
            expYear: { $first: "$expYear" },
            attachment: { $first: "$attachment" },
            status: { $first: "$status" },
            skill_sets: { $first: "$skill_sets" },
            start_date: { $first: "$start_date" },
            end_date: { $first: "$end_date" },
            hoursPerWeek: { $first: "$hoursPerWeek" },
            city: { $first: "$user.city" },
            country: { $first: "$user.country" },
            type: { $first: "$type" },
            viwerType: { $first: "$viwerType" },
            createdAt: { $first: "$createdAt" },
            negotiate: { $first: "$negotiate" },
            updatedAt: { $first: "$updatedAt" },
            currencyType: { $first: "$currencyType" },
            // user_firstName:{$arrayElemAt:["$user.firstName",0]},
            // user_lastName:{$arrayElemAt:["$user.lastName",0]},
            // user_companyName:{$arrayElemAt:["$user.companyName",0]},
            // user_profile_pic:{$arrayElemAt:["$user.profile_pic",0]},
            // user_avg_rating:{$arrayElemAt:["$user.avg_rating",0]},
            proposalDetail: {
              $addToSet: {
                prop_ud: "$prop_user._id",
                firstName: "$prop_user.firstName",
                lastName: "$prop_user.lastName",
                companyName: "$prop_user.companyName",
                profile_pic: "$prop_user.profile_pic",
                _id: "$proposalList._id"
              }
            }
          }
        }
        // {
        //       $project:{
        //         userId:1,
        //         comments:1,
        //         attachment:1,
        //         proposalDetail:1,
        //         user_firstName:{$arrayElemAt:["$user.firstName",0]},
        //         user_lastName:{$arrayElemAt:["$user.lastName",0]},
        //         user_companyName:{$arrayElemAt:["$user.companyName",0]},
        //         user_profile_pic:{$arrayElemAt:["$user.profile_pic",0]},
        //         user_avg_rating:{$arrayElemAt:["$user.avg_rating",0]},
        //         title:1,
        //         cost:1,
        //         status:1,
        //         description:1,
        //         duration:1,
        //         createdAt:1,
        //         skill_sets:1,
        //         assigneeId:1
        //       }
        //     }
      ]).toArray(function (err, result) {
        console.log("finalresult-----", result)
        err ? res.status(500).send(err) : res.send(result);
      })
    })
  },
  assignAdminList: function (req, res) {
    // Post.native(function(err,collection){
    Post.find({ type: "job", status: "pending" }, function (err, result) {
      err ? res.status(500).send(err) : res.send(result);
    })
    // })
  },
  assignAdminDelete: function (req, res) {
    console.log(req.params.id)
    Post.native(function (err, collection) {
      collection.findOne({ _id: ModelService.ObjectId(Post, req.params.id) }, function (err, result) {
        if (err)
          res.json(500, { success: 'ERROR', responseData: err });
        else {
          collection.update({
            _id: ModelService.ObjectId(Post, req.params.id)
          }, {
              $set: { "status": "deleted" }
            }, function (err, result2) {
              err ? res.status(500).send(err) : res.send(result2);
            })
        }
      })
    })
  },

  getPosedJob: function (req, res) {
    // return res.send({length:1});
    Post.native(function (err, collection) {
      collection.aggregate([{
        $match: {
          type: "job",
          userId: ModelService.ObjectId(User, req.current_user.id)
        }
      }, {
        $group: {
          _id: "null",
          count: { $sum: 1 }
        }
      }])
        .toArray(function (err, result) {
          err ? res.status(500).send(err) : res.send(result)
        })
    });
  },


  assignmentComment: function (req, res) {
    console.log("assignmentComment", req.body);
    var oid = new ObjectId();
    var _activityId = JSON.parse(JSON.stringify(oid))
    Post.native(function (err, collection) {
      req.body.comments.created_at = new Date();
      req.body.comments._id = JSON.parse(JSON.stringify(oid));
      req.body.comments.created_by_id = ModelService.ObjectId(User, req.body.msgSenderId);
      req.body.comments.msgReceiverId = ModelService.ObjectId(User, req.body.msgReceiverId);

      //  req.body.recentActivity.createdat=new Date();
      // req.body.recentActivity._id=JSON.parse(JSON.stringify(oid));
      // req.body.recentActivity.created_by_id=ModelService.ObjectId(User,req.current_user.id);
      // req.body.comments.profile_pic=req.current_user.profile_pic.url;
      // req.body.comments.created_by_name=req.current_user.firstName+" "+req.current_user.lastName;
      collection.update({ _id: ModelService.ObjectId(Post, req.body._id) }, { $push: { comments: req.body.comments } }, function (err, result) {
        if (err) {
          return res.json(500, { success: 'ERROR', responseData: err });
        }
        else {
          collection.update({ _id: ModelService.ObjectId(Post, req.body._id) }, { $push: { "recentActivity": { created_at: new Date(), _id: _activityId, created_by: ModelService.ObjectId(User, req.current_user.id), type: "assignment comment" } } }, function (err, activityResult) {
            //  err?res.status(500).send(err):res.send(result)


            var responseData = "Comments add Successfully!"
            return res.json(200, { success: 'Success', responseData: responseData })
          })
        }

      })

    })
  },

  assgnCommentList: function (req, res) {
    console.log("rrrrrrrrrrrrrrrrr---------", req.body);
    Post.native(function (err, collection) {
      collection.aggregate([
        {
          $match: {
            _id: ModelService.ObjectId(Post, req.body.assignmentId)
          }
        },
        {
          $unwind: "$comments"
        },
        {
          $match: {
            $or: [{
              $and: [
                { "comments.created_by_id": ModelService.ObjectId(User, req.body.msgSenderId) },
                { "comments.msgReceiverId": ModelService.ObjectId(User, req.body.msgReceiverId) }
              ]
            },
            {
              $and: [
                { "comments.created_by_id": ModelService.ObjectId(User, req.body.msgReceiverId) },
                { "comments.msgReceiverId": ModelService.ObjectId(User, req.body.msgSenderId) }
              ]
            }]
          }
        },
        {
          $lookup:
          {
            from: "user",
            localField: "comments.created_by_id",
            foreignField: "_id",
            as: "user"
          }
        },
        {
          $project: {
            comments: "$comments",
            firstName: { $arrayElemAt: ["$user.firstName", 0] },
            lastName: { $arrayElemAt: ["$user.lastName", 0] },
            profile_pic: { $arrayElemAt: ["$user.profile_pic", 0] }

          }
        }

      ]).toArray(function (err, result) {
        err ? res.status(500).send(err) : res.send(result);
      })
    })
  },

  createGrpPost: function (req, res) {
    console.log("req.body---->", req.body);
    var obj = {
      title: req.body.post.title,
      article: req.body.post.article,
      post_pic: req.body.post.post_pic,
      group_id: ModelService.ObjectId(Conngrp, req.body._id),
      // userId:ModelService.ObjectId(User,req.current_user.id),
      userId: req.current_user.id,
      type: "group post",
      comments: []
    }
    Post.create(obj).then(function (result) {
      sails.log.debug('Success------->', JSON.stringify(result));
      var responseData = "Job Add Successfully!"
      return res.json(200, { success: 'Success', responseData: responseData });


    }).catch(function (err) {
      console.error("Error on ContactService.createContact");
      console.error(err);
      console.error(JSON.stringify(err));
      return res.json(500, { success: 'ERROR', responseData: err });
    });

  },
  postList: function (req, res) {
    Post.native(function (err, collection) {
      var query = [{
        $match: {
          group_id: ModelService.ObjectId(Conngrp, req.body._id)
        }
      }, {
        $lookup: {
          localField: "userId",
          foreignField: "_id",
          as: "userInfo",
          from: "user"
        }
      }, {
        $addFields: {
          post_firstName: { $arrayElemAt: ["$userInfo.firstName", 0] },
          post_lastName: { $arrayElemAt: ["$userInfo.lastName", 0] },
          post_companyName: { $arrayElemAt: ["$userInfo.companyName", 0] },
          post_profile_pic: { $arrayElemAt: ["$userInfo.profile_pic", 0] },
          post_user_id: { $arrayElemAt: ["$userInfo.profile_pic", 0] }
        }
      }, {
        $addFields: {
          comments: {
            $cond: {
              if: { $eq: [{ $size: "$comments" }, 0] },
              then: [{ msg: "no comment yet", userId: 0 }],
              else: "$comments"
            }
          }
        }
      }, {
        $unwind: "$comments"
      }, {
        $lookup: {
          from: "user",
          localField: "comments.userId",
          foreignField: "_id",
          as: "commentUser"
        }
      }, {
        $addFields: {
          companyName: { $arrayElemAt: ["$commentUser.companyName", 0] },
          firstName: { $arrayElemAt: ["$commentUser.firstName", 0] },
          lastName: { $arrayElemAt: ["$commentUser.lastName", 0] },
          commentUser_profile_pic: { $arrayElemAt: ["$commentUser.profile_pic", 0] }
        }
      }, {
        $group: {
          _id: "$_id",
          title: { $first: "$title" },
          article: { $first: "$article" },
          post_profile_pic: { $first: "$post_profile_pic" },
          post_pic: { $first: "$post_pic" },
          group_id: { $first: "$group_id" },
          userId: { $first: "$userId" },
          firstName: { $first: "$post_firstName" },
          currencyType: { $first: "$currencyType" },
          lastName: { $first: "$post_lastName" },
          companyName: { $first: "$post_companyName" },
          createdAt: { $first: "$createdAt" },
          userType: { $first: { $arrayElemAt: ["$userInfo.userType", 0] } },
          like: { $first: "$like" },
          comments: {
            $push: {
              text: "$comments.text",
              at: "$comments.at",
              firstName: "$firstName",
              companyName: "$companyName",
              lastName: "$lastName",
              profile_pic: "$commentUser_profile_pic",
              _id: "$comments._id",
              userId: "$comments.userId"
            }
          }
        }
      }]
      collection.aggregate(query)
        .toArray(function (err, result) {
          console.log("err----------------------->", err)
          err ? res.status(500).send(result) : res.send(result);
        })
    })
  },
  editComment: function (req, res) {
    Post.native(function (err, collection) {
      collection.update({
        _id: ModelService.ObjectId(Post, req.body.postId),
        "comments._id": req.body.commentId
      }, {
          $set: {
            "comments.$.text": req.body.text
          }
        }, function (err, result) {
          err ? res.status(500).send(err) : res.send(result);
        })
    })
  },
  deleteComment: function (req, res) {
    Post.native(function (err, collection) {
      collection.update({
        _id: ModelService.ObjectId(Post, req.body.postId)
      }, {
          $pull: { "comments": { _id: req.body.commentId } }
        }, function (err, result) {
          err ? res.status(500).send(err) : res.send(result);
        })
    })
  },

  recentActivity_list: function (req, res) {
    Post.native(function (err, collection) {
      var query = [
        { $match: { _id: ModelService.ObjectId(Post, req.body.postId) } },
        { $unwind: "$recentActivity" },
        { $project: { recentActivity: 1 } },
        {
          $lookup: {
            from: "user",
            localField: "recentActivity.created_by",
            foreignField: "_id",
            as: "userInfo"
          }
        }
      ];
      collection.aggregate(query)
        .toArray(function (err, result) {
          err ? res.status(500).send(err) : res.send(result);
        })
    })
  },





  ListOfProposal: function (req, res) {
    console.log("req.body of ListOfProposal", req.body)
    async.waterfall([
      function (cb) {
        Post.native(function (err, collection) {
          collection.aggregate(
            {
              $lookup:
              {
                from: "user",
                localField: "userId",
                foreignField: "_id",
                as: "userInfo"
              }
            }, function (err, result) {
              if (err) {
                cb(err);
              }
              else {
                console.log("ListOfProposal", result);
                cb(null, result);
              }
            })
        })
      }
      // ,
      // function(result,cb){

      // }
    ], function (err, result) {
      err ? res.json(500, { success: 'ERROR', responseData: err }) : res.json(200, { success: 'Success', responseData: result })
    })

  }
};

