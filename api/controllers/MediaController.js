/**
 * MediaController
 *
 * @description :: Server-side logic for managing media
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var async=require("async");
var fs=require("fs");
var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];

module.exports = {
	uploadMedia:function(req,res){
    console.log("updateProfilePic---->", req.params, req.file);
    async.waterfall([
      function(cb){
        req.file(req.params.mediaType).upload({
          maxBytes: 10000000 // maximum 10 MB
        },function(err,result){
          console.log("uploadMediaResult",result)
          var _isArray=Array.isArray(result);
          err?cb(err):cb(null,_isArray?result[0].fd:result.fd)
        });
      },function(filepath,cb){
        console.log("filepath filepath------------------->", filepath)
        var a = Math.floor(Math.random() * 100000);
        // CloudinaryService.uploadProfilePic(filepath,req.params.mediaType+req.current_user.id)
        CloudinaryService.uploadProfilePic(filepath,req.params.mediaType+a)
        .then(function(cloudRes){
          console.log("CloudinaryService--------------------->", cloudRes)
          cb(null,filepath,cloudRes)
          //res.json(200,{result:result,cloudRes:cloudRes});
        })
      },function(localPath,cloudRes,cb){
        fs.unlink(localPath, function(err,result){
          err?cb(err):cb(null,{cloudRes:cloudRes,mediaType:req.params.mediaType});
        })
      }],function(err,result){
      err?res.json(500,err):res.json(200,result)
    })
  },


  uploadInvoice : function(req,res){
    console.log("filepath filepath------------------->", filepath)
    async.waterfall([
      function(cb){
              CloudinaryService.upload_image_on_cloud(req.body.filePath)
        .then(function(cloudRes){
          console.log("CloudinaryService--------------------->", cloudRes)
          cb(null,filepath,cloudRes)
          //res.json(200,{result:result,cloudRes:cloudRes});
        })

      },
      function(result,cb){

      }
      ],function(err,result){

      })


  },


  uploadMediaMultiple:function(req,res){
    var localPath=[]
    console.log("updateProfilePic---->");
    // req.file(req.params.mediaType).upload({
    //       maxBytes: 10000000 // maximum 10 MB
    //     },function(err,result){
    //       console.log("uploadMediaResult",result)
    //       var _isArray=Array.isArray(result);
    //       err?res.json(500,err):res.json(200,result)
    //     });
    async.waterfall([
      function(cb){
        req.file(req.params.mediaType).upload({
          maxBytes: 10000000 // maximum 10 MB
        },function(err,result){
          console.log("uploadMediaResult",result)
          var _isArray=Array.isArray(result);
          err?cb(err):cb(null,result)
        });
      },function(filepath,cb){
        localPath=filepath
        async.map(filepath,function(x,cbmap){
           // CloudinaryService.uploadProfilePic(x.fd,req.params.mediaType+req.current_user.id,[x.filename])
           //  .then(function(cloudRes){
           //    fs.unlink(x.fd, function(err,result){
           //      err?cbmap(err):cbmap(null,{cloudRes:cloudRes,mediaType:req.params.mediaType});
           //    })
           //  })
            if(x.type=="application/pdf"){
              CloudinaryService.uploadPdf(x.fd,req.params.mediaType+req.current_user.id,[x.filename])
              .then(function(cloudRes){
                fs.unlink(x.fd, function(err,result){
                  err?cbmap(err):cbmap(null,{cloudRes:cloudRes,mediaType:req.params.mediaType});
                })
              })
            }
            else if(ValidImageTypes.findIndex(y=>y.toLowerCase()==x.type.toLowerCase())>-1){
              CloudinaryService.uploadProfilePic(x.fd,req.params.mediaType+req.current_user.id,[x.filename])
              .then(function(cloudRes){
                fs.unlink(x.fd, function(err,result){
                  err?cbmap(err):cbmap(null,{cloudRes:cloudRes,mediaType:req.params.mediaType});
                })
                //res.json(200,{result:result,cloudRes:cloudRes});
              })
            }
            else{
              console.log("raw upload......................");
              CloudinaryService.uploadRaw(x.fd,x.filename,req.params.mediaType+req.current_user.id)
              .then(function(objS){
                cb(null,objS);
              },function(objE){
                console.log("raw upload error--------------->",objE);
                cb(objE);
              })
            }
            

        },function(err,result){
            err?cb(err):cb(null,result)
        })
      }],function(err,result){
      err?res.json(500,err):res.json(200,result)
    })
    
  },

  uploadExcel : function(req, res){
    console.log("in api mediacontroller", req.body);
    var fileName="";
    async.waterfall([
      function(cb){
        req.file(req.params.mediaType).upload({
          maxBytes: 10000000 // maximum 10 MB
        },function(err,result){
          console.log("uploadMediaResult",result[0].filename)
          fileName= result[0].filename
          var _isArray=Array.isArray(result);
          err?cb(err):cb(null,_isArray?result[0].fd:result.fd)
        });
      },function(filepath ,cb){
          console.log("filename", fileName)
        CloudinaryService.uploadRaw(filepath,fileName)
        .then(function(cloudRes){
          console.log("cloudRes", cloudRes)
          cb(null,cloudRes.url)
          // res.json(200,{result:cloudRes.url});
        })
      }],function(err,result){
        console.log("result======", result)
      err?res.json(500,err):res.json(200,result)
    })
  },

  uploadIntern : function(req, res){
    console.log("in api mediacontroller", req.body);
    var fileName="";
    async.waterfall([
      function(cb){
        req.file(req.params.mediaType).upload({
          maxBytes: 10000000 // maximum 10 MB
        },function(err,result){
          console.log("uploadMediaResult",result[0].filename)
          fileName= result[0].filename
          var _isArray=Array.isArray(result);
          err?cb(err):cb(null,_isArray?result[0].fd:result.fd)
        });
      },function(filepath ,cb){
          console.log("filename", fileName)
        CloudinaryService.uploadRawExcel(filepath,fileName)
        .then(function(cloudRes){
          console.log("cloudRes", cloudRes)
          cb(null,cloudRes.url)
          // res.json(200,{result:cloudRes.url});
        })
      }],function(err,result){
        console.log("result======", result)
      err?res.json(500,err):res.json(200,result)
    })
  }

  
  
};


