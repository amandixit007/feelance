/**
 * DomainSpecialisationController
 *
 * @description :: Server-side logic for managing Domainspecialisations
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	create : function(req, res){
		console.log("reqbody", req.body)
		DomainSpecialisation.create(req.body).then(function(result){
			console.log("result======>", result)
			res.send(result);
		})
	},
	getDomainSpec : function(req, res){
		DomainSpecialisation.find().exec(function(err, result){
			if(err){
				return err;
			}else{
				return res.send(result);
			}
		})
	}
};

