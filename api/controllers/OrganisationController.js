/**
 * OrganisationController
 *
 * @description :: Server-side logic for managing Organisations
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	create : function(req, res){
		console.log(req.body);
		Organisation.create(req.body).exec(function(err, result){
			res.send(result);
		})
	},
	getOrganisation : function(req, res){
		Organisation.find().exec(function(err, result){
			if(err){
				return err;
			}else{
				return res.send(result);
			}
		})
	}
};

