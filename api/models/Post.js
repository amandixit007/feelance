/**
 * Post.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

  	  userId:{
         model:'user'
  	  },
      assigneeId:{
         model:'user'
      },
      title:{
      	type:'string'
      },
      description:{
      	type:'string'
      },
      skill_sets:{
      	type:'array'
      },
      proposalDetail:{
      	type:'json'
      },
      status:{
      	type:'string',
        defaultsTo:'pending'
      },
      invites_sent:{
      	type:'json'
      },
      cost:{
      	type:'integer'
      },
      duration:{
      	type:'integer'
      },
      attachment:{
        type:'json'
      },
      type:{
        type:'string' //project,post,
      },
      jobType:{
        type:'string'
      },
      groupId:{
         model:'group'
      },
      hoursPerWeek:{
        type:'integer'
      },
      intermediateLevel:{
        type:'string'
      },
      stage:{
        type:'string'
      },
      preferredQualification:{
        type:'json' //job_success_score,rising_star
      },
      comments:{
        type:'json'
      },
      recentActivity :{
        type:'json'
      },
      viwerType:{
        type:'string'
      },
      subType:{
        type:'string'
      },
      country :{
         type: 'string'
      },
      currencyType:{
         type: 'string'
      },
      city :{
          type: 'string'
      },
      minSalary:{
         type:'integer'
      },
      maxSalary:{
         type:'integer'
      },
      expMnth:{
         type:'integer'
      },
      expYear:{
         type:'integer'
      },
      education:{
        type: 'string'
      },
      experience:{
        type: 'string'
      },
      industry:{
        type: 'string'
      }

  }
};

