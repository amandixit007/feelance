/**
 * Support.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
        userId:{
           	 model:'user'
        },
	    queryType:{
	        type: 'string'
	    },
	    subject:{
	        type:'string'
	    },
	    message:{
	        type:'string'
	    },
	    tags:{
	        type:'string'
	    },
	    query_status:{
	        type:'string',
	        defaultsTo:"pending"
	    },
	    query_imags:{
	        type:'array'
	    },
	    comments:{
	    	type:'json' //userId,description,imgs
	    }
  }
};

