/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
var bcrypt = require("bcryptjs")

module.exports = {

    attributes: {
        roles: {
            type: 'json'
        },
        salutation: {
            type: 'string'
        },
        firstName: {
            type: 'string'
        },
        middleName: {
            type: 'string'
        },
        lastName: {
            type: 'string'
        },
        universityName: {
            type: 'string'
        },
        universityType: {
            type: 'string'
        },
        businessType: {
            type: 'string'
        },
        designation: {
            type: "string"
        },
        country: {
            type: "string"
        },
        city: {
            type: "string"
        },
        clientType: {
            type: "string"
        },
        email_id: {
            type: 'string',
            required: true,
            unique: true
        },
        password: {
            type: 'string',
            minLength: 6,
            maxLength: 40,
            protected: true
        },
        subscription: {
            type: 'json'
        },
        hourlyRate: {
            type: 'integer'
        },
        avilability_perweek: {
            type: 'string'
        },
        contactNumber: {
            type: 'array'
        },
        primaryContactNumber: {
            type: 'string'
        },
        userType: {
            type: 'string'
        },
        dOB: {
            type: 'date'
        },
        companyName: {
            type: 'string'
        },
        companyWebsite: {
            type: 'string'
        },
        companyOwner: {
            type: "string"
        },
        description: {
            type: 'string'
        },
        industry: {
            type: 'string'
        },
        areaOfInterest: {
            type: 'array'
        },
        representativeName: {
            type: 'string'
        },
        representativeDesignation: {
            type: 'string'
        },
        address: {
            type: 'json'
        },
        job_title: {
            type: 'string'
        },
        code: {
            type: 'string'
        },
        cvUpload: {
            type: 'json'
        },
        profile_description: {
            type: 'string'
        },
        profile_compilation: {
            type: 'integer'
        },
        profile_pic: {
            type: 'json'
        },
        education_info: {
            type: 'json' //school,date_from,date_to,degree,area_of_study,description
        },
        employment_history: {
            type: 'json' //company,city,country,title,role,period_from,period_to,is_current_company,description
        },
        skill_sets: {
            type: 'array'
        },
        certification: {
            type: 'json'
        },
        portfolio: {
            type: 'json' //project_overview,project_title,thumbnail,project_url,duration,project_category
        },
        languages: {
            type: 'json' //language_name,spoken_type
        },
        is_mobile_verified: {
            type: 'string'
        },
        is_email_verified: {
            type: 'string',
            defaultsTo: 'pending'
        },
        billing: {
            type: 'json' //cardType,cardNumber,firstName,lastName,expireOnMonth,expireOnYear,cvv(Based on Options)
        },
        invoiceAddress: {
            type: 'json' //country,state,city,address,postalCode
        },
        billingAddress: {
            type: 'json' //country,state,city,address,postalCode
        },
        taxInformation: {
            type: 'json'
        },
        paymentDetail: {
            type: 'json'
        },
        manPowers: {
            type: 'json'
        },
        notification: {
            type: 'json'
        },
        connections: {
            type: 'array'
        },
        visitors: {
            type: "array"
        },
        reviews: {
            type: 'json'
        },
        ratings: {
            type: 'json'
        },
        avg_rating: {
            type: 'number'
        },
        isclientFirstLogin: {
            type: 'string',
            defaultsTo: 'true'
        },
        tagline: {
            type: 'string'
        },
        professionalLink: {
            type: 'json'
        },
        other_experience: {
            type: 'string'
        },
        rewards: {
            type: 'string'
        },
        profileComplition: {
            type: 'integer'
        },
        currency_type: {
            type: 'string'
        },
        bucket: {
            type: 'json'
        },
        kyc: {
            type: 'json'
        },
        kyc_status: {
            type: 'string',
            defaultsTo: 'pending'
        },
        // wallet_amt:{
        //     type:'number',
        //     defaultsTo: 0
        // },
        downloadedCv: {
            type: 'array'
        },
        total_experience: {
            type: 'string'
        },
        viewProfileUser: {
            type: 'array'
        },
        bulkUploadFileName: {
            type: 'string'
        }
    },
    beforeCreate: function (values, cb) {
        console.log("values", values);
        bcrypt.hash(values.password, 10, function (err, hash) {
            if (err) return cb(err);
            values.password = hash;
            cb(null, values);
        });
    },
    comparePassword: function (password, userpassword) {
        return new Promise(function (resolve, reject) {
            bcrypt.compare(password, userpassword, function (err, match) {
                if (err) reject(err);

                if (match) {
                    resolve(true);
                } else {
                    reject(err);
                }
            })
        });
    },
    getNamePic: function (id) {
        return new Promise(function (resolve, reject) {
            // console.log("getNamePic----->", id)
            User.native(function (err, collection) {
                collection.findOne({ _id: ModelService.ObjectId(User, id) },
                    function (err, result) {
                        if (err)
                            reject(err);
                        else {
                            // console.log("getNamePicgetNamePicgetNamePicgetNamePic id---->", id)
                            // console.log("getNamePicgetNamePicgetNamePicgetNamePic result.firstName---->", result.firstName)
                            resolve({
                                salutation: result.salutation,
                                firstName: result.firstName,
                                middleName: result.middleName,
                                lastName: result.lastName,
                                email_id: result.email_id,
                                universityName: result.universityName,
                                profile_pic: result.profile_pic,
                                companyName: result.companyName
                            })
                        }
                    })
            })

        })

    }

};

