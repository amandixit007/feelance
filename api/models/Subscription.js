/**
 * Subscription.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
  	subscriptionName:{
  		type:'string'
  	},
  	subscriptionPrice:{
  		type:'integer'
  	},
    subscriptionDescription:{
      type:'string'
    },
    isTrail:{
      type:'boolean'
    },
  	duration:{
  		type:'integer'
  	},
  	durationType:{
  		type:'string'
  	},
    plan:{
      type:'json'
    },
    resumeDownload:{
      type:'integer'
    }

  }
};

