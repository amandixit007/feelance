/**
 * Conngrp.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
       admin_id:{
       	  model:"user"
       },
       groupName:{
       	  type:'string'
       },
       description:{
       	  type:'string'
       },
       groupTags:{
       	  type:'array'
       },
       profile_pic:{
       	  type:'json'
       },
       members:{
       	  type:'json'
       },
       post:{
            type:'json'
       }
  }
};

