/**
 * Proposal.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

 

module.exports = {

  attributes: {
      postId:{
          model:'post'
      },
      senderId:{
      	  model:'user'
      },
      receiverId:{
          model:'user'
      },
      status:{
      	  type:'string',
      	  defaultsTo:'pending'
      },
      coverLetter:{
          type:'string',
      },
      description:{
         type:'string',
      },
      type:{
         type:'string',
      },
      recentActivity :{
        type:'json'
      },
      proposalDetail:{
      	  type:'json' //milestone_name,description,estimated_hour,date,milestone_cost
      }
  }
};

