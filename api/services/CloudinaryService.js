var cloudinary = require('cloudinary');
var xlsx = require("node-xlsx");
var base64 = require('file-base64');
var fs = require('fs');
cloudinary.config({
    cloud_name: sails.config.secrets.cloudinary.name,
    api_key: sails.config.secrets.cloudinary.key,
    api_secret: sails.config.secrets.cloudinary.secret
});
var path = require('path');

module.exports = {

    "upload_image_on_cloud": function (image_base64, cb) {
        console.log("outside image_base64: ", image_base64)
        base64.encode(image_base64, function (err, base64String) {
            console.log("aaaaaaaaa", base64String);
            console.log('Inside image_base64: ', image_base64);
            var pdf_base64 = image_base64;
            binaryData = new Buffer(pdf_base64, 'base64');
            fs.writeFile("dog.pdf", binaryData, "binary", function (err) {
                console.log(err);
            });
            cloudinary.uploader.upload("dog.pdf", function (result) {
                console.log("resultwwwwwwww->>" + JSON.stringify(result));
                image_base64 = result.url;
                // callback(null, result);
            }, { public_id: 'resume_pdf' });


            /* var pdf_base64 = image_base64;
            var binaryData = new Buffer(pdf_base64, 'base64');

            let randomNumber = Math.random().toString().substr(2, 10);       // generate 10 digit random number
            let invoiceFileName = `invoice_${randomNumber}`;
            let invoiceFileNameWithPath = `./temp/${invoiceFileName}.pdf`;

            fs.writeFile(invoiceFileNameWithPath, binaryData, "binary", function (err) {
                console.log('err: ', err);

                cloudinary.uploader.upload(invoiceFileNameWithPath, function (result) {
                    console.log("result------>> " + JSON.stringify(result));
                    image_base64 = result.url;
                    console.log(`Cloudinary URL: `, image_base64);
                    // cb(null, result.url);
                }, { public_id: invoiceFileName });
            }); */
        });
    },








    uploadProfilePic: function (filePath, public_id, format, tags) {
        console.log("cloudinary upload============>", filePath)
        if (!tags)
            tags = ['special', 'for_homepage']
        return new Promise(function (resolve, reject) {
            cloudinary.uploader.upload(
                filePath,
                function (result) {
                    resolve(result);
                },
                {
                    public_id: public_id,
                    crop: 'limit',
                    width: 2000,
                    height: 2000,
                    eager: [
                        {
                            width: 200, height: 200, crop: 'thumb', gravity: 'face',
                            radius: 20, effect: 'sepia'
                        },
                        { width: 100, height: 150, crop: 'fit' }
                    ],
                    tags: tags
                })
        })

    },
    uploadPdf: function (filePath, public_id, format, tags) {
        if (!tags)
            tags = ['special', 'for_homepage']
        return new Promise(function (resolve, reject) {
            cloudinary.uploader.upload(
                filePath,
                function (result) {
                    resolve(result);
                },
                {
                    public_id: public_id,
                    tags: tags
                })
        })

    },
    uploadRaw: function (filePath, filename, public_id, tags) {
        if (!tags)
            tags = ['special', 'for_homepage']
        var oldPath = filePath;
        var newFileName = new Date().getTime() + "~" + filename
        var newPath = path.join(process.env.PWD, "assets", "media", newFileName);
        var relativePath = path.join("media", newFileName);
        console.log("oldPath---->", filename)
        console.log("newPath---->", newPath)
        return new Promise(function (resolve, reject) {
            fs.rename(oldPath, newPath, function (err) {
                if (err) {
                    if (err.code === 'EXDEV') {
                        console.log("err.code === 'EXDEV'")
                        var readStream = fs.createReadStream(oldPath);
                        var writeStream = fs.createWriteStream(newPath);

                        readStream.on('error', function (objE) {
                            console.log("readStream error---->", objE)
                            reject(objE)
                        });
                        writeStream.on('error', function (objE) {
                            console.log("writeStream error---->", objE)
                            reject(objE)
                        });

                        readStream.on('close', function () {
                            console.log("readStream close---->")
                            fs.unlink(oldPath, function (objS) {
                                console.log("close close close");
                                resolve({ url: newPath });
                            });
                        });

                        readStream.pipe(writeStream);
                    } else {
                        reject(err);
                    }
                    //return;
                }
                else {
                    console.log("else else else else else else else--------", newPath);
                    resolve({ url: relativePath });
                }

            });
        })

    },

    uploadRawExcel: function (filePath, filename, public_id, tags) {
        if (!tags)
            tags = ['special', 'for_homepage']
        var oldPath = filePath;
        var newFileName = new Date().getTime() + "~" + filename
        var newPath = path.join(process.env.PWD, "assets", "media", newFileName);
        // var relativePath=path.join("media",newFileName);
        console.log("oldPath---->", filename)
        console.log("newPath---->", newPath)
        return new Promise(function (resolve, reject) {
            fs.rename(oldPath, newPath, function (err) {
                if (err) {
                    if (err.code === 'EXDEV') {
                        console.log("err.code === 'EXDEV'")
                        var readStream = fs.createReadStream(oldPath);
                        var writeStream = fs.createWriteStream(newPath);

                        readStream.on('error', function (objE) {
                            console.log("readStream error---->", objE)
                            reject(objE)
                        });
                        writeStream.on('error', function (objE) {
                            console.log("writeStream error---->", objE)
                            reject(objE)
                        });

                        readStream.on('close', function () {
                            console.log("readStream close---->")
                            fs.unlink(oldPath, function (objS) {
                                console.log("close close close");
                                resolve({ url: newPath });
                            });
                        });

                        readStream.pipe(writeStream);
                    } else {
                        reject(err);
                    }
                    //return;
                }
                else {
                    console.log("else else else else else else else--------", newPath);
                    resolve({ url: newPath });
                }

            });
        })

    },
    removeMedia: function (public_id) {
        return new Promise(function (resolve, reject) {
            cloudinary.v2.uploader.destroy(public_id, { invalidate: true }, function (error, result) {
                console.log(result)
            });
        })
    },

    uploadExcel: function (filePath) {
        console.log("FilePath===========>", filePath);
        return new Promise(function (resolve, reject) {
            try {
                var data = xlsx.parse(filePath);
                var _sheets = {};
                var _sheetCols = {};
                var _incorrctArray = [];

                for (var i = 0; i < data.length; i++) {
                    _sheets[data[i].name] = [];
                    var _data = data[i].data.filter(x => x.length > 0);


                    for (var j = 1; j < _data.length; j++) {
                        var obj = {};

                        // console.log("data==========>", typeof _data[j][5] , _data[j].length);
                        if (typeof _data[j][0] !== 'string') {
                            _incorrctArray.push(_data[j][0])
                        }
                        if (typeof _data[j][1] !== 'string') {
                            _incorrctArray.push(_data[j][1])
                        }
                        if (typeof _data[j][2] !== 'string') {
                            _incorrctArray.push(_data[j][2])
                        }
                        if (typeof _data[j][3] !== 'number' || _data[j][3] < 0 || _data[j][3].length > 10) {
                            _incorrctArray.push(_data[j][3])
                        }
                        if (typeof _data[j][4] !== 'number' || _data[j][4] < 0 || _data[j][4].length > 4) {
                            _incorrctArray.push(_data[j][4])
                        }
                        if (typeof _data[j][5] !== 'number' || _data[j][5] < 0 || _data[j][5].length > 4) {
                            _incorrctArray.push(_data[j][5])
                        }
                        if (typeof _data[j][6] !== 'number' || _data[j][6] < 0 || _data[j][6].length > 4) {
                            _incorrctArray.push(_data[j][6])
                        }
                        if (typeof _data[j][7] !== 'number' || _data[j][7] < 0 || _data[j][7].length > 4) {
                            _incorrctArray.push(_data[j][7])
                        }
                        if (typeof _data[j][8] !== 'number' || _data[j][8] < 0 || _data[j][8].length > 4) {
                            _incorrctArray.push(_data[j][8])
                        }
                        if (typeof _data[j][9] !== 'string') {
                            _incorrctArray.push(_data[j][9])
                        } else {

                            for (var k = 0; k < _data[j].length; k++) {
                                // else{
                                if (_data[j][k])
                                    obj[_data[0][k]] = _data[j][k].toString().trim();

                                _sheets[data[i].name].push(obj);
                            }
                        }

                    }

                }
                console.log("incorrectArray======>", _incorrctArray);

                if (_incorrctArray !== []) {
                    resolve({ "error": _incorrctArray });
                } else {
                    resolve({ "success": _sheets });
                }


            }
            catch (e) {
                console.log("exception---->", e)
                reject(e)
            }
        })
    }
};

