
var xlsx = require("node-xlsx");
var XLSX = require('xlsx');

module.exports = {
    // uploadExcel: function(filePath){
    // 	return new Promise(function(resolve,reject){
    //            try{
    //                var data=xlsx.parse(filePath);
    //                var _sheets={};
    //                var _sheetCols={};
    //                var _incorrctArray=[];

    //                for(var i=0;i<data.length;i++){
    //                    _sheets[data[i].name]=[];
    //                    var _data=data[i].data.filter(x=>x.length>0);


    //                    for(var j=1; j<_data.length;j++){
    //                        var obj={};
    //                        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    //                        var d = new Date();
    //                        var joinDate = d.getFullYear();
    //                        var _incorrectRow = [];


    //                        	if( typeof _data[j][0] !== 'string' ){
    //                        		_incorrctArray.push(_data[j][0], 0);
    //                                _incorrectRow.push(j)
    //                        	}
    //                        	else if( typeof _data[j][1] !== 'string' ){
    //                        		_incorrctArray.push(_data[j][1], 1);
    //                        	}
    //                            else if( typeof _data[j][2] !== 'string' ){
    //                                _incorrctArray.push(_data[j][2], 2);
    //                            }
    //                        	else if( typeof _data[j][3] !== 'string' || !reg.test(_data[j][3] )){
    //                        		_incorrctArray.push(_data[j][3], 3)
    //                        	}
    //                            else if( typeof _data[j][4] !== 'number' || _data[j][4]<0 || _data[j][4].length>10){
    //                                _incorrctArray.push(_data[j][4], 4)
    //                            }
    //                        	else if( typeof _data[j][5] !== 'number' || _data[j][5]<0 || _data[j][5].length>4 || _data[j][5]> joinDate){
    //                        		_incorrctArray.push(_data[j][5], 5)
    //                        	}
    //                        	else if( typeof _data[j][6] !== 'number' || _data[j][6]<0 || _data[j][6].length>4 || _data[j][6]> joinDate || _data[j][6]< _data[j][5]){
    //                        		_incorrctArray.push(_data[j][6], 6)
    //                        	}
    //                        	else if( typeof _data[j][7] !== 'number' || _data[j][7]<0 || _data[j][7].length>4 || _data[j][7]>100){
    //                        		_incorrctArray.push(_data[j][7], 7)
    //                        	}
    //                        	else if( typeof _data[j][8] !== 'number' || _data[j][8]<0 || _data[j][8].length>4 || _data[j][8]>100){
    //                        		_incorrctArray.push(_data[j][8], 8)
    //                        	}
    //                        	else if( typeof _data[j][9] !== 'number' || _data[j][9]<0 || _data[j][9].length>4 || _data[j][9]>100){
    //                        		_incorrctArray.push(_data[j][9], 9)
    //                        	}
    //                        	else if( typeof _data[j][10] !== 'string' ){
    //                        		_incorrctArray.push(_data[j][10], 10)
    //                        	}
    //                            else if( typeof _data[j][11] !== 'number' || _data[j][11]<0 || _data[j][11].length>2){
    //                        		_incorrctArray.push(_data[j][11], 11)
    //                        	}else{

    //                        		 for(var k=0;k<_data[j].length;k++){
    //                        	// else{
    //                        		 if(_data[j][k])
    //                            		obj[_data[0][k]]=_data[j][k].toString().trim();
    //                        	}
    //                        }
    //                        if(Object.keys(obj).length !==0){
    //                         _sheets[data[i].name].push(obj);
    //                        }
    //                    }


    //                }
    //                resolve({_sheets, "error": _incorrctArray});
    //            }
    //            catch(e){
    //                console.log("exception---->",e)
    //                reject(e)
    //            }
    //        })
    // },
    // convertToJson : function(filePath){
    //        var data=xlsx.parse(filePath);
    //             var _sheets={};
    //             var _sheetCols={};
    //             //var arr=[];
    //             //console.log("data.length--->",data.length);
    //             for(var i=0;i<data.length;i++){
    //                 //console.log("data[i].name--->",data[i].name);
    //                 _sheets[data[i].name]=[];
    //                 var _data=data[i].data.filter(x=>x.length>0);

    //                 for(var j=1;j<_data.length;j++){
    //                     var obj={};


    //                     for(var k=0;k<_data[j].length;k++){
    //                         if(_data[j][k])
    //                         obj[_data[0][k]]=_data[j][k].toString().trim();
    //                     }

    //                     _sheets[data[i].name].push(obj);

    //                 }
    //             }

    //            console.log(typeof _sheets, _sheets["Sheet1"]);
    //            return _sheets["Sheet1"];
    // },

    convertToJson: function (filePath) {
        return new Promise(function (resolve, reject) {
            try {
                // console.log("filePath---",filePath)
                var data = xlsx.parse(filePath);
                var _sheets = {};
                var _sheetCols = {};
                // console.log("convert2JSON", data)
                //var arr=[];
                //console.log("data.length--->",data.length);
                for (var i = 0; i < data.length; i++) {
                    //console.log("data[i].name--->",data[i].name);
                    _sheets[data[i].name] = [];
                    var _data = data[i].data.filter(x => x.length > 0);

                    for (var j = 1; j < _data.length; j++) {
                        var obj = {};


                        for (var k = 0; k < _data[j].length; k++) {
                            if (_data[j][k])
                                obj[_data[0][k]] = _data[j][k].toString().trim();
                        }

                        _sheets[data[i].name].push(obj);

                    }
                }
                // console.log("_sheets----", _sheets)
                resolve(_sheets)
            }
            catch (e) {
                // console.log("exception---->", e)
                reject(e)
            }
        })
    },

    convert2JSON: function (filePath) {
        var workbook = XLSX.readFile(filePath)
        var first_sheet_name = workbook.SheetNames[0]
        var worksheet = workbook.Sheets[first_sheet_name];
        // console.log('\n\n\n\n\n\n workbook---->', worksheet);
        return XLSX.utils.sheet_to_json(worksheet)
    }
}