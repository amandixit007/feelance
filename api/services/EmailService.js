var SparkPost = require('sparkpost');
sails.log.debug("sdsdsdsdsdsd", sails.config.secrets.sparkPostApiKey)
var client = new SparkPost(sails.config.secrets.sparkPostApiKey);
module.exports = {
	send: function (from_, to_, subject_, message_) {
		// from_="no-reply@senzecit.com"
		console.log("from_----------------", from_)
		//console.log(message_)
		return new Promise(function (resolve, reject) {

			client.transmissions.send({
				options: {
					sandbox: false
				},
				content: {
					from: from_,
					subject: subject_,
					html: message_
				},
				recipients: [
					{ address: to_ }
				]
			})
				.then(data => {
					resolve({ status: true })
				})
				.catch(err => {
					resolve({ status: false })
				});

		});
	},

	getVerificationMailHtml_clientIndividual: function (req, email_id, code, firstName) {
		return '<div style="margin: 25px;"><p>Hey ' + firstName + ',<br><br>Thank you for registering with Feelance Co.; Largest integrated platform for curated freelancers. Your application as an individual client has been successfully processed. Please, click on the link provided below to complete your email verification.</p><h2><a href="' + req.protocol + '://' + req.get('host') + '/activate/account/' + email_id + '/' + code + '">Click Here to Activate Account</a></h2><div>Let’s get started with these simple steps:<br><ul><li>Post your requirement</li><li>Shortlist consultants basis the proposals received</li><li>Begin your engagement</li></ul></div></p></div>';
	},


	getVerificationMailHtml_clientCompany: function (req, email_id, code, firstName) {
		return '<div style="margin: 25px;"><p>Hello ' + firstName + ',<br><br>Thank you for registering with Feelance Co.; Largest integrated platform for curated freelancers. Your application as a corporate client has been successfully processed. Please, click on the link provided below to complete your email verification.</p><h2><a href="' + req.protocol + '://' + req.get('host') + '/activate/account/' + email_id + '/' + code + '">Click Here to Activate Account</a></h2><div>Let’s get started with these simple steps:<br><ul><li>Post your requirement</li><li>Shortlist consultants basis the proposals received</li><li>Begin your engagement</li></ul></div></p></div>';
	},

	getVerificationMailHtml_consultant: function (req, email_id, code, firstName) {
		return '<div style="margin: 25px;"><p>Hello ' + firstName + ',<br><br>Thank you for registering with Feelance Co.; Largest integrated platform for curated freelancers. Your application as an individual consultant has been successfully processed. Please, click on the link provided below to complete your email verification.</p><h2><a href="' + req.protocol + '://' + req.get('host') + '/activate/account/' + email_id + '/' + code + '">Click Here to Activate Account</a></h2><div>Let’s get started with these simple steps:<br><ul><li>Create your skilled profile</li><li>Search project and send proposal</li><li>Start Working</li></ul></div></p></div>';
	},


	getVerificationMailHtml_consultantFirm: function (req, email_id, code, firstName) {
		return '<div style="margin: 25px;"><p>Hello ' + firstName + ',<br><br>Thank you for registering with Feelance Co.; Largest integrated platform for curated freelancers. Your application as a consulting firm has been successfully processed. Please, click on the link provided below to complete your email verification.</p><h2><a href="' + req.protocol + '://' + req.get('host') + '/activate/account/' + email_id + '/' + code + '">Click Here to Activate Account</a></h2><div>Let’s get started with these simple steps:<br><ul><li>Create your skilled profile</li><li>Search project and send proposal</li><li>Start Working</li></ul></div></p></div>';
	},


	getVerificationMailHtml_recruitmentIndividual: function (req, email_id, code, firstName) {
		return '<div style="margin: 25px;"><p>Hello ' + firstName + ',<br><br>Thank you for registering on FEELANCE, an open platform to find and create jobs! Your application as a recruiter has been successfully processed. Please, click on the link provided below to complete your email verification.</p><br><br><h1><a href="' + req.protocol + '://' + req.get('host') + '/activate/account/' + email_id + '/' + code + '">Click Here to Activate Account</a></h1><br><br><div>As a registered member of Feelance, you have the perks to:Get an easy access to a bulk data that is 100% genuine and tested under expert guidance.<br><ul><li>Find quality projects that are authorized and trusted.</li><li>Find advanced filter results through intelligent integration of data.</li><li>A separate dispute model provided for any kind of disruption.</li><li>A thorough case study on every project delivered.</li></ul></div>We hope, you have a happy experience working with us!</p></div>';
	},


	getVerificationMailHtml_recruitmentCompany: function (req, email_id, code, firstName) {
		return '<div style="margin: 25px;"><p>Hello ' + firstName + ',<br><br>Thank you for registering on FEELANCE, an open platform to find and create jobs! Your application as a recruitment firm has been successfully processed. Please, click on the link provided below to complete your email verification.</p><br><br><h1><a href="' + req.protocol + '://' + req.get('host') + '/activate/account/' + email_id + '/' + code + '">Click Here to Activate Account</a></h1><br><br><div>As a registered member of Feelance, you have the perks to:Get an easy access to a bulk data that is 100% genuine and tested under expert guidance.<br><ul><li>Find quality projects that are authorized and trusted.</li><li>Find advanced filter results through intelligent integration of data.</li><li>A separate dispute model provided for any kind of disruption.</li><li>A thorough case study on every project delivered.</li></ul></div>We hope, you have a happy experience working with us!</p></div>';
	},
	getVerificationMailHtml_fosCompany: function (req, email_id, code, firstName) {
		return '<div style="margin: 25px;"><p>Hello ' + firstName + ',<br><br>Thank you for registering on FEELANCE, an open platform to find and create jobs! Your application as a Company(foS) has been successfully processed. Please, click on the link provided below to complete your email verification.</p><br><br><h1><a href="' + req.protocol + '://' + req.get('host') + '/activate/account/' + email_id + '/' + code + '">Click Here to Activate Account</a></h1><br><br><div>As a registered member of Feelance, you have the perks to:Get an easy access to a bulk data that is 100% genuine and tested under expert guidance.<br><ul><li>Find quality projects that are authorized and trusted.</li><li>Find advanced filter results through intelligent integration of data.</li><li>A separate dispute model provided for any kind of disruption.</li><li>A thorough case study on every project delivered.</li></ul></div>We hope, you have a happy experience working with us!</p></div>';
	},
	getVerificationMailHtml_fosIndividual: function (req, email_id, code, firstName) {
		return '<div style="margin: 25px;"><p>Hello ' + firstName + ',<br><br>Thank you for registering on FEELANCE, an open platform to find and create jobs! Your application as an individual(foS) has been successfully processed. Please, click on the link provided below to complete your email verification.</p><br><br><h1><a href="' + req.protocol + '://' + req.get('host') + '/activate/account/' + email_id + '/' + code + '">Click Here to Activate Account</a></h1><br><br><div>As a registered member of Feelance, you have the perks to:Get an easy access to a bulk data that is 100% genuine and tested under expert guidance.<br><ul><li>Find quality projects that are authorized and trusted.</li><li>Find advanced filter results through intelligent integration of data.</li><li>A separate dispute model provided for any kind of disruption.</li><li>A thorough case study on every project delivered.</li></ul></div>We hope, you have a happy experience working with us!</p></div>';
	},

	getMailHtml_consultantApplyJob: function (senderFirstName, senderLastName, firstName, lastName, title) {
		return '<div style="margin: 25px;"><p>Hello, <b>' + firstName + ' ' + lastName + '</b><br><br>You have received a new proposal from <b>' + senderFirstName + ' ' + senderLastName + '</b> on your project <b>' + title + '</b><br></p><div>Please go through the project plan provided as per milestones basis:<br>We hope that this proposal stands out to the best of your interest.</br></p></div>';
	},

	getMailHtml_clientAskContract: function (senderFirstName, senderLastName, firstName, lastName, title) {
		return '<div style="margin: 25px;"><p>Hello, <b>' + senderFirstName + ' ' + senderLastName + '</b><br><br>Congratulations! Your proposal on the project <b>' + title + '</b> has been accepted by the client <b>' + firstName + ' ' + lastName + '</b>. You are now to sail on the boat to complete this project.</p></div>';
	},

	getMailHtml_clientAddMoney: function (firstName, lastName, amount, txId, title, invoiceLink) {
		return '<div style="margin: 25px;"><p>Hello, <b>' + firstName + ' ' + lastName + '</b><br><br>You have successfully added an amount of INR <b>' + amount + '</b>  to your Feelance Escrow wallet. Your Transaction ID is <b>' + txId + '</b>. You can now carry forward easy transactions on the projects <b>' + title + '</b>.<br><br>You can also view your invoice by clicking below link.<h3><a href="' + invoiceLink + '" target="_blank">View Invoice</a></h3></p></div>';
	},

	getMailHtml_consultantAckn: function (senderFirstName, senderLastName, firstName, lastName, title) {
		return '<div style="margin: 25px;"><p>Hello, <b>' + firstName + ' ' + lastName + '</b><br><br>Congratulations! Your consultant  <b>' + senderFirstName + ' ' + senderLastName + '</b> has sent his acknowledgement on the proposal of the project <b> ' + title + '</b>. Both the parties are now on the same page to start with the project work.</b>.<br><br>We hope you enjoy a smooth sail with us!!</p></div>';
	},

	getMailHtml_consultantAskSignoff: function (senderFirstName, senderLastName, firstName, lastName, title, milestone) {
		return '<div style="margin: 25px;"><p>Hello, <b>' + firstName + ' ' + lastName + '</b> <br><br>Your consultant <b>' + senderFirstName + ' ' + senderLastName + '</b> is asking for your sign off <b>' + milestone + '</b> against the project<b> ' + title + '.</b><br>We request you to review the progress and release the milestone. If you have any concern about the milestone,<b>' + senderFirstName + ' ' + senderLastName + '</b> is waiting for your feedback/comments. </br><br><p>Hope you are having a great experience working with us!</p></p></div>';
	},

	getMailHtml_clientSignoff: function (senderFirstName, senderLastName, firstName, lastName, title, milestone) {
		return '<div style="margin: 25px;"><p>Hello, <b>' + senderFirstName + ' ' + senderLastName + '</b><br><br>Good Job! You have successfully completed your project <b> ' + title + '</b>.<br> </br></br>The client <b>' + firstName + ' ' + lastName + '</b> has successfully processed all the dues concerning this project and milestone <b>' + milestone + '</b>.<br><br>Let’s not stop here and keep this journey going. Surf through the portal for more exciting projects and earn more!<br><br>Happy freelancing!</br><br><p>We look forward to working with you again!</p></p></div>';
	},

	getMailHtml_saveSubscription: function (firstName, lastName, plan) {
		return '<div style="margin: 25px;"><p>Hello, <b>' + firstName + ' ' + lastName + '</b><br><br>Thank you for subscribing to our <b>' + plan + '</b>. Now you hold access to a varied range of services offered at Feelance Co.. Now, engage with top talents for your consulting/freelance requirements.</br><br></p></div>';
	},
	getMailHtml_approveKycNotify: function (firstName, lastName, plan) {
		return '<div style="margin: 25px;"><p>Hello, <b>' + firstName + ' ' + lastName + '</b><br><br>Your Bank Details has been successfully verified. You can now have hassle free transactions from your Feelance account.<br><br></p></div>';
	},
	resetPasswordMailHtml: function (req, firstName, email_id, code) {
		return '<div style="margin: 25px;"><p>Hello ' + email_id + ',<br><br>We have received a request to reset your Feelance account password.<br>Please click on the link provided below to change your password:<div><a href="' + req.protocol + '://' + req.get('host') + '/resetPassword/' + email_id + '/' + code + '">Recover Your Password</a></div></p></div>';
	},

	newJobFeedToUsers: function (firstName, lastName, jobTitle, jobDuration, jobAmount, jobDescription) {
		return '<div style="margin: 25px;"><p>Hello, <b>' + firstName + ' ' + lastName + '</b><br><br>Please see the latest job recommendations for you.<br/><br/><b>' + jobTitle + '</b><br/>Duration: <b>' + jobDuration + '</b><br/>Amount: <b>' + jobAmount + '</b><br /><ins>' + jobDescription + '</ins><h2><br /><a align="center" href="' + req.protocol + '://' + req.get('host') + '/#/home">Apply now</a></h2></p></div>';
	},
}
