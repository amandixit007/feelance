
var express = require('express')
var app = express();
var fs = require("fs");
var http = require('http').Server(app);

var options = {
	key: fs.readFileSync(__dirname + '/ssl/feelance.key'),
	cert: fs.readFileSync(__dirname + '/ssl/feelance.crt'),
	// requestCert: true
};
var http = require('https').Server(options, app);

var io = require('socket.io')(http);

var users = {}, sockets = {};
io.on('connection', function (socket) {

	console.log("socket > a user connected > socketID....", socket.id);
	//console.log(socket);

	socket.on("initClient", function (data) {
		console.log("socket > data: ", data)
		if (!users[data.userId]) {
			users[data.userId] = [{ socket: socket.id }];
		}
		else {
			var indx = users[data.userId].findIndex(x => x.socket == socket.id);
			if (indx == -1)
				users[data.userId].push({ socket: socket.id });
		}
		sockets[socket.id] = { socket: socket, userId: data.userId };
	})
	socket.on('disconnect', function () {
		console.log("socket > a user disconnected > socketID....", socket.id);
		if (sockets[socket.id]) {
			var indx = users[sockets[socket.id]["userId"]].findIndex(x => x.socket == socket.id);
			if (indx > -1)
				users[sockets[socket.id]["userId"]].splice(indx, 1)
			// users[indx].splice(indx, 1);
			delete sockets[socket.id];
		}
	});
})

http.listen(1101);

module.exports = {
	saveNotification: function (data, recieverId) {
		return new Promise(function (resolve, reject) {
			User.native(function (err, collection) {
				var oid = new ObjectId();
				var query = {
					_id: ModelService.ObjectId(User, req.body.receiverId)
				};
				collection.update(query, {
					$push: {
						notification: data
					}
				}, function (err, result) {
					err ? reject(err) : resolve(data._id)
				})
			})
		})
	},
	newNotification: function (data, recieverId) {
		console.log("socket > newNotification > data: ", data)
		console.log("socket > newNotification > users: ", users)
		console.log("socket > newNotification > recieverId: ", recieverId)
		if (users[recieverId]) {
			for (var i = 0; i < users[recieverId].length; i++) {
				//console.log("users[recieverId][i].socket---->",users[recieverId][i].socket)
				var socketId = users[recieverId][i].socket;
				//console.log("sockets----------->",sockets)
				if (sockets[socketId])
					sockets[socketId].socket.emit("newNotification", data);
			}
		}
	},
	newfriendRequest: function (data, recieverId) {
		if (users[recieverId]) {
			for (var i = 0; i < users[recieverId].length; i++) {
				var socketId = users[recieverId][i].socket;
				sockets[socketId].socket.emit("newfriendRequest", data);
			}
		}
	}
}




// var socketIOClient = require('socket.io-client');
// var sailsIOClient = require('sails.io.js');

// var io = sailsIOClient(socketIOClient);

// io.sails.url = 'http://localhost:1337';

// io.sails.connect(function(socketObj){
// 	console.log("A user connected....")
// 	console.log(socketObj);
// })

// const webpush = require('web-push');

// // VAPID keys should only be generated only once.
// const vapidKeys = webpush.generateVAPIDKeys();

// webpush.setGCMAPIKey('<Your GCM API Key Here>');
// webpush.setVapidDetails(
//   'mailto:example@yourdomain.org',
//   vapidKeys.publicKey,
//   vapidKeys.privateKey
// );

// // This is the same output of calling JSON.stringify on a PushSubscription
// const pushSubscription = {
//   endpoint: '.....',
//   keys: {
//     auth: '.....',
//     p256dh: '.....'
//   }
// };

// webpush.sendNotification(pushSubscription, 'Your Push Payload Text');

// var firebase = require("firebase");

// // var config = {
// //   apiKey: "<API_KEY>",
// //   authDomain: "<PROJECT_ID>.firebaseapp.com",
// //   databaseURL: "https://<DATABASE_NAME>.firebaseio.com",
// //   storageBucket: "<BUCKET>.appspot.com",
// // };

// var config = {
//     apiKey: "AIzaSyD7bC9o9IfO8vGWdGKG8lAqHtE95nmmNlM",
//     authDomain: "feelance-339b7.firebaseapp.com",
//     databaseURL: "https://feelance-339b7.firebaseio.com",
//     projectId: "feelance-339b7",
//     storageBucket: "feelance-339b7.appspot.com",
//     messagingSenderId: "225415858434"
//   };

// firebase.initializeApp(config);

// const messaging=firebase.messaging();
// messaging.requestPermission()
// .then(function(){
// 	console.log("Have permission...");
// })
// .catch(function(e){
// 	console.log("Error in permission.....");
// 	console.log(e);
// })

// module.exports={

// };



// var FCM = require('fcm').FCM;

// var send_android_push={

//     'android_push':function(device_token,msg) {
//         console.log("device_token----"+device_token)
//         var serverKey = 'AAAAbh2qeO8:APA91bGV06rA6Pj_O3HLBYseyg6d0W6Jo2wlwV1uU_s636Co9r7YBJDvtqpbUZZW3ZTsSBnRj45zKFpTCxBZyiXOWfJiOMBaruZgXdSuOzwQV5pqslAJw5AkL6QN-_g2nr6ioEA3U7St';
//             var title=title;
//                     var fcm = new FCM(serverKey);
//                     var message={ 
//                         to: device_token, 
//                         collapse_key: 'your_collapse_key',   
//                         notification: {
//                             title: 'Title of your push notification', 
//                             body: 'Body of your push notification' 
//                         },   
//                         data: JSON.stringify({  
//                             msg:msg 
//                             //title: title,
//                             //type:type,
//                             //data:data       
//                         })
//                     };
//                 fcm.send(message, function(err, response){
//                    if (err) {
//                        console.log("error-->"+err);
//                    } else {
//                        console.log("Successfully sent with response: "+response);
//                    }
//         });
//     }

// }

// module.exports=send_android_push;
