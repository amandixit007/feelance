var Cryptr = require('cryptr')
var bcrypt = require('bcrypt-nodejs')

module.exports = {  
    generateCode : function(email) {
		var cryptr = new Cryptr(sails.config.secrets.cryptrSecretKey)
		var prefix = Math.floor(1000 + Math.random() * 9000);
		var postfix = new Date().getTime();
		return cryptr.encrypt(prefix+email+postfix)
	},

	bEncode : function(string) {
		return new Promise(function(resolve, reject) {
			bcrypt.hash(string, null, null, function(err, hash) {
			console.log(string)
			console.log(hash)
			  // Store hash in your password DB.
			  err?resolve({status : false,hash : hash}):resolve({status : true,hash : hash})
			})
		});
	}
}